
uses
{$IFDEF unix}
  cThreads,
{$ENDIF}
  ptcGraph, ptcCrt,
  //crt,
  smfont01, smgraf {, graph};

begin
  initgraf;
  font01('SMFONT01',0,0,15);
  font01('Font by Remco de Korte - Soft Machine',0,24,15);
  font01('updated oct. 1996',0,36,15);
  font01('abcdefghijklmnopqrstuvwxyz',0,60,15);
  font01('ABCDEFGHIJKLMNOPQRSTUVWXYZ',0,72,15);
  font01('0123456789?!.,:;=+-*()$&%"/�@',0,84,15);
  font01('Press a key...',0,120,15);
  readkey;
  closegraph;
end.
