
program demo02;

uses
{$IFDEF unix}
  cThreads,
{$ENDIF}
  ptcGraph, ptcCrt,
  //crt,
  smfont03, smgraf {, graph};

begin
  initgraf;
  font03('SMFONT03',0,0,7,15,8);
  font03('Font by Remco de Korte',0,45,8,7,0);
  font03('Soft Machine',0,90,8,7,0);
  font03('updated oct. 1996',0,135,8,7,0);
  font03('abcdefghijklmnopqrstuvwxyz',0,225,9,11,1);
  font03('ABCDEFGHIJKLMNOPQRSTUVWXYZ',0,270,9,11,1);
  font03('0123456789!?.,:;+-=*()&',0,315,9,11,1);
  font03('Press a key...',0,435,8,0,14);
  readkey;
  closegraph;
end.
