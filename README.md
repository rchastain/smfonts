# SMFONTS

SMFONTS by Remco de Korte. Fonts for Turbo Pascal *Graph* unit.

## Images

![Image](archives/small01.gif)
![Image](archives/small02.gif)
![Image](archives/small03.gif)
![Image](archives/small04.gif)
![Image](archives/small05.gif)
![Image](archives/small06.gif)
![Image](archives/small07.gif)
![Image](archives/small08.gif)
![Image](archives/small09.gif)
![Image](archives/small10.gif)
![Image](archives/small11.gif)
![Image](archives/small12.gif)
![Image](archives/small13.gif)
![Image](archives/small14.gif)
![Image](archives/small15.gif)
![Image](archives/small16.gif)
![Image](archives/small17.gif)
![Image](archives/small18.gif)
![Image](archives/small19.gif)
![Image](archives/small21.gif)
![Image](archives/small22.gif)
![Image](archives/small23.gif)
![Image](archives/small24.gif)
![Image](archives/small25.gif)
![Image](archives/small26.gif)
![Image](archives/small27.gif)
![Image](archives/small28.gif)
![Image](archives/small29.gif)
![Image](archives/small30.gif)
![Image](archives/small31.gif)
![Image](archives/small32.gif)
![Image](archives/small33.gif)
![Image](archives/small34.gif)

## Details

In [archives](./archives/) directory you can find the original files (that I have downloaded [here](https://web.archive.org/web/20030202065800/http://www.xs4all.nl/~remcodek/fontpage.html)).

In the main directory you can find some files that I retouched to be able to compile them with Free Pascal and *ptcGraph* unit.
