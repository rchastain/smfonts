
program demo02;

uses
{$IFDEF unix}
  cThreads,
{$ENDIF}
  ptcGraph, ptcCrt,
  //crt,
  smfont02, smgraf {, graph};

begin
  initgraf;
  font02('SMFONT02',0,0,15);
  font02('Font by Remco de Korte - Soft Machine',0,64,15);
  font02('updated oct. 1996',0,96,15);
  font02('abcdefghijklmnopqrstuvwxyz',0,160,15);
  font02('ABCDEFGHIJKLMNOPQRSTUVWXYZ',0,192,15);
  font02('0123456789!?.,:;+-=*()&',0,224,15);
  font02('Press a key...',0,288,15);
  readkey;
  closegraph;
end.
