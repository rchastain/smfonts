unit smfont18;

{$MODE DELPHI}
{$CODEPAGE CP850}

interface

uses
  //crt, graph;
  ptcGraph, ptcCrt;

procedure font18_1(textstring:string;textx,texty,textcolor,space:integer);
procedure font18_2(textstring:string;textx,texty,textcolor,space:integer);
procedure font18_3(textstring:string;textx,texty,textcolor,space:integer);
procedure font18_4(textstring:string;textx,texty,textcolor,space:integer);

implementation

procedure font18_1;

var
	d,t,e,h,l,xx,yy,w,c:integer;
	fdat:string;
  n:char;

begin
  dec(texty);
  t:=1;
	while t <= length(textstring) do
	begin
		fdat:='FEI0a';
		case textstring[t] of
      'a','�','�','�':fdat:='DDI1abdaabbaabaae';
      'b':fdat:='EBI1iaadabadabbcacda';
      'c':fdat:='DDI0adaadbdcbb';
      'd':fdat:='EBI0cdcbcabadbaadi';
      'e','�','�':fdat:='EDI0adaaababaabbaabaabaaa';
      'f':fdat:='CBI0baeiaae';
      'g':fdat:='ECI0bbabaabcaabaaaaabaafaa';
      'h':fdat:='EBI1iabcabagadace';
      'i':fdat:='ABI1aaf';
      '�':fdat:='ABI0bf';
      'j':fdat:='BBI0gbaf';
      'k':fdat:='EBI1icabaccdbacfb';
      'l':fdat:='ABI1h';
      'm':fdat:='GDI1geadheafe';
      'n':fdat:='EDI1gdbeadaae';
      'o','�','�':fdat:='EDI0adaadbdbcbada';
      'p':fdat:='EDI1gcaaacaaabacbc';
      'q':fdat:='EDI0abcababacaaacaaf';
      'r':fdat:='DDI1geaebd';
      's':fdat:='DDI0abacababababbba';
      't':fdat:='CBI0baehbada';
      'u','�':fdat:='EDI1eaadaebdg';
      'v':fdat:='EDI1cfbebcbacc';
      'w':fdat:='GDI1debegdcdaadb';
      'x':fdat:='EDI1bbgcbbgcb';
      'y':fdat:='EDI1cbabdcbdabcc';
      'z':fdat:='DDI1accagbcca';
      'A':fdat:='FBI0ceadbcaadabaeccace';
      'B':fdat:='FBI1icabbbbbbbbbfbaec';
      'C':fdat:='FBI0bdcadaaafbfceaeba';
      'D':fdat:='FBI1iecfceaabcbcda';
      'E':fdat:='EBI1ibacbbaccaabcfa';
      'F':fdat:='EBI1ibadabadbaadag';
      'G':fdat:='FBI0bdcadaaafbfbcabcada';
      'H':fdat:='FBI1ibbbacagadabaci';
      'I':fdat:='BBI1ig';
      'J':fdat:='CBI0fcfha';
      'K':fdat:='FBI1hcbecdebbbeeb';
      'L':fdat:='EBI1jeagagafb';
      'M':fdat:='HBI1jeaacfdddccdbei';
      'N':fdat:='FBI1kdaacfdbacl';
      'O':fdat:='GBI0cdbbccfbfbfaabcbcda';
      'P':fdat:='FBI1icabbcacabbcabadce';
      'Q':fdat:='GBI0bdcadaaacabbdaabdcaadbbf';
      'R':fdat:='FBI1icabbcacabbcabcbdac';
      'S':fdat:='FBI0acabaabacbbbbbbbbcbabaaabca';
      'T':fdat:='FBI1bfagifbgbf';
      'U':fdat:='FBI1fbaebgagbehb';
      'V':fdat:='FBI1ecadbgbfcdbaec';
      'W':fdat:='IBI1ehbfcdcafgcecdbbdd';
      'X':fdat:='FBI1aeeadbcfbcjdb';
      'Y':fdat:='FBI1ceaacbacecbebdbf';
      'Z':fdat:='FBI1bdccebcaecdccfa';
      '0':fdat:='EBI0afabdcfcdbafa';
      '1':fdat:='BBI1agh';
      '2':fdat:='EBI0abccdaabcbabbbbaacbb';
      '3':fdat:='EBI1afbcaacbacbacbcbca';
      '4':fdat:='EBI0dbddcbbabiaabab';
      '5':fdat:='EBI1eacbacbbacbbacbcca';
      '6':fdat:='EBI0bebcbcaadbaadbbda';
      '7':fdat:='EBI1bfaceabcddcf';
      '8':fdat:='EBI0acabaabbbbcabbbbbaacaba';
      '9':fdat:='EBI0accbcabbcabbcaaabeb';
      '.':fdat:='BHI1d';
      ',':fdat:='BHJ0ae';
      ':':fdat:='BDI1bbdbb';
      ';':fdat:='BDJ1bcdbba';
      '!':fdat:='BBI1eaceb';
      '?':fdat:='DBI1bfabbacbabbabe';
      '(':fdat:='CBJ0bigah';
      ')':fdat:='CBJ0hagib';
      '<':fdat:='FDI0bbdbdaaababababaaada';
      '>':fdat:='FDI1adaaabababacaaacbdbb';
      '+':fdat:='GCI0cafafacgcafafac';
      '-':fdat:='CFF1c';
      '=':fdat:='FEG1aabababababaa';
      '*':fdat:='EBE0aadecbaab';
      '/':fdat:='CBJ0fcbdcbg';
      '&':fdat:='HBI0dcbaaacbacbbaiadcbabcabbcad';
      '"':fdat:='ABD1c';
      '@':fdat:='GCI0bccacaaababbaaaaabcaaaaabadb';
      '�':fdat:='ABB1aaa';
      '�':fdat:='FBJ0aihabbbabdbafb';
      '�':fdat:='A@B0aaaaaaaa';
      '�':fdat:='ECJ0eccbcc';
    end;
		e:=ord(fdat[1])-65;
		h:=ord(fdat[2])-65;
		l:=ord(fdat[3])-65;
    n:=fdat[4];
		xx:=0;
		yy:=h;
		w:=4;
		repeat
			inc(w);
      for c:=1 to ord(fdat[w])-96 do
      begin
      	if n='1' then
        case textcolor of
          0..255:putpixel(textx+xx,texty+yy,textcolor);
          256:if getpixel(textx+xx,texty+yy)>0 then putpixel(textx+xx,texty+yy,getpixel(textx+xx,texty+yy)-1);
          257:if getpixel(textx+xx,texty+yy)<4 then putpixel(textx+xx,texty+yy,getpixel(textx+xx,texty+yy)+1);
          258:if getpixel(textx+xx,texty+yy)>3 then putpixel(textx+xx,texty+yy,getpixel(textx+xx,texty+yy)-4);
          259:if getpixel(textx+xx,texty+yy)<27 then putpixel(textx+xx,texty+yy,getpixel(textx+xx,texty+yy)+4);
          1000..1255:putpixel(textx+xx,texty+yy,textcolor+yy-h-1000);
          2000..2255:putpixel(textx+xx,texty+yy,textcolor-yy-h-2000);
          3000..3255:putpixel(textx+xx,texty+yy,textcolor+(yy-h)*2-3000);
          4000..4255:putpixel(textx+xx,texty+yy,textcolor-(yy-h)*2-4000);
          5000..5255:putpixel(textx+xx,texty+yy,textcolor+(yy-h)*3 div 2-5000);
          6000..6255:putpixel(textx+xx,texty+yy,textcolor-(yy-h)*3 div 2-6000);
          7000..7255:putpixel(textx+xx,texty+yy,textcolor+(yy-h) div 2-7000);
          8000..8255:putpixel(textx+xx,texty+yy,textcolor-(yy-h) div 2-8000);
        end;
        inc(yy);
			  if yy>l then
			  begin
				  yy:=h;
				  inc(xx);
        end;
			end;
      if n='1' then n:='0' else n:='1';
		until w=length(fdat);
		textx:=textx+e+space+1;
    case textstring[t] of
      '�','�','�','�','�','�','�','�':
      begin
        d:=textx;
        case textstring[t] of
          '�','�','�','�','�','�':textx:=textx-e-space;
          '�':textx:=textx-e-space-2;
          '�':textx:=textx-space;
        end;
        case textstring[t] of
          '�','�','�','�','�':textstring[t]:='�';
          '�':textstring[t]:='�';
          '�':textstring[t]:='�';
          '�':textstring[t]:='�';
        end;
        dec(t);
      end;
    end;
    if textstring[t] in ['�','�','�','�'] then
    begin
      textx:=d;
      textstring[t]:=' ';
    end;
    inc(t);
	end;
end;

procedure font18_2;

var
	d,t,e,h,l,xx,yy,w,c:integer;
	fdat:string;
  n:char;

begin
  dec(texty);
  t:=1;
	while t <= length(textstring) do
	begin
		fdat:='GEM0a';
		case textstring[t] of
      'a','�','�','�':fdat:='FEM0abadabahaacdaackah';
      'b':fdat:='GBM1ybafbcafbcbdcchfeb';
      'c':fdat:='FEM0becgacceedebabcba';
      'd':fdat:='GBM0efehcbdccafcbafz';
      'e','�','�':fdat:='GEM0becgacabadabbdabbgbbacbba';
      'f':fdat:='EBM0cbixabfcabg';
      'g':fdat:='GDM0bcbcaiabbbababbababeabaaacbdfba';
      'h':fdat:='GBM1ycbeacbjbfacidh';
      'i':fdat:='BBM1bakai';
      '�':fdat:='BBM0cici';
      'j':fdat:='DBM0jbjdakaha';
      'k':fdat:='GBM1yfacaecggebcdic';
      'l':fdat:='CBM1yja';
      'm':fdat:='JEM1raagbfjajgbgiah';
      'n':fdat:='GEM1uecgbfjah';
      'o','�','�':fdat:='GEM0becgacceeeccagceb';
      'p':fdat:='GEM1tcacbcbbbbcbfddd';
      'q':fdat:='GEM0addfcbbcbbcbbbcacr';
      'r':fdat:='EEM1raagbgcf';
      's':fdat:='FEM0acacadcdabbdbabeaeaacca';
      't':fdat:='ECM0bbhialabeacbg';
      'u','�':fdat:='GEM1hajfbgceu';
      'v':fdat:='GEM1deggcgcecagbde';
      'w':fdat:='JEM1edggdfbagbhfdecagbed';
      'x':fdat:='GEM1bdgadaefceebcbfdc';
      'y':fdat:='GEM1cdgbbcfddecbfcde';
      'z':fdat:='FEM0fecfacagbfcedb';
      'A':fdat:='IBM0egcibbaccaabbbebcbebcbfcabdabjeg';
      'B':fdat:='HBM1zcbcdbbddbbdicbacbfgda';
      'C':fdat:='HBM0cfdjbcdcabgehdhbabfchca';
      'D':fdat:='HBM1zgehegbaddcbiefb';
      'E':fdat:='HBM0bybbbebbddbbddbbdefdja';
      'F':fdat:='HBM1{bbdcbbfbbbfbbbfciak';
      'G':fdat:='IBM0deeibcebacgdhdhddabfbgaadea';
      'H':fdat:='IBM0aybcdadbjbjbfbbbembj';
      'I':fdat:='CBM1yja';
      'J':fdat:='EBM0jbiehxb';
      'K':fdat:='HBM1ydccadcgfddbidfhc';
      'L':fdat:='GBM0cyedibjbicjb';
      'M':fdat:='KBM1{ggifhfdfedgchy';
      'N':fdat:='IBM1|fbadieifbae~';
      'O':fdat:='JBM0eedibcefgdhdhegbacecbiefb';
      'P':fdat:='HBM1zdbccdbdbcbebbcefgdg';
      'Q':fdat:='JBM0cfehccebabhddcadegfdabedajdi';
      'R':fdat:='IBM1zcccccbebbcebbeceafacceic';
      'S':fdat:='HBM0bcbdbecebbddcbcdcbcebcbbabcehcb';
      'T':fdat:='IBM0achcibjzhdjcjbi';
      'U':fdat:='IBM1ickabfdjbjbjdgnaic';
      'V':fdat:='IBM1geicafdjcicibabecbicge';
      'W':fdat:='LBM1gejiehdfdbhdjididfeaicff';
      'X':fdat:='HBM0igcjadeehddqbggc';
      'Y':fdat:='IBM1dhffabddaegegecgcfegci';
      'Z':fdat:='HBM0ifdgdhbdbhdgefeeia';
      '0':fdat:='GBM0cgcjacfehefcajchb';
      '1':fdat:='DBM0aajxka';
      '2':fdat:='GBM0bbecabefebaddbbdcbcbaedbbcdc';
      '3':fdat:='GBM0bagaabgecaddbccdadcfbeabddb';
      '4':fdat:='HBM0fcgeffddbbccdgbiagjbc';
      '5':fdat:='GBM0bebjbebbddbbddbcbecfaaedb';
      '6':fdat:='GBM0cgdibbbbbebbddbbddbggdb';
      '7':fdat:='GBM1cibedabciadcgefhci';
      '8':fdat:='HBM0hcbdaeahbdbccdcbcdbdbmbcccb';
      '9':fdat:='GBM0bdgfbecbcdcbcdcbbbbjcgc';
      '.':fdat:='CKM1i';
      ',':fdat:='DKN0aacja';
      ':':fdat:='CEM1ccfcfcc';
      ';':fdat:='DEN0gabcdaadcgcca';
      '!':fdat:='CBM1gbkaegc';
      '?':fdat:='FBM0aajcbcaeccaebcbigdg';
      '(':fdat:='EBN0cibchdjbkbk';
      ')':fdat:='EBN0kbkbjdhcbic';
      '<':fdat:='IEM0dagcfaaaebabdacadacbbbcbbaeaabeb';
      '>':fdat:='IEM1bebaaeabbcbcacbcacadbabeaaafcgad';
      '+':fdat:='IEM0dahahahadidahahahad';
      '-':fdat:='DHI1h';
      '=':fdat:='IGJ1abbbbbbbbbbbbbbbbba';
      '*':fdat:='GBG0baeabacgecbababac';
      '/':fdat:='EBM0jbgdededgbj';
      '&':fdat:='LBM0hcheacbbbicdbdbdbgaebdbccefbacebbcebbbfbe';
      '"':fdat:='ACF1d';
      '�':fdat:='BBC1ddd';
      '�':fdat:='HBO0a}lbbbdbbbbbdbbgcbccbfida';
      '�':fdat:='BAC0gaaaaaaa';
      '�':fdat:='GCO0kajchdfdgdhcja';
		end;
		e:=ord(fdat[1])-65;
		h:=ord(fdat[2])-65;
		l:=ord(fdat[3])-65;
    n:=fdat[4];
		xx:=0;
		yy:=h;
		w:=4;
		repeat
			inc(w);
      for c:=1 to ord(fdat[w])-96 do
      begin
      	if n='1' then
        case textcolor of
          0..255:putpixel(textx+xx,texty+yy,textcolor);
          256:if getpixel(textx+xx,texty+yy)>0 then putpixel(textx+xx,texty+yy,getpixel(textx+xx,texty+yy)-1);
          257:if getpixel(textx+xx,texty+yy)<4 then putpixel(textx+xx,texty+yy,getpixel(textx+xx,texty+yy)+1);
          258:if getpixel(textx+xx,texty+yy)>3 then putpixel(textx+xx,texty+yy,getpixel(textx+xx,texty+yy)-4);
          259:if getpixel(textx+xx,texty+yy)<27 then putpixel(textx+xx,texty+yy,getpixel(textx+xx,texty+yy)+4);
          1000..1255:putpixel(textx+xx,texty+yy,textcolor+yy-h-1000);
          2000..2255:putpixel(textx+xx,texty+yy,textcolor-yy-h-2000);
          3000..3255:putpixel(textx+xx,texty+yy,textcolor+(yy-h)*2-3000);
          4000..4255:putpixel(textx+xx,texty+yy,textcolor-(yy-h)*2-4000);
          5000..5255:putpixel(textx+xx,texty+yy,textcolor+(yy-h)*3 div 2-5000);
          6000..6255:putpixel(textx+xx,texty+yy,textcolor-(yy-h)*3 div 2-6000);
          7000..7255:putpixel(textx+xx,texty+yy,textcolor+(yy-h) div 2-7000);
          8000..8255:putpixel(textx+xx,texty+yy,textcolor-(yy-h) div 2-8000);
        end;
        inc(yy);
			  if yy>l then
			  begin
				  yy:=h;
				  inc(xx);
        end;
			end;
      if n='1' then n:='0' else n:='1';
		until w=length(fdat);
		textx:=textx+e+space+1;
    case textstring[t] of
      '�','�','�','�','�','�','�','�':
      begin
        d:=textx;
        case textstring[t] of
          '�','�','�':textx:=textx-e-space;
          '�','�':textx:=textx-e-space-1;
          '�':textx:=textx-e-space-1;
          '�':textx:=textx-e-space-3;
          '�':textx:=textx-space-1;
        end;
        case textstring[t] of
          '�','�','�','�','�':textstring[t]:='�';
          '�':textstring[t]:='�';
          '�':textstring[t]:='�';
          '�':textstring[t]:='�';
        end;
        dec(t);
      end;
    end;
    if textstring[t] in ['�','�','�','�'] then
    begin
      textx:=d;
      textstring[t]:=' ';
    end;
    inc(t);
	end;
end;

procedure font18_3;

var
	d,t,e,h,l,xx,yy,w,c:integer;
	fdat:string;
  n:char;

begin
  dec(texty);
  t:=1;
	while t <= length(textstring) do
	begin
		fdat:='IFQ0a';
		case textstring[t] of
      'a','�','�','�':fdat:='HFQ0abcdccafabbjbbddbaenakbj';
      'b':fdat:='IBQ1�dahbdbhbdcfcdkfihgc';
      'c':fdat:='HFQ0deeiboeehdhegbacdca';
      'd':fdat:='IBQ0gghifkddecdbhdbbh�ao';
      'e','�','�':fdat:='IFQ0cfeibjaccbaeccbdcbcicbaecbcccca';
      'f':fdat:='GBQ0eanblna�bbhdbbj';
      'g':fdat:='IDQ0kbeccdckbiabbbdbbacgbbbhbbabbdccaaiba';
      'h':fdat:='IBQ0a�cbhadbnbhbdldlfia';
      'i':fdat:='CBQ1caoaoal';
      '�':fdat:='CBQ0dldldl';
      'j':fdat:='EBQ0nbneaoakacajb';
      'k':fdat:='IBQ0a�gbcbhcjghdbfdbeemc';
      'l':fdat:='DBQ0a�lb';
      'm':fdat:='MFQ1�abibiyajabjcilakbia';
      'n':fdat:='IFQ1�hcjbhzbia';
      'o','�','�':fdat:='JFQ0cfehcjacfehdhefcajbiefc';
      'p':fdat:='IFQ1�eadbebccccchefgdf';
      'q':fdat:='IFQ0bdgfehdccccbebcbead�';
      'r':fdat:='GFQ1�abhcjcici';
      's':fdat:='HFQ0bcbdbecbafddbccdccbecfacbehcb';
      't':fdat:='FCQ0cbllbabgbdbj';
      'u','�':fdat:='IFQ0aibzhbjch�a';
      'v':fdat:='IFQ1eghdjhejcgdajbhdff';
      'w':fdat:='KFQ1ffjblhcaicicjjoaicff';
      'x':fdat:='IFQ1cfhbqcehdgfceajcgfd';
      'y':fdat:='IFQ1dfhdjbbdggegdbicgeeg';
      'z':fdat:='HFQ0bafeegcibeadadchdffegb';
      'A':fdat:='LBQ0hhekdlbdadcbadbdedccfcdcfdccgdbcdbbndlgi';
      'B':fdat:='KBQ0ana�bccgcbefbcefadelcibiacdgkdb';
      'C':fdat:='KBQ0efhkdmbdeeadhgjfjfjhhcachckdb';
      'D':fdat:='KBQ0ana�igjgihhcaeeebmekggc';
      'E':fdat:='JBQ0c�bccgcbefbcefbcegabehhemb';
      'F':fdat:='JBQ0b�bceecbhcbchcbchcbbidlbn';
      'G':fdat:='LBQ0ffgkdmbeedbchgjfjfeadfebbhdhacchjeb';
      'H':fdat:='LBQ0c�cddcddlcmchadchcbcf�dl';
      'I':fdat:='DBQ0a�jc';
      'J':fdat:='GBQ0mbmdmgi�amc';
      'K':fdat:='KBQ0a�eceafckejhefamdkgfld';
      'L':fdat:='IBQ0e�gfkcmcmckemc';
      'M':fdat:='NBQ0a�iilflhhhfghejdj�ana';
      'N':fdat:='LBQ0a�hcaelflgkhbbf�ana';
      'O':fdat:='MBQ0gfgkcmbefdacifjfjfjcacicaefdbmekggc';
      'P':fdat:='KBQ1�cceddcfcccgccbhcbchgifkdk';
      'Q':fdat:='MBR0efijflddfdbdhdacecbcacfcacacgfadgebdfdcnelfhad';
      'R':fdat:='LBQ1�cdcedcfccdfcceecbhcwbgadfemc';
      'S':fdat:='KBQ0bdcecfddbgdgbddfccdfccdfdccgccbdadcgbbegjdc';
      'T':fdat:='LBQ0bclcldlcmcb�jemcncmcl';
      'U':fdat:='LBQ0ajenboachdmdmcmcmfhdaoancje';
      'V':fdat:='KBQ1igldmcbgfmdldkdabgebmckfhg';
      'W':fdat:='OBQ1igmcokfkdiecldldnkfleifamckegi';
      'X':fdat:='KBQ0mgflcnaefhjekfgmagblehjd';
      'Y':fdat:='LBQ1dlgihlfbdfjgigejdjdhgifjdl';
      'Z':fdat:='KBQ0mfgjejdlceagbddkejfhghhfma';
      '0':fdat:='JBQ0dhflcnadhfldlfhdanclfhd';
      '1':fdat:='EBQ0aan�mb';
      '2':fdat:='IBQ0bbicaefhfhgdadfdbececldbagfbbeeca';
      '3':fdat:='IBQ0naacifebdedcedcdedagchbhaddgabgdc';
      '4':fdat:='JBQ0hckeigheabgdcbfcejdkclbhnbe';
      '5':fdat:='JBQ0oadedcahclefcbfddbfddcdediabehiec';
      '6':fdat:='JBQ0eggkdmbcbbefbbgdcbgdccefbjabdhjec';
      '7':fdat:='IBQ1dlcmbgfabekcmbdeihaadlcm';
      '8':fdat:='JBQ0jdddcfbycfccedebeecdcrbebgccdeb';
      '9':fdat:='JBQ0cejgebaicfdcddfbddfbdedbccbmdkgge';
      '.':fdat:='DNQ0abahaba';
      ',':fdat:='ENR0bacbbjaca';
      ':':fdat:='DFQ0abfbaddhddabfba';
      ';':fdat:='EFR0jacbfbbdehdeabfca';
      '!':fdat:='DBQ1hebakaoagjba';
      '?':fdat:='GBQ0abncdcbbacddagceamdbbgjei';
      '(':fdat:='FBR0ejdoaeiaadmcobn';
      ')':fdat:='FBR0nbocmdaaieaodkd';
      '<':fdat:='LFQ0ebjchdhdgbbbfbbbebdbdbdbdafbbbfbbbfbabhb';
      '>':fdat:='LFQ1bhbabfbbbfbcafbcbdbdbdbebbbfbbbgdhdicibe';
      '+':fdat:='LFQ0ebjbjbjbjbexebjbjbjbjbe';
      '-':fdat:='EJL0an';
      '=':fdat:='LHN1bcdcdcdcdcdcdcdcdcdcdcdcb';
      '*':fdat:='HBI0bbfbbadaaibfeaacbbbacbd';
      '/':fdat:='FBQ0nbjfgehegfjbn';
      '&':fdat:='PBQ0jdkfcdbhaicledceddcjagbfafcfbddghcadgcbdgbcdgbbdhcnag';
      '"':fdat:='BCH1l';
      '�':fdat:='KBT0a�ocpcbcecccadeccicdcebidcdgndb';
      '�':fdat:='GBC0bdfd';
      '�':fdat:='DAD0ibaabbbaab';
      '�':fdat:='JCT0pbndldldldldldldldnb';
		end;
		e:=ord(fdat[1])-65;
		h:=ord(fdat[2])-65;
		l:=ord(fdat[3])-65;
    n:=fdat[4];
		xx:=0;
		yy:=h;
		w:=4;
		repeat
			inc(w);
      for c:=1 to ord(fdat[w])-96 do
      begin
      	if n='1' then
        case textcolor of
          0..255:putpixel(textx+xx,texty+yy,textcolor);
          256:if getpixel(textx+xx,texty+yy)>0 then putpixel(textx+xx,texty+yy,getpixel(textx+xx,texty+yy)-1);
          257:if getpixel(textx+xx,texty+yy)<4 then putpixel(textx+xx,texty+yy,getpixel(textx+xx,texty+yy)+1);
          258:if getpixel(textx+xx,texty+yy)>3 then putpixel(textx+xx,texty+yy,getpixel(textx+xx,texty+yy)-4);
          259:if getpixel(textx+xx,texty+yy)<27 then putpixel(textx+xx,texty+yy,getpixel(textx+xx,texty+yy)+4);
          1000..1255:putpixel(textx+xx,texty+yy,textcolor+yy-h-1000);
          2000..2255:putpixel(textx+xx,texty+yy,textcolor-yy-h-2000);
          3000..3255:putpixel(textx+xx,texty+yy,textcolor+(yy-h)*2-3000);
          4000..4255:putpixel(textx+xx,texty+yy,textcolor-(yy-h)*2-4000);
          5000..5255:putpixel(textx+xx,texty+yy,textcolor+(yy-h)*3 div 2-5000);
          6000..6255:putpixel(textx+xx,texty+yy,textcolor-(yy-h)*3 div 2-6000);
          7000..7255:putpixel(textx+xx,texty+yy,textcolor+(yy-h) div 2-7000);
          8000..8255:putpixel(textx+xx,texty+yy,textcolor-(yy-h) div 2-8000);
        end;
        inc(yy);
			  if yy>l then
			  begin
				  yy:=h;
				  inc(xx);
        end;
			end;
      if n='1' then n:='0' else n:='1';
		until w=length(fdat);
		textx:=textx+e+space+1;
    case textstring[t] of
      '�','�','�','�','�','�','�','�':
      begin
        d:=textx;
        case textstring[t] of
          '�','�','�':textx:=textx-e-space;
          '�','�','�':textx:=textx-e-space-1;
          '�':textx:=textx-e-space-4;
          '�':textx:=textx-space-2;
        end;
        case textstring[t] of
          '�','�','�','�','�':textstring[t]:='�';
          '�':textstring[t]:='�';
          '�':textstring[t]:='�';
          '�':textstring[t]:='�';
        end;
        dec(t);
      end;
    end;
    if textstring[t] in ['�','�','�','�'] then
    begin
      textx:=d;
      textstring[t]:=' ';
    end;
    inc(t);
	end;
end;

procedure font18_4;

var
	d,t,e,h,l,xx,yy,w,c:integer;
	fdat:string;
  n:char;

begin
  dec(texty);
  t:=1;
	while t <= length(textstring) do
	begin
		fdat:='NGY0a';
		case textstring[t] of
      'a','�','�','�':fdat:='MGY0cagedechcebjadcodddgdcffccgfccggbcgcararbqdo';
      'b':fdat:='NBY0ava�ccjefbmcecmcecmcedkdefhdgphoklnhf';
      'c':fdat:='MGY0ggilfndpbffebdjgmfmfmglhjcbehdccgdc';
      'd':fdat:='NBY0khnmipgqgeheedkdecmcebncebnkl�cu';
      'e','�','�':fdat:='NGY0fhileocqbdcjadddcgecefddefdcfgccfmfbbhfccffcededc';
      'f':fdat:='JBY0haubvbttbva�ackhbcpcbcp';
      'g':fdat:='NDY0qcheeefhbgdrcnbccddfbcccfdccccfdbcdddebccmccbnccadaiddaccefdbapab';
      'h':fdat:='MBY0a�bdlbfcubucudjeesesfrimb';
      'i':fdat:='EAY0grabcwbwbsabqe';
      '�':fdat:='EAY0grfsfsfste';
      'j':fdat:='HAY0vbvdvcudabcwbradbqcbcod';
      'k':fdat:='MBY0b�ikldrfpilphfcjedgheakgtd';
      'l':fdat:='EBY0bua�ne';
      'm':fdat:='VGY0a�kcacpbpcpdj�arapccpbpcpdoejwarbqelb';
      'n':fdat:='MGY0a�kcacpbpcpdj�ardmb';
      'o','�','�':fdat:='NGY0fgimeocqbegeadkgmfmgkjgebqcoelihf';
      'p':fdat:='NGY0b�haedibecicdchdddfdfddeflgkiikfj';
      'q':fdat:='NGY0cfliikhlgddeedfdechddcicdcibedhae�ar';
      'r':fdat:='KGY1ra�adjdabpcpcpeneoco';
      's':fdat:='MGY0mdeedfcgedbieoegbeefdedfdedgdebhdjbddibcfgmec';
      't':fdat:='IDY0ccscspd�afifdcmaecp';
      'u','�':fdat:='MGY0bmdra�jdpcpbpcack�b';
      'v':fdat:='NGY1fmkhmfolinfnfnelfjhbodnekhgl';
      'w':fdat:='RGY1hklgodqlimelfbodneodqlhnfkgapcnelgij';
      'x':fdat:='MGY1cljfoc{amjgmfmgimchaqdmggne';
      'y':fdat:='NGY1fjlgneocdfgbcijjhlfmfkgdmflgjifm';
      'z':fdat:='LGY0ogikgkfmdocgcfbgdmflgjhjihna';
      'A':fdat:='RBY0milngqfrdtcoccbfdefaafdfheeehefehefeifdegabgbeeccudtfrhpkla';
      'B':fdat:='PBY0drb�cedkddfjcdgjcdgjbegsdoavcmafekcdgjpfc';
      'C':fdat:='QBY0hhmniqfsdubhfhbfjmlkmknjnjnkmeafkecekedbkfqed';
      'D':fdat:='QBY0dqc�klmknkmlleagjfbhfhbuesfqinnhe';
      'E':fdat:='OBY0gqb�cedlcdfjddfjcefjcefjceelbcglkkngtc';
      'F':fdat:='PBY0e�ceejddjeddkeddkecekecekecekfbbnfresbv';
      'G':fdat:='RBY0ihmmjpgresdgggbfkebellmjnjnjgaekgbdleselbedmccelqfc';
      'H':fdat:='RBY0fra�dffegeseresekbfekeceipe�cugpa';
      'I':fdat:='FBY0era�ne';
      'J':fdat:='JBY0tcrfsfskm�awavcqf';
      'K':fdat:='QBY0era�geecjereqfpjmmhhavdrgpimmhse';
      'L':fdat:='NBY0jmeta�hlngqeseserfqgqguc';
      'M':fdat:='VBY0cta�lmpaahqjpmnllljljjmgpfphl�bsc';
      'N':fdat:='RBY0bua�keahqhriqjpkomadh�csb';
      'O':fdat:='TBY0khmmipfsdtchggagjfaemjnjnjnjnkmeagjfbhgfdtesgpjmnhe';
      'P':fdat:='PBY0bua�erfefgeeieedjeddkfbdlllkniohrdq';
      'Q':fdat:='UBZ0hhnnjphsetdhggcfkebflfaehbdeaehdbeaeidaeaejiafiibejgcgghdtetftgshidevba';
      'R':fdat:='QBY0bua�dseffgdfiedfiechhfbkekakajckaidjaghhcdkftd';
      'S':fdat:='PBY0dedhehdhcjffbkfealfkcdgidefiedfieeeifdejeecfaedmbeelccgjniqee';
      'T':fdat:='RBY0ddrfqfresereseema�kkqgtesesfscs';
      'U':fdat:='RBY0cmhseucvbwaejhsfseseseserkkgawavbucshmh';
      'V':fdat:='QBY0aklphrftducdiirgsfsereaaofbdiibtdseqgokjl';
      'W':fdat:='UBY1mkqgtdvbxoipfogdseqgrfseuqiqhmjavbtdrfoijn';
      'X':fdat:='QBY0tgnnhqftcjasfpmiqgrgpjjsakavcsgojhse';
      'Y':fdat:='RBY1friokmllmgdfhcghpjnkmjhogpfpgkkmjniogqes';
      'Z':fdat:='QBY0tfoljohpfqfresdfdjcferfphoininjlklogta';
      '0':fdat:='OBY0gjkpgsdubgifaeniphphpineafjfbvctfpkjg';
      '1':fdat:='GBY0bavcu�oe';
      '2':fdat:='NBY0dbmebfigaggiaehiaeheadadidbdadhdcdadgdddadfedjdefdalgdakhdbieabdeeheb';
      '3':fdat:='NBY0uabcqcaendaegaejfcfiedghdeghcfghbhfsddaiblagdkbfficckee';
      '4':fdat:='PBY0leqgoinjmkkhadjgcdifedhffdakgperdsdkgadcdetdtdg';
      '5':fdat:='NBY0ubhgedcldeandtekdcghedghedgheefhefddadenadflbefjpfe';
      '6':fdat:='OBY0himnhretcebedebecdfeaddchhddhhddhhdefidfdeaednademcaijoge';
      '7':fdat:='MBY1fresdkfcdhkadfrescgeibfhnkbahmabgqfres';
      '8':fdat:='OBY0pegeehdibjbvbocjcgeheefhfdfhfdficgedaoceavchcieeehred';
      '9':fdat:='OBY0efpjiaclfdameeaecfdjeeehgdehgdehgdddaefcedbeddcecterhokii';
      '.':fdat:='GSY0bcceauaeccb';
      ',':fdat:='GT[0cafbcaavafdcc';
      ':':fdat:='GGY0bciccegeagenenegaegeccicb';
      ';':fdat:='GG[0bckaeeibchgnfofgbegfdcidc';
      '!':fdat:='GBY1jicbndeaobvbnjkneabqcb';
      '?':fdat:='KBY0bbocceedcebdeebkefbkefbkdgceaebffcblmjohqfo';
      '(':fdat:='JB[0hmjtdwbhlcbfsfteudvdxbv';
      ')':fdat:='JB[0vbxdvduetfsfbclhbwdtjmh';
      '<':fdat:='RGX0hcndndmflfkcbcjcbcjbdchcdchcdcgcfcfcfcechcdchcdbjcbcjcbcjcaclc';
      '>':fdat:='RGX1clcacjcbcjccbjccchcdchcecfcfcfcgcdchcdcibdcicbcjcbckflfmdndocg';
      '+':fdat:='SGY0hcpcpcpcpcpcpcpch�hcpcpcpcpcpcpcpch';
      '-':fdat:='HOR0a~a';
      '=':fdat:='SKT1cdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdc';
      '*':fdat:='MBM0dbicdaebcbebbdebacadadchddaegbacecbcdbdadcjbf';
      '/':fdat:='IBZ0vcsfoimhmimhmhpfsbw';
      '&':fdat:='YBY0ofqhnkddelbgbddeamgcalhgcggfehefeicgeoadbqajcicheibgejddekmeafkddekceejdeejcfdkcedlcuducj';
      '"':fdat:='CDL1{';
      '�':fdat:='PB]0dxbza�vewewecdgedebegedndfdiandhcmefekgdgjtfc';
      '�':fdat:='LAD0abahabrbahab';
      '�':fdat:='LAE0pcaacbcbcaac';
      '�':fdat:='ND\0wbudsfqfqfqfqfqfqfqfqfqfretc';

		end;
		e:=ord(fdat[1])-65;
		h:=ord(fdat[2])-65;
		l:=ord(fdat[3])-65;
    n:=fdat[4];
		xx:=0;
		yy:=h;
		w:=4;
		repeat
			inc(w);
      for c:=1 to ord(fdat[w])-96 do
      begin
      	if n='1' then
        case textcolor of
          0..255:putpixel(textx+xx,texty+yy,textcolor);
          256:if getpixel(textx+xx,texty+yy)>0 then putpixel(textx+xx,texty+yy,getpixel(textx+xx,texty+yy)-1);
          257:if getpixel(textx+xx,texty+yy)<4 then putpixel(textx+xx,texty+yy,getpixel(textx+xx,texty+yy)+1);
          258:if getpixel(textx+xx,texty+yy)>3 then putpixel(textx+xx,texty+yy,getpixel(textx+xx,texty+yy)-4);
          259:if getpixel(textx+xx,texty+yy)<27 then putpixel(textx+xx,texty+yy,getpixel(textx+xx,texty+yy)+4);
          1000..1255:putpixel(textx+xx,texty+yy,textcolor+yy-h-1000);
          2000..2255:putpixel(textx+xx,texty+yy,textcolor-yy-h-2000);
          3000..3255:putpixel(textx+xx,texty+yy,textcolor+(yy-h)*2-3000);
          4000..4255:putpixel(textx+xx,texty+yy,textcolor-(yy-h)*2-4000);
          5000..5255:putpixel(textx+xx,texty+yy,textcolor+(yy-h)*3 div 2-5000);
          6000..6255:putpixel(textx+xx,texty+yy,textcolor-(yy-h)*3 div 2-6000);
          7000..7255:putpixel(textx+xx,texty+yy,textcolor+(yy-h) div 2-7000);
          8000..8255:putpixel(textx+xx,texty+yy,textcolor-(yy-h) div 2-8000);
          9000..9255:if (xx+yy)/2=(xx+yy) div 2 then
           putpixel(textx+xx,texty+yy,(textcolor-9000-yy) div 7) else
           putpixel(textx+xx,texty+yy,(textcolor-8997-yy) div 7);
        end;
        inc(yy);
			  if yy>l then
			  begin
				  yy:=h;
				  inc(xx);
        end;
			end;
      if n='1' then n:='0' else n:='1';
		until w=length(fdat);
		textx:=textx+e+space+1;
    case textstring[t] of
      '�','�','�','�','�','�','�','�':
      begin
        d:=textx;
        case textstring[t] of
          '�','�':textx:=textx-e-space+1;
          '�','�','�':textx:=textx-e-space;
          '�':textx:=textx-e-space-1;
          '�':textx:=textx-e-space-4;
          '�':textx:=textx-space-3;
        end;
        case textstring[t] of
          '�','�','�','�','�':textstring[t]:='�';
          '�':textstring[t]:='�';
          '�':textstring[t]:='�';
          '�':textstring[t]:='�';
        end;
        dec(t);
      end;
    end;
    if textstring[t] in ['�','�','�','�'] then
    begin
      textx:=d;
      textstring[t]:=' ';
    end;
    inc(t);
	end;
end;

begin
	writeln('SMFONT18 - font by Remco de Korte - Soft Machine');
	delay(100);
end.
