
program demo30;

uses
{$IFDEF unix}
  cThreads,
{$ENDIF}
  ptcGraph, ptcCrt,
  //crt,
  smfont30, smgraf {, graph};

var
  i:integer;

const
  tc1:tctype30=(1,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15);
  tc2:tctype30=(15,14,13,12,11,10,9,8,7,6,5,4,3,2,1,1);

begin
  initgraf;
  for i:=1 to 15 do setpal(colnum[i],32+i*2,i*4,i);
  font30_4('SMFONT30',10,0,1,0,tc1);
  font30_3('Remco de Korte',10,40,5,0,tc1);
  font30_3('Soft Machine 1997',10,60,5,0,tc1);
  font30_4('abcdefghijklmnopqrstuvwxyz',10,120,5,0,tc2);
  font30_3('ABCDEFGHIJKLMNOPQRSTUVWXYZ',10,160,5,0,tc2);
  font30_2('0123456789.,:;!?()<>+-=*/&"''',10,200,5,0,tc2);
  readkey;
  closegraph;
end.
