unit SMFONT07;

{$MODE DELPHI}

interface

uses
  //crt,graph;
  ptcGraph, ptcCrt;

procedure font07(textstring:string;textx,texty,textcolor,space:integer);

implementation

procedure font07;

var
  y,s,t:integer;
  fdat:string;

begin
  for t:=1 to length(textstring) do
  begin

    fdat:='';
    case textstring[t] of
      ' ':inc(textx,10);
      'a','A':fdat:='hh,hh,gi,gi,fj,fj,ek,ek,dl,dl,cm,cm,bn,bn,ao,q';
      'b','B':fdat:='af,ah,ai,ai,ai,ai,ah,aj,ak,al,al,al,al,ak,ai,n';
      'c','C':fdat:='fk,dm,cl,bk,bj,ai,ah,ag,ah,ai,bj,bk,cl,dm,fk,o';
      'd','D':fdat:='ag,ai,aj,ak,ak,al,al,al,al,al,ak,ak,aj,ai,ag,n';
      'e','E':fdat:='ah,ag,af,ae,ad,ac,ab,aade,ab,ac,ad,ae,af,ag,ah,k';
      'f','F':fdat:='ah,ag,af,ae,ad,ac,ab,aade,aa,aa,aa,aa,aa,aa,aa,k';
      'g','G':fdat:='fk,dm,cl,bk,bj,ai,ah,ag,am,am,bm,bm,cm,dm,fk,o';
      'h','H':fdat:='aaii,aaii,abhi,abhi,acgi,acgi,adfi,ai,adfi,acgi,acgi,abhi,abhi,aaii,aaii,l';
      'i','I':fdat:='cc,cc,cc,cc,cc,cc,cc,cc,cc,cc,cc,cc,cc,cc,cc,e';
      'j','J':fdat:='ii,ii,ii,ii,ii,ii,ii,hi,gi,fi,ei,dh,ch,bg,ae,k';
      'k','K':fdat:='aaii,aahi,aagi,aafi,aaei,aadi,aaci,ai,aaci,aadi,aaei,aafi,aagi,aahi,aaii,l';
      'l','L':fdat:='aa,aa,aa,aa,aa,aa,aa,aa,ab,ac,ad,ae,af,ag,ah,j';
      'm','M':fdat:='aaoo,aaoo,abno,abno,acmo,acmo,adlo,adlo,aeko,aeko,afjo,afjo,agio,agio,ao,r';
      'n','N':fdat:='aaii,aaii,abii,abii,acii,acii,adii,adii,aeii,aeii,afii,afii,agii,agii,ai,l';
      'o','O':fdat:='fj,dl,cm,bn,bn,ao,ao,ao,ao,ao,bn,bn,cm,dl,fj,q';
      'p','P':fdat:='ae,ag,ah,ah,ah,ag,ae,aa,aa,aa,aa,aa,aa,aa,aa,j';
      'q','Q':fdat:='fj,dl,cm,bn,bn,ao,ao,ao,ao,ao,bn,bn,cm,dlmn,fjno,q';
      'r','R':fdat:='ae,ag,ah,ah,ah,ag,ae,aa,ab,ac,ad,ae,af,ag,ah,j';
      's','S':fdat:='cf,bg,ah,ag,af,be,cd,de,ef,dg,ch,bh,ah,bg,cf,j';
      't','T':fdat:='ao,cm,ek,gi,hh,hh,hh,hh,hh,hh,hh,hh,hh,hh,hh,q';
      'u','U':fdat:='ao,ao,ao,ao,ao,ao,ao,ao,ao,ao,bn,bn,cm,dl,fj,q';
      'v','V':fdat:='ao,bn,bn,cm,cm,dl,dl,ek,ek,fj,fj,gi,gi,hh,hh,q';
      'w','W':fdat:='aq,bp,bp,bp,co,co,co,dhjn,dhjn,dhjn,egkm,egkm,egkm,ffll,ffll,s';
      'x','X':fdat:='ai,bh,bh,cg,cg,df,df,ee,df,df,cg,cg,bh,bh,ai,k';
      'y','Y':fdat:='ak,ak,bj,bj,ci,ci,dh,dh,eg,eg,ff,ff,ff,ff,ff,m';
      'z','Z':fdat:='ah,hh,gh,gh,fh,fh,eh,eh,dh,dh,ch,ch,bh,bh,ah,j';
      '0':fdat:='eg,ci,bj,bj,ak,ak,ak,ak,ak,ak,ak,bj,bj,ci,eg,m';
      '1':fdat:='cc,cc,bc,bc,ac,ac,cc,cc,cc,cc,cc,cc,cc,cc,cc,e';
      '2':fdat:='dg,bi,aj,bk,ck,dj,ei,fh,gg,ff,ee,dd,cc,bb,ak,m';
      '3':fdat:='dg,bi,aj,bk,ck,dk,ej,fi,ej,dk,ck,bk,aj,bi,dg,m';
      '4':fdat:='gg,gg,fg,fg,eg,eg,dg,dg,cg,cg,bg,bg,ah,gg,gg,j';
      '5':fdat:='ak,aa,aa,aa,aaeg,aaci,aj,aj,ak,ak,ak,bj,bj,ci,eg,m';
      '6':fdat:='gg,ff,ee,dd,cceg,bi,bj,aj,ak,ak,ak,bj,bj,ci,eg,m';
      '7':fdat:='ak,cj,ej,gi,ii,hh,hh,gg,gg,ff,ff,ee,ee,dd,dd,m';
      '8':fdat:='eg,ci,bj,bj,ak,ak,bj,ci,bj,ak,ak,bj,bj,ci,eg,m';
      '9':fdat:='eg,ci,bj,bj,ak,ak,ak,bk,bj,cj,egii,hh,gg,ff,ee,m';
    end;
    if textstring[t] in ['a'..'z','A'..'Z','0'..'9'] then
    begin
      s:=1;
      for y:=texty to texty+14 do
      begin
        case textcolor of
          0..255:setcolor(textcolor);
          1000..1255:setcolor(textcolor-1000+y-texty);
          2000..2255:setcolor(textcolor-1986-y+texty);
        end;
        line(textx+ord(fdat[s])-97,y,textx+ord(fdat[s+1])-97,y);
        inc(s,2);
        if fdat[s]<>',' then
        begin
          line(textx+ord(fdat[s])-97,y,textx+ord(fdat[s+1])-97,y);
          inc(s,2);
        end;
        inc(s,1);
      end;
      textx:=textx+ord(fdat[s])-97+space;
    end;
  end;
end;

begin
  writeln('SMFONT07 - Font by Remco de Korte - Soft Machine');
  delay(100);
end.
