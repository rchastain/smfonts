
SMFONTS are (mostly) non-scalable bitmap-fonts which can be used in Pascal programs in all sorts of graphic modes.

I created them for the programs I made in TurboPascal (see the download page for some examples).

You are FREE to use these units in any way you like (in a program), though I would appreciate it if you mentioned me in the credits of your program.

NOTE: I like to regard these units as my intellectual property, I hope you can respect that.

If you have any questions regarding this or if you intend to change the source and distribute your own versions please contact me at: emcodek@xs4all.nl
