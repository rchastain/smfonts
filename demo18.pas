uses
{$IFDEF unix}
  cThreads,
{$ENDIF}
  ptcGraph, ptcCrt,
  //crt,
  smfont18, smgraf {, graph};

var
  i:integer;

begin
  initgraf;
  for i := 1 to 15 do setpal(colnum[i],i*4,i*4,i*4);
  font18_4('SMFONT18',0,0,8013,4);
  font18_2('Font by Remco de Korte - Soft Machine',0,32,7005,2);
  font18_1('updated nov. 1996',0,48,5005,1);
  font18_3('abcdefghijklmnopqrstuvwxyz',0,120,8015,3);
  font18_3('ABCDEFGHIJKLMNOPQRSTUVWXYZ',0,160,8015,3);
  font18_3('0123456789!?.,:;+-=()<>*/&"',0,200,8015,3);
  font18_1('Press a key...',0,280,12,2);
  readkey;
  closegraph;
end.
