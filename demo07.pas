uses
{$IFDEF unix}
  cThreads,
{$ENDIF}
  ptcGraph, ptcCrt,
  //crt,
  smfont07, smgraf {, graph};

var
  i:integer;

begin
  initgraf;
  for i:=0 to 14 do setpal(colnum[15-i],63-i*7 div 2,63-i*7 div 2,63-i*7 div 2);
  font07('SMFONT07',0,0,1001,1);
  font07('by Remco de Korte - Soft Machine',0,40,15,1);
  font07('updated oct. 1996',0,60,15,1);
  font07('abcdefghijklmnopqrstuvwxyz',0,100,2001,1);
  font07('0123456789!?.,:;+-=*()$&%',0,120,2001,1);
  font07('Press a key...',1,161,1001,1);
  font07('Press a key...',0,160,2001,1);
  readkey;
  closegraph;
end.

