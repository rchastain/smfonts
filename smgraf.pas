
unit smgraf;

{$MODE DELPHI}

interface

var
  key: char;
  fadepal: array[0..15, 1..3] of {byte}smallint;
  colnum: array[0..15] of byte;

const
  colnum1: array[0..15] of byte = (0, 1, 2, 3, 4, 5, 20, 7, 56, 57, 58, 59, 60, 61, 62, 63);
  colnum2: array[0..15] of byte = (0, 1, 2, 3, 4, 5,  6, 7, 16, 17, 18, 19, 20, 21, 22, 23);
  vgapal: array[0..15, 1..3] of byte = (
    ( 0,  0,  0), ( 0,  0, 40), (16, 40,  0), ( 0, 40, 40), (40,  0,  0), (40,  0, 40), (40, 20,  0), (40, 40, 40),
    (20, 20, 20), (20, 20, 63), (32, 63, 16), (20, 63, 63), (63, 20, 20), (63, 20, 63), (63, 63, 32), (63, 63, 63)
  );

procedure getkey;
procedure emptykey;
procedure wait;
procedure initgraf;
procedure setpal(cn: {byte}smallint; r, g, b: {byte}smallint);
procedure getpal(cn: {byte}smallint; var r, g, b: {byte}smallint);
procedure swappal(s, d: {byte}smallint);
procedure fadeout;
procedure fadein;
procedure quickfadeout(z: integer);
procedure quickfadein(z: integer);
function radius(x, y: real): real;

implementation

uses
{
  crt, graph, bgidriv, dos;
}
  ptcGraph, ptcCrt;

type
  toggles = (rshift, lshift, ctrl, alt, scrolllock, numlock, capslock, insert);
  status = set of toggles;

var
  keystatus: status {absolute $40:$17} = [];

procedure getkey;
begin
  key := #255;
  if keypressed then key := readkey;
  if key = #0 then key := chr(ord(readkey) + 128);
end;

procedure emptykey;
begin
  repeat
    getkey;
  until key = chr(255);
end;

procedure wait;
begin
  emptykey;
  repeat
    getkey;
  until key <> chr(255);
  if key <> chr(27) then emptykey;
end;

procedure initgraf;
var
  gd, gm: smallint;
begin
{
  if RegisterBGIdriver(@EGAVGADriverProc) < 0 then halt;
}
  gd := 9;
  gm := 2;
  initgraph(gd, gm, '');
  for gd := 0 to 15 do colnum[gd] := colnum1[gd];
end;

procedure setpal(cn: {byte}smallint; r, g, b: {byte}smallint); {assembler;}
begin
  SetRGBPalette(cn, r shl 2, g shl 2, b shl 2);
end;

procedure getpal(cn: {byte}smallint; var r, g, b: {byte}smallint);
var
  rr, gg, bb: {byte}smallint;
begin
  GetRGBPalette(cn, rr, gg, bb);
{
  r := rr;
  g := gg;
  b := bb;
}
  r := rr shr 2;
  g := gg shr 2;
  b := bb shr 2;
end;

procedure swappal;
var
  r, g, b: {byte}smallint;
begin
  getpal(d, r, g, b);
  setpal(s, r, g, b);
end;

procedure fadeout;
var
  i, j: integer;
begin
  for i := 0 to 15 do
    getpal(colnum[i], fadepal[i, 1], fadepal[i, 2], fadepal[i, 3]);
  for j := 63 downto 0 do
  begin
    for i := 0 to 15 do
      setpal(colnum[i], fadepal[i, 1] * j div 63, fadepal[i, 2] * j div 63, fadepal[i, 3] * j div 63);
    delay(10);
  end;
end;

procedure fadein;
var
  i, j: integer;
begin
  for j := 0 to 63 do
  begin
    for i := 0 to 15 do
      setpal(colnum[i], fadepal[i, 1] * j div 63, fadepal[i, 2] * j div 63, fadepal[i, 3] * j div 63);
    delay(10);
  end;
end;

procedure quickfadeout;
var
  i, j: integer;
begin
  for i := 0 to 15 do
    getpal(colnum[i], fadepal[i, 1], fadepal[i, 2], fadepal[i, 3]);
  for j := z downto 0 do
  begin
    for i := 0 to 15 do
      setpal(colnum[i], fadepal[i, 1] * j div z, fadepal[i, 2] * j div z, fadepal[i, 3] * j div z);
    delay(10);
  end;
end;

procedure quickfadein;
var
  i, j: integer;
begin
  for j := 0 to z do
  begin
    for i := 0 to 15 do
      setpal(colnum[i], fadepal[i, 1] * j div z, fadepal[i, 2] * j div z, fadepal[i, 3] * j div z);
    delay(10);
  end;
end;

procedure oudpalet;
var
  i: byte;
begin
  for i := 0 to 15 do
    setpal(colnum[i], vgapal[i, 1], vgapal[i, 2], vgapal[i, 3]);
end;

{
function shiftstate: byte;
var
  regs:registers;
begin
  regs.ah:=2;
  intr($16,regs);
  shiftstate:=regs.al;
end;
}

function readnumlock: boolean;
begin
  readnumlock := {(shiftstate and 32) <> 0}FALSE;
end;

function radius(x, y: real): real;
var
  tmp: real;
begin
  tmp := sqrt(x * x + y * y);
  radius := tmp;
end;

begin
  randomize;
  if readnumlock then
    keystatus := keystatus - [numlock];
end.
