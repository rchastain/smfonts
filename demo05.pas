
uses
{$IFDEF unix}
  cThreads,
{$ENDIF}
  ptcGraph, ptcCrt,
  //crt,
  smfont05, smgraf {, graph};

begin
  initgraf;
  font05_4('SMFONT05', 0, 0, 15, 4);
  font05_3('Font by Remco de Korte - Soft Machine', 0, 32, 15, 3);
  font05_2('updated oct. 1996', 0, 64, 15, 2);
  font05_3('abcdefghijklmnopqrstuvwxyz', 0, 160, 15, 3);
  font05_3('ABCDEFGHIJKLMNOPQRSTUVWXYZ', 0, 192, 15, 3);
  font05_4('0123456789!?.,:;+-=()��ၔ', 0, 224, 15, 4);
  font05_1('Press a key...', 0, 288, 15, 4);
  readkey;
  closegraph;
end.
