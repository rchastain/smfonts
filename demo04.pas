
uses
{$IFDEF unix}
  cThreads,
{$ENDIF}
  ptcGraph, ptcCrt,
  //crt,
  smfont04, smgraf {, graph};

begin
  initgraf;
  setpal(colnum[9], 32, 32, 32);
  setpal(5, 20, 0, 0);
  font04('SMFONT04', 0, 0, 7, 15, 8, 5);
  font04('Font by Remco de Korte - Soft Machine', 0, 40, 9, 7, 8, 4);
  font04('updated oct. 1996', 0, 80, 9, 7, 8, 3);
  font04('abcdefghijklmnopqrstuvwxyz', 0, 120, 7, 15, 8, 6);
  font04('ABCDEFGHIJKLMNOPQRSTUVWXYZ', 0, 180, 7, 15, 8, 5);
  font04('0123456789!?.,:;+-=*()&', 0, 240, 7, 15, 8, 7);
  font04('ABC', 320, 320, 9, 7, 8, 20);
  font04('Press a key...', 0, 450, 4, 12, 5, 4);
  readkey;
  closegraph;
end.
