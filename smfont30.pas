
unit smfont30;

{$MODE DELPHI}

interface

uses
  //crt, graph;
  ptcGraph, ptcCrt;

type
  tctype30=array[1..16] of byte;

procedure font30_1(textstring:string;textx,texty,space,colmode:integer;tc:tctype30);
procedure font30_2(textstring:string;textx,texty,space,colmode:integer;tc:tctype30);
procedure font30_3(textstring:string;textx,texty,space,colmode:integer;tc:tctype30);
procedure font30_4(textstring:string;textx,texty,space,colmode:integer;tc:tctype30);

implementation


procedure font30_1;
var
	t,xx,yy,i,j{,k},q:integer;
	fdat:array[1..13] of string;
  width:byte;

procedure incxx;
begin
  inc(xx);
  if xx>textx+width then
  begin
    xx:=textx;
    inc(yy);
  end;
end;

procedure putpix(col:byte);
begin
  case colmode of
    0:putpixel(xx,yy,tc[col]);
    1:if odd(xx+yy) then putpixel(xx,yy,tc[(1+col) div 2]) else putpixel(xx,yy,tc[(2+col) div 2]);
    2:if odd(xx+yy) then putpixel(xx,yy,tc[(2+col) div 3]) else putpixel(xx,yy,tc[(3+col) div 3]);
  end;
  incxx;
end;

begin
	for t:=1 to length(textstring) do
	begin
    for i:=1 to 13 do fdat[i]:='';
    case textstring[t] of
      'a':fdat[1]:='F41ggfff20fg0Eedd1ddcc1cc0bbaba';
      'b':fdat[1]:='G210ggf20gf20hF1fe1ed0ed1dc0dc1cb0cb1b0b0aaa';
      'c':fdat[1]:='F42fffgee0defdc2cc2cbb1c0baaa';
      'd':fdat[1]:='H32fff21gf10hhggg0fff1ff0ee10ee0dd10dd0cc10cc1Baab';
      'e':fdat[1]:='F410gfgh0gf1ghfeddefdc2ccb1c0babb';
      'f':fdat[1]:='E21gfeegg0eeggf1feed0ddc10cc10cbb0baaa';
      'g':fdat[1]:='G421Ghhfg1gh1G1ff20efEedd10ddccc1bb0bbaaa';
      'h':fdat[1]:='I30fff210gg210ghG10ff10ff1fe10ee1ed10dd1dc10cc0bbb10bbb';
      'i':fdat[1]:='D0dc1dc20gfd1ed1dc1cc1cb0babb';
      'j':fdat[1]:='C0cc0cc10edd0dd0dd0dc0cc0bb0bbaaa';
      'k':fdat[1]:='I30ggg210hh210hi0iihh1gg0gg2Ff2Eed10cddC0bbb1B';
      'l':fdat[1]:='D2edd1dd1ee1dd1dc1cc1cb0babb';
      'm':fdat[1]:='L52hhggijjhggh1ff1hh1fg1ee1ff1ee1dd1ee1dd1cc1cc1cc0bba1bb1bbb';
      'n':fdat[1]:='I4310hhGf10ff10ee1ee10dd1dd10cc1cc10bb0abb10aab';
      'o':fdat[1]:='G4210gffh1f1eggee10fgdd10eeccb1c1baab';
      'p':fdat[1]:='H43hihG1gg1fff0gg10fe0ee10ed0dd10dc0dCb1bb20baaa';
      'q':fdat[1]:='H430iHh0ggg1gg0fgg1gg0eff1ff0ddd1de1Ccc21bb20baab';
      'r':fdat[1]:='F41igffef0fedde0ed0cd0dc2cb10cbaa';
      's':fdat[1]:='E4gghifee0gedde1dcddc0bccbaab';
      't':fdat[1]:='E310fe1hfede0ed10dc10cc10bb10baaa';
      'u':fdat[1]:='I4310efg10gg1ef10fe1de10ed1dd10dc1cc10cc10Baaa';
      'v':fdat[1]:='G420igfe0ef0fed0d1edccc10cbb2baa20a';
      'w':fdat[1]:='K50fgh0iiih0ff0fg1hgg0e1eefffeedc10ddd0dcc2ccb0bba2ab10a';
      'x':fdat[1]:='G420G0gg0ffeee10edd2ddc10c0ccc0bb0babb';
      'y':fdat[1]:='G420hgFg0ffe0f1fE10ddd2dcc2ccb1bb0b10aaa';
      'z':fdat[1]:='F41hggF0eee10edd1ddc10ccc0Baaa';
      'A':fdat[1]:='J2hg210H21I20Hgg2fg0gfe2Eedc1cc10ccc0B0bbabb';
      'B':fdat[1]:='Hhhihhh10hii0hi1iii0ii1Hgh1fgf0fff0eee0edd0ddc0ccBbaaa';
      'C':fdat[1]:='H1ggffe1gf1fechgf10edgfe20eed20ddcc20ccb10b1bbaaa';
      'D':fdat[1]:='Igghiiih10ghi0iig1hii1ihh0ggh1gff0fff1fee0eee1ddd0ddc0ccb0Baaa';
      'E':fdat[1]:='HihhGg0hgg1gg0hhh0g10gggff10efe0e10ded10d0cdc1ccBbabb';
      'F':fdat[1]:='HhhGgg0gff1gg0gff0f10fE10edd0d10dcc20cbb2bbaaa';
      'G':fdat[1]:='I1iihhge1gh1hgf0fgh10gf0ffg21eff0ffeeD1ddc1ccc0cbb10bbaaa';
      'H':fdat[1]:='Ihijjjiihh0hji0iig1ijj0jih1hiihhhg1fgg0gfe1eee0eed1ddd0dcc0Bbbabb';
      'I':fdat[1]:='Egeeef0eee1fef1eee1ddd1dcc1cbb0bbaab';
      'J':fdat[1]:='G1Gg10ggg2ggg2gff0gfE0edddcc0cc0bbb1baaa';
      'K':fdat[1]:='JI0iigf1iii0ii2iijji20Hhh2fGf2Eeed10ccd0dccb0B1babb';
      'L':fdat[1]:='GfE10eee2gff2fee2eed2ddd2dcc1cBabb';
      'M':fdat[1]:='Kggi2jihh0hij10jih1hij1ljih1ghijjihgg1ffghhgffe1ddEddd1c0ccc0cbb0aa1b1babb';
      'N':fdat[1]:='Ihgh10hgg0ghh10g1hihhh0f1fgggffe1e0eeedc1d1ddcb1c10cbb0abb10aa';
      'O':fdat[1]:='I1Ih10gh1ihf0fgh1ihggefg10gffdee10eeecddd1ddd0ccc1bb10bbaaa';
      'P':fdat[1]:='Hiiihhhi1hhh0ghh0hhg0ghh0gff0fff0eeD1ccc20cbb2bbaaa';
      'Q':fdat[1]:='I1Ih10hh1ihf0ghi1ihggghh10hggfgh10hggefgg1gff0eff1ee0Dddc2cccbb0b2baaa';
      'R':fdat[1]:='IjjIh10iii0iih1ijj0iih1hhh0hgg1gggfff10eee0edd1ddd0ccc0Bb0aab';
      'S':fdat[1]:='G0iH0hhh1hgiihh1ghgggff1ffeeede1dddccc1cbb0bbaaa';
      'T':fdat[1]:='I0gghG0gf0gff0ghgf0fff0gh10eed21ddd21ccc21cbb20bbaaa';
      'U':fdat[1]:='IGg0fed0fgf10e1fgf10e1efe10d1ded10d1ddc10c1ccb1b2bbaaa';
      'V':fdat[1]:='IG1gff0ggf1ff1ffe1e2eeddd2dC20bbb21baa210a';
      'W':fdat[1]:='LghijK0hgf0ghi0jjj0gf1fhi0iii0ge10fghgggfe2effeeedc2cdd0ccc21cb1bb21ba1aa';
      'X':fdat[1]:='IG0gff1ggg0gf2hhhg20gggf21fff20Ed10cd0dccb0bbb0bbaaa';
      'Y':fdat[1]:='IfG0ggh0F0fg10fffef20eed21ddd21ccc21cbb20bbaab';
      'Z':fdat[1]:='G0gggffegg0gffeg1ggg10fff10feee10edd10ddcc0cBbaaa';
      '0':fdat[1]:='F210gfff0ff0ff0ee1eedd1ddcc1cc0bb0bb1aaa';
      '1':fdat[1]:='F3ddd0feed2dd2dc2cc2cb1bbbaab';
      '2':fdat[1]:='F210fffe0gf0F1eee10ddd1dcc10ccb0bbbaab';
      '3':fdat[1]:='G3ggff1gf0fff0f1fff2fef1eeddde2cdd2ccbbbaaa';
      '4':fdat[1]:='G310ff2ggg10G1gF1fe0ee0ed1dd0Cbbb2bb20aa';
      '5':fdat[1]:='F210Ffgfe10gfeee10edde10dcd10ccc10bb0baaa';
      '6':fdat[1]:='F1fe10fff10gf10gffee0ee1D1Cb0bb0baaa';
      '7':fdat[1]:='F21feeddeed1dd2dd10cc2cb2bb10ba2aa';
      '8':fdat[1]:='G0iihhi0hih1iihih1iiGgg1fffeffdd0Dcc1cbc0bbaaa';
      '9':fdat[1]:='F210hggf0ig0ffghg1ffgf1eeedd0dd0cccb10bbb1aaa';
      '.':fdat[1]:='C3210ccdbbcaab';
      ',':fdat[1]:='C3210dcccbb0bb1a0b';
      ':':fdat[1]:='C10edeedeecd10dcccbbbaa';
      ';':fdat[1]:='C10edeedddcd10dcccbb0bb1a0b';
      '!':fdat[1]:='C0ccecc0dc0c2dcbcbbbaa';
      '?':fdat[1]:='Feddcd20ef10def10c31ccc10bbb10aaa';
      '(':fdat[1]:='E10ef0eee1ed1fed1edd1ddd1ccc10bb10bbb2bb';
      ')':fdat[1]:='Efe2eed10ed10eee1ede1ddd1ccc1cb1bba0ba';
      '<':fdat[1]:='F310f10eee0ddc1dc2cbbb20bbb20a';
      '>':fdat[1]:='F21f20eed20ddc20cc1cBba10a';
      '+':fdat[1]:='E40c2c1Bb1a2a';
      '-':fdat[1]:='C32dccbbb';
      '=':fdat[1]:='F41eddccc21bbbaab';
      '*':fdat[1]:='E1d1e0d0ddC0bbb1a0b';
      '/':fdat[1]:='D1de1ce1c10d1cc1b10b1ba1aa';
      '&':fdat[1]:='J1hhi1gff0fg20fe0ghh10feeefgg0ffed0dd1eedc1cc1c0cc10bb10bb2baaa';
      '"':fdat[1]:='Cc0cb0ba0b';
      '''':fdat[1]:='Abaa';
    end;
    if fdat[1]<>'' then
    begin
      xx:=textx;
      yy:=texty;
      if textstring[t] in ['a'..'z'] then dec(yy);
      width:=ord(fdat[1,1])-65;
      j:=1;
      i:=2;
      repeat
        repeat
          case fdat[j,i] of
            '0':incxx;
            '1':for q:=1 to 2 do incxx;
            '2':for q:=1 to 4 do incxx;
            '3':for q:=1 to 8 do incxx;
            '4':for q:=1 to 16 do incxx;
            '5':for q:=1 to 32 do incxx;
            '6':for q:=1 to 64 do incxx;
            '7':for q:=1 to 128 do incxx;
            'a'..'o':putpix(ord(fdat[j,i])-96);
            'A'..'O':for q:=1 to 4 do putpix(ord(fdat[j,i])-64);
          end;
          inc(i);
        until i>length(fdat[j]);
        inc(j);
        i:=1;
      until fdat[j]='';
      textx:=textx+width;
    end else textx:=textx+16;
    inc(textx,space);
  end;
end;

procedure font30_2;
var
	t,xx,yy,i,j{,k},q:integer;
	fdat:array[1..13] of string;
  width:byte;

procedure incxx;
begin
  inc(xx);
  if xx>textx+width then
  begin
    xx:=textx;
    inc(yy);
  end;
end;

procedure putpix(col:byte);
begin
  case colmode of
    0:putpixel(xx,yy,tc[col]);
    1:if odd(xx+yy) then putpixel(xx,yy,tc[(1+col) div 2]) else putpixel(xx,yy,tc[(2+col) div 2]);
    2:if odd(xx+yy) then putpixel(xx,yy,tc[(2+col) div 3]) else putpixel(xx,yy,tc[(3+col) div 3]);
  end;
  incxx;
end;

begin
	for t:=1 to length(textstring) do
	begin
    for i:=1 to 13 do fdat[i]:='';
    case textstring[t] of
      'a':fdat[1]:='J531ijjjih10hi1kjih21lkih10ijjjihf0ffgh0hgfe0deef0feed0ccDccbb0B0aaa';
      'b':fdat[1]:='J1hii20hhiii20ghijj21hijk21ijkllkjh1ijkllkjhg0hijk0jihf0ghhi0hgfe0ffgg0ffed0deee0ddc1Ccbbb2baaa';
      'c':fdat[1]:='I5210jjihg1hijjjhgeghhi1hgefggg1gfdeeef20ddded10c0Cccbb1bbaaa';
      'd':fdat[1]:='K210ihg21jjihg21kjihg210kjhg10jkllkihf1ijkllkjhg0ghij1kjhg0fghi1ihgf0eefg1hgfe0ddee1feed1ccDccbb1bbb0baaa';
      'e':fdat[1]:='I521Jh10ijkjjig0ghi10ihggghihhgfeeFfeeD210cddCb1bbbaaa';
      'f':fdat[1]:='G0hhhgg0hhhgggeggg0ffefff2F10gF1gF10eee2edd2ddc10cccbb1bbaaa';
      'g':fdat[1]:='I521iihhgedghi1hgf0hhi1ihf0ghi1ihf1iijii10ghi21ghhihhgf0effgffeeddd2ddCcccbb10bbaaa';
      'h':fdat[1]:='L1hhi210ghhii210gghij3ghij3gijk0lkih10hijkmlkihf1gijk1kjhg1fghi1jhgf1efgh1hgfe1deef1feed0cccddd0ddccb0Bb0'+
      'bbaaa';
      'i':fdat[1]:='F1eee1feee1fee31ggf0hG0hhggg1gfff1feee1eddd0dCBaaa';
      'j':fdat[1]:='E1eee0feee0fee3ggfgggffggfff0gfff0ggff0gfff0feee0eddd0dCB0aaa';
      'k':fdat[1]:='L1ghh210gghhh210gghii3hijj3hikk1kihf1ijkl1kjhg1ijkl0mkj2hijjkkih2fghhiihgf10eeff0gfedd0dccddd0dccBB1baaa';
      'l':fdat[1]:='F1ggf0gggff0ggfff1gfff1gfff1gggf1hggg1gfff1feee1eddd0dCBaaa';
      'm':fdat[1]:='Q621hij0jii10jhg1gghijkjiii0jjhge0ffgi10iiij1igf1efg10ggg10gfe1eef10fff10fed1dde10eee10edc0Cd1C1ccBbbb1B1'+
      'baab';
      'n':fdat[1]:='L541ijl0lkih1hhijlmlkihf0hhijk1kjhg1ghij1jhgf1ffgh1hgfe1eeff1feed0dcD0ddccBbbb0bbaaa';
      'o':fdat[1]:='J5310kllki10ijlllkih0gijk0kjihffghi1ihfeeffg1gfeddE0eddc0Cccbb10bbaaa';
      'p':fdat[1]:='J531jkl0lji0hijkllljiggijkl0kjhg0hijk0jigf0iijj0jhge0iijj0igfd0Hgged1Feed10eddd20dCb2bbbaaa';
      'q':fdat[1]:='K5321jkllkihf1ijkllkihf0ghik1kjhg0fghj1jihg0efhi1jihg0dfgh1jiih1efghhhggf10defffeee210eddd21dCb20bbbaaa';
      'r':fdat[1]:='I521iji0ig0hIihgehhiihhgfd0gggf0fd1feee20eddd2dCb10cbbaaa';
      's':fdat[1]:='G4320jI0ijjiiihI0ihghhggg1gFeee0edddCcbb0bbaaa';
      't':fdat[1]:='G40gg2hhg1hhhgggfghGf0gggf10fffe10eeed10dddc10cccbbb1baaa';
      'u':fdat[1]:='L541ijk10ihf0ghijl0mkjhg0ggijk0mkjhg1fghi1jhgf1efgg1hgfe1deef1feed1ccDdccbb1B0baaa';
      'v':fdat[1]:='K532ghijjj0ihgefghiih0hgf1fG0ff2ffEd20dddcc21ccbbb210baa30a';
      'w':fdat[1]:='P6ghijk0mnnmk0ihgefghij0mmmlj0hgf1gghi1llki0gf2fgghijihgfed2eeefgggfedc21cddd0ddcbb21B1bba3aa2a';
      'x':fdat[1]:='K5320hijkkj0igfghijkkjjhg10jkkkjjh20ijjih20ghhhggf2ef0gfedd0ccd1eddccBBaaa';
      'y':fdat[1]:='K532hhijkk0jigfghijjj0ihge0hI0hg2Ihgf2iiihhgf20hhhgf21gggfe2ff0eed2ddd0ddc2cccbbb21baaa';
      'z':fdat[1]:='H50IhhghIhhghi0iihh10hhggf10gfff10E1DdcccBbaaa';
      'A':fdat[1]:='N20hhh310iiih31Ji3jkkkji3jklkjih21ij1lkjh21jk1mlji20ghijkkjhgg2fgghhhgffe10dde2feedd0Cd1dddcccBbb1bbbaaa';
      'B':fdat[1]:='Lhhjklmlkih1hhiklmmkjhg1hijk1kjhg1hhjk1kjhg1hhik1kjhg1hijklmkjh10iijlmmkjhg1gghi10hgfe0ffgh10gfed0eeff10e'+
      'edcDeeddccb0Bbbbaaa';
      'C':fdat[1]:='L2ijjigf2gghiigfddd0gfgg10eccc0gff2dcbbhggf20cbbhggg3G3Ff3E20d1De2dc1Ccccbb20bbaaa';
      'D':fdat[1]:='Mhhhijklkih10ggghijkkihf10gfgh10ihff1gffh10ihfff0gffh2hfff0gghi2hgff0hhij2igff0gghi2gfee0ffgh2fedd0eeff10'+
      'eedc0Deeddccb1Bbbbaaa';
      'E':fdat[1]:='Khiijkjjhg1hhiJhge1hiii10ge1hiii10ge1hiii0ih2iijjkji2ijjkkki2ghhi0ih2ffgh2ed0eeff10eDddeeddccBBaaa';
      'F':fdat[1]:='Khhijkkkihf0hhijjkjigfe0hhjj10gfe0hhii10f10hhii1gf10hIhge10Iihfe10G1ed10fffe210eeed21dddcccb2Baaa';
      'G':fdat[1]:='M2ijjigfe2ghijjigfee1gghi10hfff1ggh20gff0gggh20ggf0gghi30fghi1lkjihg0effg1jihgfed0eeff10gfed1ddeee1eddc10'+
      'Ccccbb21bbaaa';
      'H':fdat[1]:='Mhhhjkl0lkihgghhhjkl0lkihgf0hhij10kihg1hhij10kihg1hhij10kihg1hijklmlkihg1hijklmlkihg1gghi10ihgf1ffgg10hgf'+
      'e1eeef10feed0dcD0eddccBbbb0bbbaaa';
      'I':fdat[1]:='FhGfgggfff0gfff1gfff1gfff1gggf1hggg1gfff1feee1eddd0dCBaaa';
      'J':fdat[1]:='J2hGf2gggfff20ggff21hhgf21ihgf21jihf21jihf1gh1hhge0effg0ffed0dde1eddc1Ccbb2bbaaa';
      'K':fdat[1]:='Nhijkll0kihfe1hijkll0kihfed1ijkl1kih20ijll0mkj21ijklmmlj21ijklmmlk21ijklmmlkj20ghijkkjjhg2ffgg0ihhgfe10E1'+
      'ffeedd0dCd1ddccBbabb10bbaaa';
      'L':fdat[1]:='J0Gf2gggfff20gfff21gfff21gfgg21gghh21hhij21gghi21ffgh10ed0eeff10DDdccBbbbaaa';
      'M':fdat[1]:='Ohhhii20jihhghiiij20kjihg0iijkl10mljih1ijkkm10nlkih1ijkkln0nmljig1ijkklMljig1hijjLlkjig1gh0hijjj0ihgf1ff0'+
      'gghhh0ggfe1de1fff1fedd0C0ddd0ddcccbbbaa1b1bbbaaa';
      'N':fdat[1]:='Mhhii20igfehiijj2jhge0ijjkl2ig1hijkln10ih1gijklmlj0ih1ghijklkiihg1fghijkihggf1ee1higfeee1dd10gfeddc1cc2dc'+
      'cbb0cbbb20bba0baaa21aa';
      'O':fdat[1]:='M2jkkji21ghijkjhgf10gggh1jhgff1gfg2hgff0G2igffeG20hgfeG20hgfeFg2gfed0eeef2fed1dddee10ddc10Ccccbb21bbaaa';
      'P':fdat[1]:='Khhijkllji1ghijklljig1hijk1kihg0hijj1jhgf0hhij1igfe0hiii1hfed0Iihged1Ggfec10feee210eddd21dCb20bbbaaa';
      'Q':fdat[1]:='M2jkkji21ghijkjhgf10gggh1jhgff1gfg2igff0G2igfffG20hgffghhh20igfehhiik2ihfe0hijk2jigf0hijkl10jig10hiJihg10'+
      'efgghgggf10ccddEee1c10CCbb21bbaaa';
      'R':fdat[1]:='Mhhjklllji2hhiklllkihf10hijk1kihf10hijk1kjhg10hijk1kjhg10hjklmmlj20hiklmmlji2ghij0kjihf10ffgh1hgfee1eeef1'+
      'ffedc0dccddd0ddccBB1baaa';
      'S':fdat[1]:='J1jklljig1ijkllkihfhijk10jhghijkl10ighijkllji1hijkllkih1hijkkjihgf0hhiihgfeee10gffeD10eddc0Cccbb10bbaaa';
      'T':fdat[1]:='L0hijjkkjiig1giiijjihhfeefh0H0gfddfg0G0fec2gfff3gggf3hggg3gfff3feee3eddd210dCb21bbbaaa';
      'U':fdat[1]:='MhGh10feddgggffh10eddc0gfff20dc1gfff20dc1ggff20dc1G20ed1gghh20ed1ffgg20ed1eeff20dc1ddeee10dc2cdddcccbb20b'+
      'bbaaa';
      'V':fdat[1]:='M0hhhij10hgeeggghij1ihfed0gghij10hfe10ghii10hf2ghii1hgf2gH0gg21Gfff21fEd3dddcc3ccbb31bba31aa';
      'W':fdat[1]:='Q0hijk1mmml1ihfeggijklMlk0igfd0ghij1mnml1hgf10hijl0nnml1hg2hhjk0mnmkj0gg2ghijklmljigf21fghjkkjigfe21eefgh'+
      '0hgfed21ddeef0fedc3ccd1dccb3bbb10bba30a2ba';
      'X':fdat[1]:='M0hijjk0jihfe0fghIihgfd1ghiii0hhg20hhiihhh210hI30J30klllk210iJih21gh0iihgf2ef1ggfeed0ccddd0eeddccBbb0Baaa';
      'Y':fdat[1]:='Lghhijj1ihfefghIihged1ghhh1gf2ghGf21gggfff210gggf3hggg3gfff3feee3eddd210dCb21bbbaaa';
      'Z':fdat[1]:='J1hIhgf0gHhgff0fg1gggf1fg1gggf20hhhg21jiih20jkjj20hiiih20ghhh1fe0eF1DDdccBbbbaaa';
      '0':fdat[1]:='I420kkkj10jklkkjh1jklkkjh0hikk0kjighijj0jihgfghh0hhgfefff0ffed0eeedddc1Cbbb2baaa';
      '1':fdat[1]:='H410gggf1Gf1gggfff2hggg2ihhh2hggg2gfff2feee1DdccBbbaaa';
      '2':fdat[1]:='J421ijjiih10hIhhg0ghhIhg0gh1jjiih20kkji20jkjj20hiii1gf0ffghggfeDeeeddccbbbccBa210aa';
      '3':fdat[1]:='J421ijjii10ghijjjiih0eg1jiiih20jiiih2ijjih2ijkjjih1ghijjihgg20ihgfe20ggfedde10feddccdddcccbb1bbbaaa';
      '4':fdat[1]:='J4310gg21hhhg20jjihg2kkkjih2k0lkjh10j1lkih1hi1kjih0fghiiihggfdeffgffeedccDdccc20cbbb21baaa';
      '5':fdat[1]:='I42Jigf0ijjjihgf0hIhg0fgh21fghihhh1fghiihhg2iihhgf2gggfe2ffeed2eedc0Cccbb1bbaaa';
      '6':fdat[1]:='I32ii21jkjj2jkkk20jLji0ijkllljighijkkjihgghhi0hhgeffgg0ffeddeeedddc1Cbbb10baaa';
      '7':fdat[1]:='I410hIhgfeghhhggfedFfeeede10eee21eee21fee20ffe21eee20D20ccc20cbbb20baaa';
      '8':fdat[1]:='I310Kj10jkllljigijkl0ljighjkl0kjighjKjh1ijkkjih0ghhiihhgeeff0gffeddde1eddc0Ccbb10bbaaa';
      '9':fdat[1]:='I42kllkj10jklllki0hjkllkjihgijk0jjigghii0iihgfgHggf0fgggfffe1Edd20ddcc20ccb20bbb21aaa';
      '.':fdat[1]:='D50dc0cccBa0aa';
      ',':fdat[1]:='D50ed0DC0bbb1ba1a1a';
      ':':fdat[1]:='D320dd0edddeddd0dd21dc0cccBa0aa';
      ';':fdat[1]:='D320dd0edddeddd0dd21ed0DC0bbb1ba1a1a';
      '!':fdat[1]:='D0fe0fEDddcdddc0dd1dd21dc0cccBa0aa';
      '?':fdat[1]:='H0hhhgg1fgggfff0dee0eedd2D10D2ddd20dd321dc20cccb2bbba20aa';
      '(':fdat[1]:='G2fee10ffe10fff10fff10gfff10F10fffe10ffee10E10D2dcc20ccb20bbb20aaa';
      ')':fdat[1]:='Geee20ffe20fff20fff2gfff10F10fffe10ffee10feee10eddd10dcc10ccb10bbb10aaa';
      '<':fdat[1]:='I431e21gfe10hhhgfd0Ggf1eeef20ddde21cdddc21cccbb21bbaa3a';
      '>':fdat[1]:='I41e3efg21ffG20Gff21ffed20eedc10dddcb1cccbb10bbaa20a';
      '+':fdat[1]:='J43hg3hg3hg3hg2FgffeeDDccc2cb3bb3ba3aa';
      '-':fdat[1]:='E4321dddCBaaa';
      '=':fdat[1]:='I52gghiihgfeeffgffeed41DddccBbbbaaa';
      '*':fdat[1]:='G10f20ggg1ff0f0fED1ccc10cb0bb1ba0aa';
      '/':fdat[1]:='E10cc10cc10c10cc10cc10cc10c10cc10cc10bb10b10ba10aa';
      '&':fdat[1]:='O10hhijj10hgf10hhhijj1hgffe1hhi210gfe1hii21ggfe0hhijkl10hgff1hijkll0jhgff1gghi10kihg2ffgh10ihgfe10eeffg2f'+
      'ed2deeef1eddc2ccdddcccbb210bbbaaa';
      '"':fdat[1]:='E20ee0eddd0dccc0cbbb0baa1a';
      '''':fdat[1]:='B1ccBaaa';
    end;
    if fdat[1]<>'' then
    begin
      xx:=textx;
      yy:=texty;
      width:=ord(fdat[1,1])-65;
      j:=1;
      i:=2;
      repeat
        repeat
          case fdat[j,i] of
            '0':incxx;
            '1':for q:=1 to 2 do incxx;
            '2':for q:=1 to 4 do incxx;
            '3':for q:=1 to 8 do incxx;
            '4':for q:=1 to 16 do incxx;
            '5':for q:=1 to 32 do incxx;
            '6':for q:=1 to 64 do incxx;
            '7':for q:=1 to 128 do incxx;
            'a'..'o':putpix(ord(fdat[j,i])-96);
            'A'..'O':for q:=1 to 4 do putpix(ord(fdat[j,i])-64);
          end;
          inc(i);
        until i>length(fdat[j]);
        inc(j);
        i:=1;
      until fdat[j]='';
      textx:=textx+width;
    end else textx:=textx+16;
    inc(textx,space);
  end;
end;

procedure font30_3;
var
	t,xx,yy,i,j{,k},q:integer;
	fdat:array[1..13] of string;
  width:byte;

procedure incxx;
begin
  inc(xx);
  if xx>textx+width then
  begin
    xx:=textx;
    inc(yy);
  end;
end;

procedure putpix(col:byte);
begin
  case colmode of
    0:putpixel(xx,yy,tc[col]);
    1:if odd(xx+yy) then putpixel(xx,yy,tc[(1+col) div 2]) else putpixel(xx,yy,tc[(2+col) div 2]);
    2:if odd(xx+yy) then putpixel(xx,yy,tc[(2+col) div 3]) else putpixel(xx,yy,tc[(3+col) div 3]);
  end;
  incxx;
end;

begin
	for t:=1 to length(textstring) do
	begin
    for i:=1 to 13 do fdat[i]:='';
    case textstring[t] of
      'a':fdat[1]:='L543210jkllki2hijkllkjh10hij1lkjih210nlkih10klmonmkjh1ijlmnmlkih0gghij0kjihf0efggh0ihgfe0ddeef0gfeedc0ccD'+
      'dccbb1B0aaa';
      'b':fdat[1]:='M4ijj210iiijjk210hiijkl3iijkl3iiklm0nlki10iijlmnnlkih1hijklnmljigg0hhikl0mkjhgg0hhjkl1kjhgf0hhijk1kihff0g'+
      'ghii1ihfed0effgg1gfed1ddeee1eddc1CCbb21bbaaa';
      'c':fdat[1]:='K54310kkjih2ijkkjhge1iijjkihfecIj1hgedhiiij10gf0hhiij21gghhii20effggg10fe0deeffeeedc1cddCb2bbbaaa';
      'd':fdat[1]:='N43jih3llkjih3mlkihh30lkihh2jklmmljihg10hjklllkihgg1hhijl1kihgg0hhhij10kjhhg0hhhik10lkihh0ghhij10mkjih0ff'+
      'ghi10kjhgg0eeefg10ihgfe1ddeef0ggfeed10ccDddccbb10B0bbaaa';
      'e':fdat[1]:='K54310lmlk20jklmljig1ijkl1jigfiijkl1kihfiijklmljigfhiijkkkjhgefgghh21effggg10fe0deeffeeedc1cddCb2bbbaaa';
      'f':fdat[1]:='I32ihhh2Ihgf0hii0ihgf0iii1hgf0I21jiii10hIihg0hIhhg1Ihh2iiihh2hhggg2gF10Eddd1Ccbb10bbaaa';
      'g':fdat[1]:='L54310g1ijlmlk0hgf0hikllljigffgghi10igfe0hhij10jhgf0hijk10kihf1jkllmlkih10ijklllji10hhij3hijklnmljih1hjkl'+
      'mmkjihgfghiijjihgfeeef2gfeedddee10fedc1cDdccbb10bbbbaaa';
      'h':fdat[1]:='O40iiij30iiijjk30hiijjk31iijkl31iijkl1mlji2iiiklmmmkjhh10hhijklllkihhg1hhhij1lkihgg1ihhik10kjhhg1iiijk10l'+
      'jihh1hhghi10jiggf1gffgh10hgfee1Ef10ffedd0dddcddd1ddcccBB1bbbaaa';
      'i':fdat[1]:='G0fffe1gFe0gFe1fff320hhh0iHg0Hgg1hhhgg1iH1iH1Gf1fffee1eeddd0Ccbb0bbaaa';
      'j':fdat[1]:='G1F1ggF0ggF1gff32H0iHg0Hgg1hhhgg1hhhgg1hhhgg1hhhgg1hhhgg1Hg1hhhgg1ggfff1feeed1DCbbb1baaa';
      'k':fdat[1]:='O41iii30ihhiij30hhhijk31iijkl31iiklm1kjhgf10ijkln1ljigf10ijkmn1lji20ijkmnnmlji20ijklnnmlki20jjklmnmlkj20h'+
      'hijkkkjjhg2ggghh0ihhgfe10eeeff1ffeed1dddcddd1ddcccBB1bbbaaa';
      'l':fdat[1]:='G30H0iHg0Hgg1hhhgg1hhhgg1hhhgg1hhhgg1hhhgg1iH1iihhh1hG1Fe1eeedd0ddCBaaa';
      'm':fdat[1]:='S651kl1M1mlji1hijkl0oN0omljig0hhjklmoMmomljig1hiklm0Mm0mljig1ijkln0Nn0nlkih1ijkmn0Nn0nmkjh1hijkl0Ll0lkihg'+
      '1fghii0Ii0jihgf1effgg0Ggggffed0DdeeDdeeddccBbbb0Bb0bbbaaa';
      'n':fdat[1]:='O6321jkl1mkjh10Ikl0mmkjhh1hhhijklllkihhg1hhhij1lkihgg1ihhik10kjhhg1iiijk10ljihh1hhghi10jiggf1gffgh10hgfee'+
      '1Ef10ffedd0dddcddd1ddcccBB1bbbaaa';
      'o':fdat[1]:='M620klllji20hjkllljig10hhik1lkihg1hhik1lkihgghhhik10kihgfghhij10jihFghh10ihfedeeefgg1gfed1ddeee1eddc10Ccc'+
      'cbb21bbaaa';
      'p':fdat[1]:='M62klm0nlki10iiklmnnlkih0hhijklnnlkihg0hhikl0mljigg0hhjkl1ljigg0iijkl1ljigf0iijkl1jigfe0iijjk1ihfed0iijjk'+
      '1hged1Jjjhgedb1Hhgfec2gF3Ed201dddcccb21Baaa21';
      'q':fdat[1]:='N630jklmmljihg10hjklllkihgg1hhijl1kihgg0hhhij10kihgg0hhhij10ljihg0ghhij10lkihg0fffhi10ljihg0eeefh10kjihg1'+
      'ddfgh1kjiih10defgijjjiih2cdef0hhggg30gF30Ed3dddcccb210Baaa';
      'r':fdat[1]:='K5430klmn0lji0iijklmlkihfhiijkkkjhge0hiijjjhged0Ii0hfec0Ii0ged1hhggg21gF21Ed20dddcccb2Baaa';
      's':fdat[1]:='J542klllkjh1jklmlkjhgijklll0jigijklllk0i0ijklmlkj10jklmlkji1hijkjihgffg0hhhgfEf0ffeedccDcccb1Baaa';
      't':fdat[1]:='J430ji21jjji2iJi10hiiijiihgegHhhhfe1Hh20Ii20Ii20hI20ghhhg20ffgfffee1deeDc10cccbbb20baaa';
      'u':fdat[1]:='O6321ijk20hhh0ihhhjk1lkihhg0Hij1lkihgg1hhhjk10kjhhg1hiijk10lkiih1hhijk10mkjih1ggghi10kjhgg1fffgh10ihgfe1d'+
      'eeeffgggfedd10cccDddccbb10B1bbaaa';
      'v':fdat[1]:='N621ghijklmm0kihfefghijkll0jigfd1hijkk1jhg2ghijjj0ihf20hIhhg21ghhGf210fffeee3eDc30cccb31bbba310aa';
      'w':begin
        fdat[1]:='U6531hiiklm0onMm0kihfefghijkl0nnnmmml0jhged1hhjjk1nnnml10hg2ghijj1nnnmlj0ihf20hiiijlmmmljihgg21ggghhikkkihg';
        fdat[2]:='ffe210Fgh0hgfedd3Dee1edccc30C10cbbb31bba20baa310aa21a6431';
      end;
      'x':fdat[1]:='N6210hijklll1jhgefghiklllkjigf10hijklkkjhg20ijkkkjih210klllkji210lmmmlj210ijkkkjih20fgh0iihgff2ef1ggffee1'+
      'ccddd0eeddcccBbb0Baaa';
      'y':fdat[1]:='N6210ijjkll1jigfeghiijlll0jhgedfghijkk10hf2hijkk1ihf2hiJ0ih21iJihg21Jihhg210kjjihg3kjjhgf3jjihfe20ghh0hgf'+
      'e20efff0feed20D0ddc210cccbbb30baaa';
      'z':fdat[1]:='K543ijkkkjihg0ghijjjihgf0gh1jjihg1gh0ijjjih20jkkkj20jkllkj20iijkj20gghiih1fe0fffggf0eDddeeddccBBaaa';
      'A':fdat[1]:='R210jii321jkkjj320jkkkji32jkllki310iklmmkji31jklmmlki30hij0mmlkji3gij1llkji3hi10mllkjj21hijkkllkkkjj21ggh'+
      'hIiiihh2eee20Ggf2ddd20Feed0dCc10DdcccBaab2Baaa';
      'B':fdat[1]:='Oiiijklmmlkih10hhhijkmmlkihg10hhijk1lkihgg1iiikl10ljihg1iijkl10lkihh1iijkl10lkiih1iijkl1mlkii10iijklmmmlk'+
      'i2iijklMkjii1iijkm1mmkjihg0hhijk10kjhhgf0ggghi10ihgfed0effgg1ggfeddcDEddccbb0BBbaaa';
      'C':fdat[1]:='O20ijkkjihg20hhhiiihgfff10G10gEd0hggg20dddcc0hhhg21ccbbHg21bbaaHh210b1Hh31Hh31gghhhi31fG31eeF21dd1ddE2ddc'+
      '10ccDdccbb21Baaa';
      'D':fdat[1]:='PIjkllkkjh2Hiikkkjihhg10Hh10jihhg10hhhgh2jhhhg1hhhgg2jhhhgg0hhhgg20hhhgg0hhhgg20hhhgg0Hh20hhhgg0Ii20ihhgf'+
      '0iijjk20ihggf0hhhij20hgfed0ggghh2gfeed1efffg10feedc1DEddccb10BBbaaa';
      'E':fdat[1]:='Niiijjklkjhge1hhhijjkjigfdd1hhijj10gfdd1iijjj2fed1iijkk20ed1iijkk1ig20iijkj1ig20iiJjig20iijjkkjjh20iijkk1'+
      'ki20hhiii1j10fe0Gh20fed0efffg2fedcDEddccb0BBbbaaa';
      'F':fdat[1]:='NIjkkkjigfe0Hijjjihfedd0hhhij2fedd0iiijk20eed0iiijk21ed0iiikl10hf2iiijk10gf2iiijkjigfd2iiijjjhged2iiijj1f'+
      'ec2Hh10d20gggff30efeee3Ddccb21Bbaaa';
      'G':fdat[1]:='R20Iihggff21Hhggfeeddd2hhghg10Edd10hhggh21fffe10H3ff10Hh320Hh320Hh20jjiihhg0Hi2kkjihhggegghhhi20jhggf10fG'+
      '20hgfee10eefffg2gfedc2ddeeef10eddc20ccDdccbb30Baaa';
      'H':fdat[1]:='Piiihijk1kjihhhgHhij1kjhhhgg0Hj2jihhg1Ik2kjihh1Ik2kjihh1Ik2kjihh1Ik2kjihh1IkLjihhg1IjKjihhh1Ij2kiihh1hhgh'+
      'i2ihggg1gfffg2hgfff1Ef2ffeed0dddcddd1dddcccBB0Baaa';
      'I':fdat[1]:='GiiihhhgHhgg0hhhgg1hhhgg1hhhgg1hhhgg1hhhgg1hhhgg1iH1iiihh1hhggg1gF1Ed0dddcccBbaaa';
      'J':fdat[1]:='M21iiihhhg21Hhgg210hhhgg3ihhgg3iihgg3jihgg3kjhgg3ljigg1hik10ljigg0hhikl1kjigf0fghij1ihgfe0effgh1ggfed0dde'+
      'ef0feedc10ccddCb21bbbaaa';
      'K':fdat[1]:='Qiiijkl10jigffd1hiiklmm0kihffec10ijklm1jigf21ijkmm1kih210iklmn0mlj3iklmnonlk3ijkmnonmlj210ijklmnnmlk210ij'+
      'jlmnnmlkj21ijjklmmmllkj20hhhij0kkjjihh2gffgh1Hggf10Ef10fffeeed0dddccdd1ddddcccBbabb10Baaa';
     'L':fdat[1]:='M0iihhhg21Hhgg210hhhgg3hhhgg3hhhgg3hhhgg3hhhgh3Hh3Ij3ijjjk3hhiij2gfe0ggghi2fedeefffg10fedcDeeeddccb0BBbaaa';
      'M':fdat[1]:='Riijiii210jjiighiJ21lkjjig0jjkklm2nlkjjh1ijkklm2omlkjh1ijklmnn1oomlkih1ijklmnn1oonlkih1hjklMmnomljig1hijL'+
      'llnnmkjhg1hij0Llm0mkjhh1hij0Kkl0lkihh1fgh0Iii0jihgg1efg1G1hgffe1dee1E1feedd0Cc1cc1dddcccBba1bb10bbbaaa';
      'N':fdat[1]:='Phijjj21ihgfehijkkk20jhgfe0jkkllm20ihg1ikllmnn2jig1ijklmnnn10kjh1hiklmNm0kjh1ghjklMmlkjh1gghjkLlkkjh1fgh0'+
      'jKjjjih1fgg1Jiihhg1eef10hhhggfff1edd2ffeeedd1ddc20ddC0cccbb21B0bbaaa102aaa60';
      'O':fdat[1]:='Q20ijkkjii3hhiijjIh20gggh10ihiihh10hhgg20hiihhh1H20hiH0iihhh21ihhhggiiihh21hhGhiiih210hgggfhIi21hggffghhh'+
      'ii21hgffe0fggghh20gfed1eefffg20fedc10ddeeef10eddc20ccDdccbb3Baaa';
      'P':fdat[1]:='Niiijklmmlki10hhhijklllkihg1hhhik1lkihgg0hhijk10kihgg0iiikl10kihgf0iiijk10jhgfe0iiijk10hgeed0iiijj1hgedc1'+
      'iiJhgedb10Iiigfdc2hhggg30gF30Ed3dddcccb210Baaa';
      'Q':fdat[1]:='Q20ijkkjii3hhiijjIh20gggh10ihiihh10hhgg20hiihhh1H20hiH0iiihh21ihhhggIh21hhGIi210hgggfhIi21hggffhiijjj21hg'+
      'ffehijjkkl20hgfe1hijkll20ihge10ijklmm10ljig20hjkLkkjh2effghIii20ccddeffGg2d2cddeeeffe1dc21cccddCb31bbbaaa';
      'R':fdat[1]:='Piiijklmmlkih2hhhijkmmlkihg2hhijk1lkihgg10iiikl10kjhgg10iijkl10kjhhg10iijkl10ljihg10iijkl1mkjhh2iijklmmlk'+
      'jh20iiiklmmlkji20iiijk1mlkihh10Hi1jjihgg10gffgg10hgffe10Ef10ffeddd0dddccdd1ddcccBbbabb01bbaaa60';
      'S':fdat[1]:='M10jkllkjhg2jkkllljigf1jjkk2jhgfijkkl20jhghjklmnn2ihhijkmnnnmlk1ghijkmmmlkji1ghiklllkjjih0ghiklllkjihg10i'+
      'jkkkjihgfef10iiihgfeedee20gfedcddde2eddc1ccdddcccbb20bbbaaa';
      'T':fdat[1]:='O1jklmLljig1hijKkkkihf1ghi0jjijj0igfddfgh0iihii1feccef1iH1fdccd10Hh10db20hhhgg31hhhgg31iiihh31Ih31Hg31ggg'+
      'ff31ffeee3cDcccb210Baaa';
      'U':fdat[1]:='P0iiH2fffeeHhgg2fE0hhhgg21eee1hhhgg21eee1hhhgg21eee1hhhgg21eee1hhhgg21eed1Hg21eed1Hh21fee1hhhii21fed1Gh21'+
      'edd1eF21dd10ddE2ddc2ccDdccbb210Baaa';
      'V':fdat[1]:='Q0ijjiijk10ihFghIjk10igfeee0ghiiij20gfe2hiiikl10igfe2hhijkl10igf21hijkl10ihf21hijkkk0jig3ijkkjjihg3hijiii'+
      'hg30gHggf31Ffee31eeD32Cb32bbba0320aa632';
      'W':begin
        fdat[1]:='W0iiijkl1nmlkkl10jhgfeghiijklmnnmlklmm0kihfee0hhiikl1nnmllm10igfe10hiikl10onmmm10igf2hijklm1oonnnl1ig21hjkk'+
        'l0Onml0jhg21hijjkmNmlkjihf21hiiijkMlkihhg3hhhijkl0lkihhgg3Ghjk0kihgff210';
        fdat[2]:='10Fg1hgfeee31eddee10edddc31Cd10dccb32bbb2bbba32aaa20aa6510';
      end;
      'X':fdat[1]:='Q0hijklmm1ljigfe0fghijklmllkihfed10ghikkl1jigf20ghijkkkjjhg210gijkkjjjh30ijKj310jkklkk310jklllki31kllmmlj'+
      '31klmmmlkj3ijjkkkjiih21fgh1iihhggf20eff10ggffee1ddcD0ddddddccBbbb1Bbaaa';
      'Y':fdat[1]:='Oghiijkll1jigfeeghhijkk0jihfed1ghijjj1hge20hI0ihf21giiihhhg3Hhgg30hhhgg31hhhgg31iH31iiihh31hhggg31gF31Ed3'+
      '0dddcccb3Baaa';
      'Z':fdat[1]:='N1ghikllkjjihg1fgijkkjiiihf0deg10kjiihg1deg10jjiihf10f10jjiihg3jjjihf210Jig210jjjkjig210kkklkj210Lml210ij'+
      'jjkkj10f10hhhiii10fed0fffG10eDddEeddccBBbbbaaa';
      '0':fdat[1]:='L432lmlk21jklmmlji10hjklmmljig1hikl0mljig0hhijl1ljigghhikl1lkihghhikl1lkihgghijk1kjigfffghi1ihgfe0efggh0g'+
      'ffe1dEeeddc10Cccbb21bbaaa';
      '1':fdat[1]:='K4310jjihf10jjjiihgf1hiiihhg10fgHgg21hhhgg21Hg21Ih21Ji21Ih21hhhgg21fggfee1cDdddccb0Bbbaaa';
      '2':fdat[1]:='M4321lmmlkj20iklmmlkji1fghjKjihg0efhiijjjiihf0efg1jjkjihg21kklkjh21kllmlj210lmnnl210jklmm10ig1hijkkkjihgf'+
      '0ffgghhhggfeDdeefeeddccBbcccB31aaa';
      '3':fdat[1]:='M432ijkkkji2fghikkkjji10efhijkkkjih210lllkji210lllkji21jkllkj20fhijklkjj2ghikllkkji10gijkllkjjih21llkjihg'+
      '210jihgfe21iihgfedeefffgffeddc0ccDdccbb2Baaa';
      '4':fdat[1]:='L5ihg3iihh210jjihh21kjjihg20jkkkjhh2jk0lkjih2k1nlkji10jl1omlji1hj10nmlji1hijlmmlkjihefhijkkkjihhdefghiihh'+
      'ggfbcddefffE21Dc210cccbb210bbaaa';
      '5':fdat[1]:='L431jklmmlkihg1jkllmkjhgf0ijklmlkihge0jkklmlkihge0jkklmlkihg1ijk3ghjklmml2ghjklmnmlki0ghjklmmlkjih0hjklll'+
      'kjihg2jkjihgfe20iihgfedeeFfeddc0cDdccbb10Baaa';
      '6':fdat[1]:='L4iii3jkkkj21jklll210jklm210iklmnnlki10iklmnnmkjh0iijlmnnmkjhghijkl0nlkihghhijk1ljigffghii1jigfeeffggh0gg'+
      'fed0dEeeddc10Cccbb20bbaaa';
      '7':fdat[1]:='L320jk31jklmonmkjigghijlmlkjihgffgijkjihggfdefghihhgff0ccefgggfffe1bd2fffe3fff30gff3gggf210G3G210Fe210eee'+
      'd210dddcc210ccbbb3baaa';
      '8':fdat[1]:='L3210lmnnlki10iklmnnlkih0hijlm1mljighiklmn0mljighijkmnnlkih1ijklmmlki2jkmnnlkih1ijkmnnlkihgghik0mmkjigffg'+
      'hi10jigfeeffgh1hgfed0eeFeedc1ccddCb20bbbaaa';
      '9':fdat[1]:='L4310klmmlj20jkmnmlji10ijkmnnlkih0hijkl0nmkjhghijkl1mkjhghijkl1mljigghijl1lkjigfghjkl0kjihg0ghijkkjiihf0f'+
      'ghIhhg10fghhgggff21fffee210eeddd21dC21Ba30aa';
      '.':fdat[1]:='E5410ffe0eeeDCB0aaa';
      ',':fdat[1]:='E5410ggf0gFfffEeed0dddc1ccc1bb1bba1aa';
      ':':fdat[1]:='E420ffe0ffeeeffeeeffeee0ffe210ffe0eeeDCB0aaa';
      ';':fdat[1]:='E420ffe0ffeeeffeeeffeee0ffe210ggf0gFfffEeed0dddc1ccc1bb1bba1aa1';
      '!':fdat[1]:='E0kkj0klkk0klkkkjkJjjiihhgggfffe1ddd1edd310K0jkjjhfgffeeeddc0baa4';
      '?':fdat[1]:='K10mnnm21kNlk10jkNlki0jhil0mmkkhhhghk0llkjggdefh0iihge1cd1ggfe21edd3eed4321kkki21ijjjih20Fee20dedddc210aa'+
      '520';
      '(':fdat[1]:='I21gfe2ggfe2gggf2hggg2hhggg2hhgg2hhhgg2hhhgg2hhggg2hhggg2hG2Gf2fggff20F20Ee20D21C21B210aaa';
      ')':fdat[1]:='Ieff210fggf21G21hggg20hhggg20hhgg20hhhgg2hhhgg2hhggg2hhggg2hG2Gf2gggfe2gffe2feeed2dddc2C2B2aaa';
      '<':fdat[1]:='L510e3gfed21ihgedc2jjigfdc1Iihgf10ggH21effg3deeffg210cddeeff210cddeD210Cbb3bbaa310a';
      '>':fdat[1]:='L43e310effg3deffgh210defghii210ghijiihg210jihgfe3gfed21hgfedc2ggfedcb1Edccb10Cbb21bbaa3a';
      '+':fdat[1]:='M43210ggg31iii31kkk31kkk31kkk20fghiijjjihhfeefggHggfecceFfffeedb20ddd31dcc31ccb31bbb31aaa';
      '-':fdat[1]:='G541gggffEeeDCBaaa';
      '=':fdat[1]:='L5432gghijjjihgfegghijjjihgfegghijjjihgfe43effgghhgfeeDdeeeddccBBbbaaa';
      '*':fdat[1]:='J2ii210jjji2h1kk0ih0ghi0jj0ihffgHhgfe10gfff2dEddc1Ccbbb10ba1aa';
      '/':fdat[1]:='G20dd2ddd2dd20dd20dd2ddd2dd20dd2edd2ddd2dd20dd2edd2ddc2cc20cb2bbb2aaa';
      '&':fdat[1]:='S2ihhhg20hhgg2Ihhg10iihhgg1I1ih10i0hhgf1iiij30hhgf1ijjk3ihgfe1jjK20jihge1iijjkkk10jjihgfe0Ijkkk0kiihhgf10'+
      'hhhij10lkihhg20ghhii10kkihgfe2fgggh10i1gfed2eefffg20fedc20ddeeef10eddc210ccDdccbb31Baaa';
      '"':fdat[1]:='Efe0eeed0dddc0cccb0bbb1a0a1a';
      '''':fdat[1]:='BcccBaa0a';
    end;
    if fdat[1]<>'' then
    begin
      xx:=textx;
      yy:=texty;
      if textstring[t] in ['a'..'z'] then dec(yy);
      width:=ord(fdat[1,1])-65;
      j:=1;
      i:=2;
      repeat
        repeat
          case fdat[j,i] of
            '0':incxx;
            '1':for q:=1 to 2 do incxx;
            '2':for q:=1 to 4 do incxx;
            '3':for q:=1 to 8 do incxx;
            '4':for q:=1 to 16 do incxx;
            '5':for q:=1 to 32 do incxx;
            '6':for q:=1 to 64 do incxx;
            '7':for q:=1 to 128 do incxx;
            'a'..'o':putpix(ord(fdat[j,i])-96);
            'A'..'O':for q:=1 to 4 do putpix(ord(fdat[j,i])-64);
          end;
          inc(i);
        until i>length(fdat[j]);
        inc(j);
        i:=1;
      until fdat[j]='';
      textx:=textx+width;
    end else textx:=textx+16;
    inc(textx,space);
  end;
end;

procedure font30_4;
var
	t,xx,yy,i,j{,k},q:integer;
	fdat:array[1..13] of string;
  width:byte;

procedure incxx;
begin
  inc(xx);
  if xx>textx+width then
  begin
    xx:=textx;
    inc(yy);
  end;
end;

procedure putpix(col:byte);
begin
  case colmode of
    0:putpixel(xx,yy,tc[col]);
    1:if odd(xx+yy) then putpixel(xx,yy,tc[(1+col) div 2]) else putpixel(xx,yy,tc[(2+col) div 2]);
    2:if odd(xx+yy) then putpixel(xx,yy,tc[(2+col) div 3]) else putpixel(xx,yy,tc[(3+col) div 3]);
  end;
  incxx;
end;

begin
	for t:=1 to length(textstring) do
	begin
    for i:=1 to 13 do fdat[i]:='';
    case textstring[t] of
      'a':begin
        fdat[1]:='R710klmmmllk210ffhijkkLkk2eefghijkllmllkj10eeeghi1lmnmlkjh10ffg2mnnmlkjh31nnomlkj';
        fdat[2]:='h31ooomlkjh20lNnnnmlkjh10klmNnnnmllji1kklmNnnnmmlji0ijkklnn1ooonmlji0hjjklmm1nnnmlkjh0ghiijjk1lllkjihg0efgg'+
        'hhIiihggfed0Efffggfffedddc0Cccdd0ddccbbb2bbabb10baaa';
      end;
      'b':begin
        fdat[1]:='V432mlkjh3210mnnmlji320jlmnmmlkj310ghjklMlk310fhijkmmmnmm32ghjklmmnnn321jklmmooo1oonml210jklmmnnn0ooonnmlk2'+
        '0jklmmNNmmlll2jkllMm';
        fdat[2]:='Mmlmmm2jkLLLlmnnn10jkLll1Llmnnn10jklllkkk10lllklnnn10jkLll10llkklmmm10jkLll10mmlkklll10jkMmm10nmlkkkjj10ijl'+
        'lM10nmkjiih2hijkllmm10mljihgf2fghijjkk1kkjhgfe2ddefgghhIhgfedc2b';
        fdat[3]:='ccddeeFfeedcb31ccdddcccbb321bbbaaa';
      end;
      'c':begin
        fdat[1]:='Q654320nooonnl3lmnnoonnlkj20mlmmNmljih10nmlMmlljihhg0nnnLkkkjhgfff0nnnmlkkj1igfeednnnmmmlk10';
        fdat[2]:='jhgfedMmmml2ihfe0LM2jig1kkklmmnnn3iijklmnnon20i1ghiklmNnlkihf0ffghijkllkkjhgfe1effghhiiihgfedc10deeFfeedcb2'+
        '0ccdddcccbb30bbbaaa';
      end;
      'd':begin
        fdat[1]:='U52mlkjh320klmmlkjh32llmnmlkjh32mmnnmlkjh32nnoonlkjh320ooonlkjh21lmN0nnnmlkjh20lMMmnmlkjh10kLLlllmllkjh10LK'+
        'Klllkjh1';
        fdat[2]:='kLjjj10kklllkjh1kLjjj10kklllkjh0jklllkjj2kklllkjh0iklllkjj2kklllkjh0ijLkk2llmmllji0ijkLl2mmnnmlji0hijkllmm2'+
        'nnonmlji0ghijkklm2oonnmkjh1fghijjkl1mmllkjihg1effghhiiJjiihgfed1deeeffGgggffeddc10';
        fdat[3]:='CDDcccbb21Bb1bbbaaa';
      end;
      'e':begin
        fdat[1]:='Q654320llmllj30jkLlkih21kkkLlkjii2kkklll1mlkjii1kkllmm2mlkji1kkllmm2lkjihgjkllmmm2';
        fdat[2]:='kjhgfeikllmnnmmllkihgfeijklMmllkihgfehjkklMlljigfe0gijjkll31fghijklm21h10fghijkkllkjihfe10efgghiiihhgfed10d'+
        'deeFfeddc21ccdddcccbb30bbbaaa';
      end;
      'f':begin
        fdat[1]:='N410mnnmlj21jklnnmljih10ijklmmljigf1iijjkllkjhgfe0iijkk1kjhgfe0ijkkl10jigfe0jkllmm10ihf10lmN3kmnonnmk2hiklm'+
        'nnnmkjhg0gijklmmmlkihf0ghijLkjhge0fghjKkihfe2jkkkjji210jKki210jL';
        fdat[2]:='kj210klmmmlk210klmnmlk210ijkllkih20ghhijiihgf10eeffgggffedc1ccDddccbb2Bbaaa';
      end;
      'g':begin
        fdat[1]:='T731hg21mnnM10kjhg10kklMMllkihf1kkklMmmmlkkjig1ijkkkmm1nnmlkjihf1ijkklmn10n';
        fdat[2]:='mkjihg10ijjkklm10lkjhgfe10ijjkklll1jihgfed2jkllmmllkjihgfe21klmmmllkjihgf21kllmmmllkjii210jkklmm320ijklmnn3'+
        '20hijklmNNnmlj10ijkLLmmmlkji1jjkklKkkllmlkjih1jkklKkklllkkjighijjk';
        fdat[3]:='KKkkjjihfghhii3kjihgfefgghhi210jihgfd0deffggh20ihgfedc1ddeeffGffeddcb20ccDdddccbb30Bbaaa';
      end;
      'h':begin
        fdat[1]:='U431lkjh3210mmlkji320jlmmmlkj32ijkmlmllk32ijkllmmml32hjkllmnnm320jkllmnnn10onmlk20jkllmnnn0Nmlkj2jkllmnnn0N'+
        'llkjh10jkllMMmlkkjh10';
        fdat[2]:='jkLLlllkkkji10jkLll1lllkkkji10jklllkkk10llKi10jklllkkk10lllkkki10jkLll10Llkj10jkmmL10Mllji1jlMmm10Mmljj1jkm'+
        'mmlmm10Mmljj1ijkkkjkk10Kkjih0fghhihhii1Iiihgg0eeFF0Ggffeeeccd';
        fdat[3]:='Dddd0Ddddccc1Babb10Bbaaa';
      end;
      'i':begin
        fdat[1]:='J10lkkji10iijjihgf0ghhiihhgf0gghihhhgf0gghihhhg10hhiii41mlkjh1jlmmlkjh0ijkmllkjh0ijklllkjh0ijklllkjh1jklllk'+
        'jh1jklllkjh1jklllkjh1jkLji1jkmmmlji1klmmmlki1kkmmmlki1ijkkkjih0gghhiihgfeeeFfeddccDcccb1';
        fdat[2]:='Baaa';
      end;
      'j':begin
        fdat[1]:='K20lkkkj2iijjiihh1ghhiiH1gghiHh1gghihhhg2hhiii42mlkjh10jlmmlkjh1ijkmllkjh1ijklllkjh1hjklllkjh10jklllkjh10jk'+
        'lllkjh10jklllkjh10jklllkji10jklllkji10jklllkji10jklllkji10jklllkji10jklllkji0';
        fdat[2]:='1jlmllkji10klmmlkjh10lmmmlkih10llmlkjhg10jkkjihgefggHgfeddeeffeeedc1cddCb2bbbaaa10';
      end;
      'k':begin
        fdat[1]:='Y4321nnmkjh4klmnmlkji321ijjklmmllkj321iijjkmlllkj3210iijkLkk40jkLll40jklllmmm10mllkkih210jkllmnnn1nmlkkjigf'+
        '21jklmmnnn1nmlkjihfe21jklmnooo10mlkjhged21jklmnooo1onlkih30jklmoPponmkj31jklmnPp';
        fdat[2]:='onmlk31jklmnOoonnml31jklmnOoonnmml30jkmmNNnmmml3jlmnnnMMmmml210jkMll0lll';
        fdat[3]:='llmmmlk21ijkkkjjj1Jjkkkjh2gghhiihhgg1Hiiihgf10feFfeee1Ffgffedd1dcDC10Ddddccb2Baaa20Bbaaa';
      end;
      'l':begin
        fdat[1]:='L41nmkjh2lmonmkjh1ijlmnmlkjh0ghjklmmlkji0fhijkmllkji1ghjklllkji2jklllkji2jklllkji2jklllkji2jklllkji2jklllkj'+
        'i2jklllkjh2jklllkjh2jklllkjh2jkLji2jkmmmlji2jlmmmlkj2jlmmmljj2i';
        fdat[2]:='jkkkjih10fghhiihggg1eeFfeee1cDdC10Baaa631';
      end;
      'm':begin
        fdat[1]:=']763210lmnn2nmmm20nnmlk10';
        fdat[2]:='1llmnnn1oonnmnn10oonmlkj1jjkllmNnnnmmmnn0Nmlkji0jjkkllMmmmllMMlkkji0jjKLlllkklMmmmkkjji1jkkjjll1lllkkklll0l'+
        'llkjjji1jkkjjkk10llkjjkk10llkjjjh1jkkjjkk10llkjjkk10llkjjjh1jKll10mmkkkll10mmkkjji1';
        fdat[3]:='kklklll10mmlklll10mmlkkji1kLmm10nnmllmm10nnmlkjj1kLmm10nnmllmm10nnmlkjj1Jkkk10llkjkkk10llkjjih0gHI0IhI0Ihhg'+
        'g0eFF0gggFff0gggfffeeecDD0DDd0DdC0Babb10Babb10Baaa10';
      end;
      'n':begin
        fdat[1]:='U743mmnn10nnmlk210mmN1Nmlk2jlmmN0Nnmlkj1jjkMMMmlkkj0j';
        fdat[2]:='jkLLLllkkk0jjkLll1Lllkkk1jklllkkk10Llkkk1jklllkkk10Llkkk1jkLll10mmmlllkk1jkmmmlll10Mmlkj1klMmm10nnnmmlkj1kl'+
        'Mmm10nnmmmlji1ijKkk10llkkkjih0hhhIiii0IiihgfeFgFf0gggFedcddd';
        fdat[3]:='Dddd0Dddccbb0Bbabb10Baaa7';
      end;
      'o':begin
        fdat[1]:='S732Nnm31kmNnnnmlk21klmmNnmmllj2LMmmmlllki1lllklll1mmmlllkj1lll';
        fdat[2]:='L1MllkjhLL1mmmllkkjhLL10mmlkkkihkkklllmm10nmmkkjigKllmm10nnmkjjhgjjjkklmnn1nmlkjigf0iiijklmm1mlkihge1ggghij'+
        'kk1kjihfed10effghhiiihgfedc20deeFffedcc210ccDcccbb31bbbaaa4';
      end;
      'p':begin
        fdat[1]:='V75N10onmmk210lNnn0Nnmlk20klmnnMMmmllk1hijkmmmLl';
        fdat[2]:='LmL0ghiklllKKkLl0fgijkkkjjj10kkklllmmm1hjkkJ10kkkllkmmm10jkkJ2kkllklll10jKkk2LK10jkkklll2mmllkkjj10jkkllmm2'+
        'nmlkjjii10jkklmnn2nmlkihg2jkklmnm10mlkjigfe2jkllmnmmlkkjhgfed20';
        fdat[3]:='jkllmmllkjihfeddc20jllmmmlkjigfdccb21klMk1hfecb3klmnmlk3210ijkllkih320ghhijiihgf310eeffgggffedc31ccDddccbb3'+
        '2Bbaaa310';
      end;
      'q':begin
        fdat[1]:='U7430nooN1mkjh20llmNNmlkjh2lllMMmmlkjh10mmlllMMllkjh10';
        fdat[2]:='mmLll0Lllkjh1mmmllkkk10Llkjh1mmmLl10Llkjh1mmmLl10mmmllkjh1Lllmm10mmmllkjh1kkklllmm10nnnmlkjh1ijjkllmn10ooom'+
        'lkjh1hhikklmnn1ooomlkjh10ghiijklm1nnomlkjh10efgghijklmmnnmlkjh2deffhijklmmnmmlji20';
        fdat[3]:='ddefhijklmnnmlki21cdegh1lmonnmkj320lnoonmkj32jklmllkih31fghhijjjihgf30eeffGffedc3ccDdddccbb31Bbbaaa1';
      end;
      'r':begin
        fdat[1]:='Q65432nnoo1omlj20mmO0oomlji1jlmmnooo0onmkjihijkmmNnmlkihgfijklmmnmmlkjigfeehjklllmlkkjhgeddc0jkLkj0h';
        fdat[2]:='gedbb1jkLki1fdca10jklmmlki30jkMkj30jlmnnnlk30jkmnnnmk30ijklllkji210fghhijjihgf21eeffGfedd20ccDdddccb210Bbba'+
        'aa543210';
      end;
      's':begin
        fdat[1]:='O65320mN1kj2lmnoonnnlkih1klmnoonnmljigfijlmmnonnmljigfijklmnnn1ljigfhijlmN1kihfghjklmnnnml20hiklmNmlj10hjkl'+
        'mn';
        fdat[2]:='nnnmlki10jklNnmlki1i1mnoonnmlkihghi10nnnmlkjhgfghij1llkjihfeeffghIhggfed0deeFfeeddc1Cdddcccbb20b1bbaaa64321';
      end;
      't':begin
        fdat[1]:='P543lkk310nnmml30mooonnm3lmooonnm21ijlmNnmljigfghjklmnnnmlkihfefhijkMlkihfec0ghjkLkjhgedb10jklllkjh3jklllkj'+
        'h3jklllkji3jklll';
        fdat[2]:='lkj3jkllmmlk3ikllmmml3ijklmnnm3hikllmmm10h2ghijjkkkjihge10efgghhiihgfed2deeFeedcb20cdddcccbb3bbbaaa6420';
      end;
      'u':begin
        fdat[1]:='U74210mL21mlkjh1jlmmL10Mlkjh0ijkmLl1Mllkjh0ijkLll1Lllkjh0h';
        fdat[2]:='jkLll1Lllkjh1jklllkkk10Llkjh1jklllkkk10Llkjh1jkLll10mmmllkjh1jklllmmm10Mllji1ikllM10Nmlji1ijllmmnn10ooonmlj'+
        'i1hjkllmmn10onnmlkjh1ghijjkkll0Lkjihg1efgghhIIihgfed1eeeFGfffeeddc1c';
        fdat[3]:='Ccddd0dddcccbb20bbabb10bbbaaa70';
      end;
      'v':begin
        fdat[1]:='V74310ijlmnnmlkk2kjiiih0ghiklmmmL1kjihhhgefghjklmLl1kjihggfd0fgijkLm0';
        fdat[2]:='10jhggf20hikkllmn2jigf21gijkllnn10ljigf210hiklmnoo1kjhg3gijklnnnmljigf30hijlmmmlkihf31ghjklllkjigf310hiJjih'+
        'f32ghhihhhgf321fggFe3210eeeddd310';
        fdat[3]:='20dCb40bbba410aa7321';
      end;
      'w':begin
        fdat[1]:='`7651klmnmm';
        fdat[2]:='ll10mmmllkkk2kjiiihfghjklMmm0MmLl1kjihhhgefgijklllmmm0MmLl1kihggfed0fhijklllm10nnnMm2ihgfe2ghikklmnn10ooNn2'+
        'ihgf21gijklmnn10Onnn10kihf3hiklmnno0OoN1kihf3gijklmNnnn';
        fdat[3]:='Nnmkjhg31hijklllMMmmlkihf31ghjjKKKkkjhg32hIiiij0Jjjiihf32gghGg10Hhhgf321fffE10Fffee3210Dc20eeD4cccbb21Cb1';
        fdat[4]:='3210bba210bbba410aa3aa75320';
      end;
      'x':begin
        fdat[1]:='U7420jklnooN10ljihg1hijkmNnnn0lkjhgfeefgijklmmN0ljigfed0efhijklmnon10jhge10';
        fdat[2]:='1ghjklmnoon0kihf210hjklmoponlkih30jklmnoomlkih31klnnonmlkj31jklmnnnmmlk31klmmnnMl30jllMmmnnml210ijll1mmmnoo'+
        'nlk21hijk1Klmlkih10Gh10Ijjjihgf0Efff1Ggggffedccdc';
        fdat[3]:='cddd0Ddeeddccbb0Bbb1Bbbbaaa7';
      end;
      'y':begin
        fdat[1]:='U7420klnoomlll10lkjiih1iklmnnmL0lkjhhhgfgijklnnmL0kjigggfefhijkmmmlll10jigffe1';
        fdat[2]:='ghikllM2igfe2gijkllmnn10jigf21hikllmno10jig210gijklmoon0ljig3hiklmnonmkjhg3gijklnnmlkih31ijklmmmljig31ikllm'+
        'llkjhg310klllkjjig32lmmlkjihg320nmljihhf3i';
        fdat[3]:='kl1nmkjhgg3ijklm1lkihgf3ijkll1kjhge3fghijjj0ihged30fgghhhggfed31dEeeddc32Cccbb321bbaaa320';
      end;
      'z':begin
        fdat[1]:='Q65430jlmnOnmljig1ijklNnnmkjhg1hijkMmmlkihf0eghijllmmmlkjhg1efgh1lmmlljigf1dfg10lmmlkjhg2fg1llmllk';
        fdat[2]:='jhg3lmmllkjh3lmnmmllk3lmnnnmmml3lnooN10ih10lmnooonnn10jhg0ijklmmlll10jhgf0ghiJjjjihgfedeffggHhggfedd0DEeeed'+
        'dccb0BBBaaa54310';
      end;
      'A':begin
        fdat[1]:='\310klllk421klmmmlk420klnnnmlj42lmnoonlk410klmnoonmlj41jlmnoonmlk40ijklmnnnmlkj4hijllmmmllkkj3210';
        fdat[2]:='ijklMmllkj321ijjll0Mmmlki320jkkl10Nmlkj32jklmm2ooonmlk32kklm20nooonmkj31jkkLlllmmnnnmlk31jKKkllmnnnmlj3KkjI'+
        'jjklmnnnmk3llkkjiHhij';
        fdat[3]:='lmnoonlk21lllkji210jlmnoonmk21jkjjh3ijklmmlkjh10ghiiihgf21fghiijjjihgfeeeffgffedd20eeffGffedccDddccb20cDeed'+
        'dccbb0Bbaaa210Bbbbaaa543';
      end;
      'B':begin
        fdat[1]:='W0jklmNNmmlk21hijlMMmmmllkk2gijkLLLllkj2hjkLll1Llkkjh2jkLll10lllkkkih2jkLll10mllkkjhg2jkllM10nmllkjhg2jklmm'+
        'nnn10nnmlkjig2jklmmnnn10nnmmlji20jklmmnnn1nnmmmlk21';
        fdat[2]:='jklmmnMmmmlml210jkllmmLLmmmlj2jkLKKlmmlki10jklllkjj10kkklmmmkj10jklllkjj2kklmmmkjh1jkLkk2llmmmlkih1jlMll2Ml'+
        'kjhg1klmnnnmm2nmmlkjigf1ijklllkk2lkkjihged1hiiJj10jjiihgfed1ffgg';
        fdat[3]:='HHgggfeddc1DEEeeddccbb10BBBbbbaaa73210';
      end;
      'C':begin
        fdat[1]:='W30Nnn321MMMlkj210lmmmllKLkji20lmmllkjIjjkkjji2lmmmljihGhijiiihh1kmmmlkihf2fghGf0jlmmmljig21effE0klmmmkjh3D'+
        'cc0klmmmlji30ccbbbjklmnmlji30baaa0ik';
        fdat[2]:='lmnnlki321ijlmnnmlj321ijklmnnmk321hiklmnnnmk320gijklmnnnm321hijkmNm31g1fhijkmNn3ihf1fghjklmNn2kihfe1eefghij'+
        'kLlkkjhgfed10deefghhIiihgfedc21';
        fdat[3]:='ddeeffgggffeddcb3cccDddccbb320Baaa740';
      end;
      'D':begin
        fdat[1]:='Y0jklmNNnmmm3hijlmmnnMMmmml20gijklmmmLLlmmml20hjkLkJjjjklmmlk20jklllkji10iiiklmmmlj2jklllkji2iijkmmmlki10jk'+
        'lllkjh20ijklmmmkj10jklllkjh20hjklmmmkjh1jklllkjh21jklmmmkjh1jklll';
        fdat[2]:='kjh21jklmmmkjh1jklllkjh21jklmmmkjh1jklllkjh21jklmmlkih1jklllkjh21jkmmmlkih1jklllkji21klmmmljig1jkLkj21lmmml'+
        'kihf1jlMlk21mmmlkjhg10klmnnnmm20nnmmljigf10kmnoonnn20nmmljigfd10jklmmlll10ml';
        fdat[3]:='lkjihfed10ghiJJjjjihgfedc10effggHhhhgggffedcc2DEEedddccbb20BBBbbaaa710';
      end;
      'E':begin
        fdat[1]:='V0jklmNMmmlkjih1hijlmmnnmmlKkjihg1gijklMlkkjjjiihgfe1hjklllmllkjiihgggfed10jkLlk21ggfdc10jklllmml210gedb10j'+
        'kllmnnn10ji10ec2jklmmnnn1ljig3jklmmnnn1ljig3jklmmnnn1ljig3jklmmNm';
        fdat[2]:='kjhg3jkllmnnnmljihg3jklllmmlljihgf3jkLlk1hggf3jklllmll1iihh3jlMml1jjj2ihg1klmnnnmm10ll10jihg1ijklllkk3kjig1'+
        'hiijjjii21kjihgf0ffggHhhhIihgfeDdEeeeFffeedd0bbb';
        fdat[3]:='BbbbCddCb3210bbbaaa721';
      end;
      'F':begin
        fdat[1]:='U0jklmNMmmlkji1hijlmmnnmmllKjiih0gijklMllkjjjiihhgf0hjklllmllkjiihhggfed1jklllmll20hggfdc1jkllM210gedb1jklm'+
        'mnnn10ji10ec10jklmnooo1lkih210jklmnooo1lkih210jklmnOmljig210jklmmNmkjhg21';
        fdat[2]:='0jkllmnnnmljigf210jklllmmlljigfd210jkLlk1hfec210jklmmmlk1hfec210jlMlk1gfd3klmnnnmk10fe3klmnnnmk320ijklllkjh'+
        '310ghhijjjihgfe3eeffGffedc3ccDdddccbb31b';
        fdat[3]:='Bbaaa73';
      end;
      'G':begin
        fdat[1]:='[30nooNn1mlj310mmnnmmmlllmmmlkji30MmlkkJjjjihgf210MlkjihhGhggffe21lmmmlkjhgfedddeeedddc20lMkjhge20Eedd2klmm'+
        'mljih30gffe20klmmmljih40jklmmmlji210';
        fdat[2]:='310jklmnmlji41iklmnnlki210klmnnnmkj1ijlmnnmlj20iijklmmmlkihfijklmnnmk20jjjkLkjhgehiklmnnnlk2Kllkkjigfdgijkl'+
        'mnnml210lllkjihf10hijlmNm21mlkjihg2ghijlmNn20nlkjigf20fgij';
        fdat[3]:='klmN10nmkjhge21efghijkkLllkjihfed210deffghiiJihgfedc30cddeeffGffedcc32cccDdddccbb4Bbaaa64210';
      end;
      'H':begin
        fdat[1]:='Z1klmnnmlkj2kllmnnmkjh0hijlmmnmkjjj1jjklMkjhggijklmmlkjii1iijklmmlkihf0hjkLjihh10hjkLjig10jkLkj21klmllkjh2j'+
        'kllmmlk21lmnmlkjh2jklmmnml21mnnmlkjh2jklmnnnm21ooonmkjh2jklmnnnm21ooonmkjh';
        fdat[2]:='2jklmnnnMMnnmlkjh2jklmmnmlKkklmnmlkjh2jkllmmlkJjjklmllkjh2jkLkjHhhjklllkjh2jklllkjh21jklllkjh2jkLji21jkmmll'+
        'ji2jlmmmlkj21klmmmlki2klmnnmkj21llnnnmkj2klmnnmkj21llnnnmk';
        fdat[3]:='j2ijkllkii21jklllkih10ghhijiihgg10Hijiihgf0eeffgggffeee1FgggffedcccDddC1ccDddccbb1Bbaaa21Bbaaa6521';
      end;
      'I':begin
        fdat[1]:='L1klmnnmkjh0hijlMkjhggijklmmlkihf0hjkLjig10jklllkjh2jklllkjh2jklllkjh2jklllkjh2jklllkjh2jklllkjh2jklllkjh2j'+
        'klllkjh2jklllkjh2jklllkjh2jkLji2jlmmmlki2klmnnmkj2klmnnmkj2ijkllkih10ghh';
        fdat[2]:='ijiihgf0eeffgggffedcccDddccbb1Bbaaa631';
      end;
      'J':begin
        fdat[1]:='T31klnnnmkjh30hiklmnmmkjhg3hijkmmmlkihf3ghjkLjig310jklllkjh32jklllkjh32jklllkjh32jklllkjh32jklllkjh32jklllk'+
        'jh32kklllkjh32kl';
        fdat[2]:='lllkjh2jii20Lkkjh10kjjjk2Lkkih1iKll10mmllkkih1ijjkklm10nnmlkjig1hijjklm10nnmlkihf1ghijklm10nmlkihf10efghijk'+
        '1llkjigfe2effghIihgfedc20deefffggffedcc210ccDddccbb31Baaa320';
      end;
      'K':begin
        fdat[1]:='[1klmnnmkkk2kklllkihf10hijlMK1kjjkkkihfe10gijklmmlK1jiijjihfecb10hjklllmlll10jiiihgfdcb20jkllM2kihhge30jklm'+
        'mnnn2kjihf31jklmnooo10mljih310jklmnooo1onmkj32jklmoPpoonl';
        fdat[2]:='k32jklmoPpponml32jklmoPppponmk310jklmnOOnmlk31jklmNNnnnmlk30jklmNNnMl30jkmmnMMmmmnml3klmnnmLLlmnnml210klnnn'+
        'mlk10Klmnnml21kmno';
        fdat[3]:='omlk10Jklmnnmk2ijklmlkjih10hhhijkllkji10ghiijjihgf2ggghhijihgf0eeffGfedd2eeffgggfeedcDeeddccb20Deddccb0Bbbb'+
        'aaa21Bbbbaaa6320';
      end;
      'L':begin
        fdat[1]:='T1klnnnmkj31hiklmnmmkjhg3hijkmmmlkihf3ghjkLjig310jklllkjh32jklllkjh32jklllkjh32jklllkjh32jklllkjh32jklllkjh'+
        '32jklllkjh32jklllkjh1';
        fdat[2]:='31jklllkjh32jklllkjh32jkmmmlkj32klMlk210hg10klNml21jihg1kmnoonnm21kjhg0ijkllmlll20kjhgf0ghiiJJjihgfedeeffgg'+
        'Hhhhggfedd0cDEEeddccb1BBBbbaaa210';
      end;
      'M':begin
        fdat[1]:=']0jklmmlk32lmnnmlji0hijlmmmlk31klnnnmkjhggijklmmmkj30lmnonlkihf0hjklmnnlk3klnoomljig10jklmnnmlj210lmnoomlji'+
        'g10jklmnonml21mmnopnmkjh2jklmooonmm20nnoopnmkjh2jklmoponnn2Oppnmkjh1';
        fdat[2]:='1ijlmnooonnn10Ooonmkjh2ijklnOooo0Oooomlkjh2hiklmNNNnnnmlkjh2hijklNmmmNNllkjh2ghijlMlllMMllkjh2gghi1lllkkkll'+
        'l1llmllkjh2gghi1lllkjjkll1llmlllji2hhij1llkjiikkk1llmmmlki10';
        fdat[3]:='0iiij10lkiiijk10llmnnmkj2jiij10kihggij10llnnnmkj2ihhi2hfffg2jklllkjh10Ggg10fedee10hhiijjihgf0eefEe1dccc10FG'+
        'feddcddCc10bbb10Ddeeddccb0bbbaaa20a20Bbbbaaa5';
      end;
      'N':begin
        fdat[1]:='Z0jklmnml31jKji0hjklmnnml3iJjihghijlmnnnml210ijIhhfgijklnoonml21jjiihhhgf0hjklmoponml210jihhg2jklmopponml21'+
        'kjihh2jklmoppponnm20mkjih2jklmoPoonn2nlkih2ijkmnOOo10nmkjh';
        fdat[2]:='2ijklmNnnooPnmkjh2iijklMmmnnopppnmkjh2iiijjKkklmnoppnmkjh2hhhiiJjjklmnopnmkjh2Hh10hhhjklmnonmkjh2iH2gghjklm'+
        'nnmkjh2Ih20fghiklmmlkjh2Ji21fghijlkkjih2kkkjj210efgijiiih';
        fdat[3]:='g2ijiih3efGgff10gHgf3dEedd1eeFedd30ddC1cDcccb31Ba10Baaa320aa65210';
      end;
      'O':begin
        fdat[1]:='Z30nOnnn3210lmmNNnnm310LLllmNmk3LKkkklmNlk21lmmlkjIiijkmnnnmkj2lmmmljih10hhiklmnnmlj10klmmmkjh20gijklnnnlki'+
        '1klmmmlji21hiklmnnmkj0jklmnmlji21hijkmnnmk';
        fdat[2]:='jhjklmnnlki210ijklnmmkjhiklmnnmkj210hjklmmmkjhijlmnnnlk3jklmmlkihijklnnnmlj210jklmlljighijlmnnnmk210klmmlkj'+
        'hgghiklmnnmml21mmmllkihf0ghjklmnnmm21mmmlkihf1fghiklmmnnn20nmlkjhge10efhijklmnnn10nmlkihf';
        fdat[3]:='e20efghijkLllkjihgfdc21defgghiijjjihhgfedc3ddeeffGffedcc32ccDdddccbb3210Bbaaa65320';
      end;
      'P':begin
        fdat[1]:='U0jklmnnOnnnml20hijlmmNNmmlk10gijklMMmmmllki1hjkLLLllkj10jkLll1Lllkjh1jkLll10Llkjh1jkLll10mmmllkih1jkllM10n'+
        'nmlkjig1jklmmnnn10nnmlkjhg1jklmnooo10nnlkjhge1jklmnonn1mmljihfe1';
        fdat[2]:='0jklmnnnmmlkjihgfed10jklmmnmlkjihgeddc2jkllmmlkjigfdcbb20jklmmlkjigfdca210jlmmmlki320klmnnmkj320klmnnmkj320'+
        'ijkllkih32ghhijiihgf31eeffgggffedc30ccDddccbb310b';
        fdat[3]:='Baaa730';
      end;
      'Q':begin
        fdat[1]:='Z30nOnnn3210lmmNNnnm310LLllmNmk3LKkkklmNlk21lmmlkjIiijkmnnnmkj2lmmmljih10hhiklmnnmlj10klmmmkjh20hijkmnonlki'+
        '1klmmmlji21hjklmnnmkj0jklmnmlji21hijlmnnmk';
        fdat[2]:='jhjklmnnlki210ijkmnnmkjhiklmnnmkj210ijklmmmkjhijlmnnmlj210hjklmmlkihijklnnnlki210jkLjighijlmnnmlj210jklllkj'+
        'hgghiklmmmlk210klllkkihf0ghjklmmmll21lllkjihf1ghiklmnnmm21mlkjihge10hjklmNn20lkjihf';
        fdat[3]:='e20jlmnOoo10mlkjhged21kmnnoopppoonmlkjigf3lmnnooppoonmlkjj210ghijklmmnOnmmlk210defghhijklmNn30bccddefghijkl'+
        'mmN20hg20cdeefghijkkLkjihfe210cddeffghIiihgfed31cddeeffggffe';
        fdat[4]:='ddc320cccDdccbb40Baaa2';
      end;
      'R':begin
        fdat[1]:='Z0jklmnnOonnnmm30hijlmmNNnnnmlj21gijklMMmmmnmlki21hjkLLLmmlki210jkLll1Lmmlkih21jkLll10Mlljig21jkllM10nmmllk'+
        'ihf21jklmmnnn10nnmlkjigf21jklmnooo10oonmljig10';
        fdat[2]:='2jklmnooo1ooonmljig210jklmnOoonnmmlk30jklmmNnnnM31jkllMMmmnnml3jkLLLmnnmkj21jkLkk1Lmnnmlj21jlmmmlkk10lllmnn'+
        'nmkj20klmnnmlk10Lmnnnlk20klnnnmlk10Klmnmlk';
        fdat[3]:='20jklllkji2iiijkkkjih10ghiijjihgg10ggghhiihhgf0eeffGfeee10eefffgffedccDeeddccc10ccDdccbb0Bbbbaaa21Baaa6521';
      end;
      'S':begin
        fdat[1]:='U210Nnnn1lk3mNMmlkji20lmnnnmmLlkjiih10klmnonmmlllkkjihhg10klmnonmmlllkkjihgfe0iklmnonmm2kjihgfe0ijlmnoonn21'+
        'jigfe0ijklnOon20jhg1hjklmnoopppoonm21hijlmnnoooppponmk20ijklmmmnnoopponlk10';
        fdat[2]:='0hijkkkllmmnooonmkj2ijkjjjkklmnnonmkjhhhi1Kkklmnnnmlkihhiij10Llmnnnmljighijj20Mnnmlkjhghijkk210nnnmkjigfghi'+
        'jklm20onmkjhge0efghijkLllkjihfed1effghiiJihgfedc10cddeeffGffedcc21cccDdddccbb210';
        fdat[3]:='10Bbaaa720';
      end;
      'T':begin
        fdat[1]:='X10kmnOonnOonlki2jklNNNnnlkih1iijkMMMmmkjhh1gghjkklllmllmlllkkihFeefhi1kLlkk1ihfeeDefg1jkLkj1hfeCbbce10jkll'+
        'lkji10edbbb0aac2jklllkji2caa30jklllkjh4jklllkjh';
        fdat[2]:='4jklllkjh4jklllkjh4jklllkjh4jkmmllji4klmmmlki4kmnnnmkj4lmooonlk3210klmooonlk3210ijklmmlkih320ghh';
        fdat[3]:='iJihgf310eeffGgffedc31ccDeeddccbb32Bbbbaaa74210';
      end;
      'U':begin
        fdat[1]:='Z1klmnnmkjh210jjkjjih0hijlMkjhg20hijjjiihggijklmmlkihf20gIhhhf0hjkLjig210Hhgg10jklllkjh30hhhgg2jklllkjh30hh'+
        'hgg2jklllkjh30hhhgg2jklllkjh30hhhgg2jklllkjh30hhhgg';
        fdat[2]:='2jklllkjh30hhhgg2jklllkjh30hhggg2jkLji30hhggg2jkLki30hhhgg2ikllmmlj30iihhg2ijllmmml30jihhg2ijklM30jihg20hij'+
        'klmnnm210lkihg20fgijklmnnn20mkjhge';
        fdat[3]:='21fghijkllmmmllkjihfed21defgghiiJihgfedc3ddeeffGgffedcc310ccDDccbb321Bbbaaa65320';
      end;
      'V':begin
        fdat[1]:='Y1klmnnmlj210jKjh0hijkmnnmlkih2iiJihfghiklmmmlkih2Iiihgefghjklmllkih20Hhgfd1hijLkj21hhhggf20ijklllkj21hhggf'+
        '21hikLl21ihgg210gijklllmm2kihgg3hikllmmn2kjhgf3hij';
        fdat[2]:='klmnoo1lkihg31hjklmnoo1lkihf31hijlmnoonmkjhg32ijklnoonlkihf32hiklmnnmljig320gijklmmlkjhg321hiklllkjig3210gi'+
        'jjkjjihg4hhiiihhgf4fgggfff';
        fdat[3]:='e41Eddd41dCb42bbba421aa7210';
      end;
      'W':begin
        fdat[1]:='c1klmnnmll2mmmnnmlkj20kkjjih0hijkmnnmmlll0Lmnmlkkk1kjjjiihgfghiklmmmL0Lmmmlkjj1jjihhhgfefghjkLkk1Llmllkj10j'+
        'ihgggfe10hijLkk10lllmmmlkk20hggff20ijkLl2Mmmll20hggf21hikLm2Nnnm';
        fdat[2]:='m2jigf210hijkllmnn10OoN10jigf3hjkllmno10OOo1ljigf3hijllnooo0OOoo1ljig31ijklmoon0OOoonmkjhg31hiklmnoNNnnnoml'+
        'jig310gijklmnMMmmnnmkjhg32hikl';
        fdat[3]:='mmLl0Lmmlkihf32hijklkkkjj1kkklllkjig321hjjkjjjii1jjjkkkjjhg321hijjiiihh10iijjiiig4hiHg2Hhhgf4gggfffe20ggFe4'+
        'Edd21eeeddd210';
        fdat[4]:='310ddccc21dCb41cB210bbba42aaa30aa730';
      end;
      'X':begin
        fdat[1]:='Z10klmoooM10llkjjig2gijklNmmm1llkjihgfd10fgijklmNnm0lkjihgfdc10efgijklmmnnn10jihfedc21fhijklmnnn10jhge31ghi'+
        'jlmnonm0kihf32ghiklmnnmljig321gijklmnmlkih321ghiklMkj210';
        fdat[2]:='30hiklmnnml41jklmnonml41kmnooonmk40lmnooonmlk3210jklmnnnmmmlk320jjklMMl320jkkLllmnnml310kkkll0Lmnonnl30llkk'+
        'k10lllmnoonmk';
        fdat[3]:='210kkjii20jjklmmlkjh10ghhiihhgg10hhhiJihgfeeeFffee10fffGgffedcccDddccc0Dddeeddccbb1Baaa20Bbbbaaa6521';
      end;
      'Y':begin
        fdat[1]:='X0ijkmnonmlll2lkkkjih0fhijkmnnMm1llkjjihgeefhijkMmmm1lkjiihged0efhijklmmmn10lkihgfec2ghijklmnn2jigfe210ghik'+
        'lmnnn1kjhge30gijklnnnmljigf31ghijlmmmlkihf32gijklmlljig321hjkLji';
        fdat[2]:='g3210jklllkjh4jklllkjh4jklllkjh4jklllkjh4jkLji4jlmmmlki4klmnnmkj4klmnnmkj4ijkllkih3210gh';
        fdat[3]:='hijiihgf320eeffgggffedc32ccDddccbb321Bbaaa743';
      end;
      'Z':begin
        fdat[1]:='U2jlmmNnooomlji2ijklMmnnonmkjhg1hhijKllmnnmljigf0fffghijjjklmmmlkihf1ddeeg2klmmmkjhge1ccde20lmmmljigf10bcde'+
        '2lmmmlkihf20cd20lmmmkjhge310lmmmljigf310lmmmlkihf32Mljigf21';
        fdat[2]:='20lmnmlkihg310lmmmlkjih32mmnmlkjih310lmnnmlkjj310lmnonnmlkk20g20lnooonmml20ihg10kmnooonnn20kjhg1ijklmmmlll2'+
        'kjhgf1ghiiJJjihgfed0eeffggHhhhggfedd1cDEEeddccb10bb';
        fdat[3]:='BBBaaa70';
      end;
      '0':begin
        fdat[1]:='S6noonm310lmnOnmlk210llmNnnmlkk20lllMMllkj10kLMmmmllkki1kLll1Llkki0jkLll1Llkkj0jklllkkk10Llkjhiklllkll10Llk'+
        'jhijll';
        fdat[2]:='lkll10mmllkkjhijkLm10mmmlkkihhikkllmm10nnmlkjiggijkklmn10onmlkjhg0hijklmnn1onmkjigf0fgijklmn1nmljigf10fghij'+
        'kkllkjihged10defgghiiihhgfed20ddeeFfeddc3ccdddcccbb310bbbaaa51';
      end;
      '1':begin
        fdat[1]:='P54210onmkjhg21mnonnlkihf2jklmnnmljigf10hijkmnmlkjh2eghijlmllkjh20fghjklllkjh3jklllkjh3jklllkjh3jklllkjh3jk'+
        'lllkjh3jkLji3klmm';
        fdat[2]:='mlkj3lmnnnmlk3mnoonnml3mOnnl210klMllki10fghhiJjjihgfeeeffggghhggffedcccDEddccbb1BBbaaa651';
      end;
      '2':begin
        fdat[1]:='S543210ooppoonm30mnOooonlk20jklmNnoonmkj10ghijlllmmnnonmkjh0effgijkkllmnnnlkih0dddfghijklmmnmljig0bbcde10kl'+
        'mmmlkihf1bb20klmmmkjhge30llmmljigf21';
        fdat[2]:='10klmmlljig31lMlkjh30lmnnnmmlj31mnonnnmm31mnOnn2i20mnoppO1mkjh10jklMNmkjih1hiiJkklllkjihg0ffggHhhiiihggfeDE'+
        'eeFfeeddBBbbccdCb3210bbba31';
        fdat[3]:='21aa6321';
      end;
      '3':begin
        fdat[1]:='T60mnnooonn31ijkmmnnooonlk20efghjkllmnoonlki2ddfghijklmnnmljig10cdefgijklmnnmljig30klmnnnmljig31mnnonnlkih3'+
        '1nOnmkj31mnooo';
        fdat[2]:='nnm30gijklmnnnonml21efghikllmnoonlki2cdefhijklmnonmkj20cdefhijkmnonmkjh30jklmnonlkih31lmnnnmljig31Nnmkjhg31'+
        'oonnmkjhge30oonnmkjhge1hijkllmmmllkjigfed0efghhiJiihgfedc1ddeef';
        fdat[3]:='fGgfeedcb2cccDdddccbb30Bbbaaa610';
      end;
      '4':begin
        fdat[1]:='S621kjih321llkjh32lnmlkjh310lmnnmkjh310mnonmkjh31lnnonlkjh30kmnnnmlkjh3jklmmnmlkjh210ijklllmllkjh21';
        fdat[2]:='ghij0kLkjh21hij1llmmllji20hij10mmnmmlki2iij2Nnmkj2jkl2Oomlj10jkl20Ponlk10ijklmnnnOonlkihfghijkllmmNmljigeef'+
        'fhijkklmnnnmkjhgcdddeghijklmmmlkihf0bbbcdefghiJigfe30gghhhgfe310';
        fdat[3]:='eFeed310Ddccb310Bbaaa53';
      end;
      '5':begin
        fdat[1]:='R5431lmnoPpnmkjh10klmoPponlkih10klmNnmlkjigf1jkklmmllkkjihgfe1jklmmmlkjihgfedc1kllmmmlkjigfedc1jklMlkjigfed'+
        '10kklmnm32klmnoo310ijlmnoppooon2';
        fdat[2]:='1hijkmnnOonmk10fghiklmmnnooonlk1eefgijklmmnoonmkj0ddefhijklmnoonmkjh1fghijklmnnonlkih21kllmnnnmkjhg3mnnmmlj'+
        'igf3nmmlkjhgedfgh2llkkjhgfed0ffghIiihgfedc1deefffggffeddcb2ccDddccbb3Baaa20';
      end;
      '6':begin
        fdat[1]:='S431kkji321lmlkjig310lmmmlkjh31lmnnmmlk31lmNmm31klmnooon310lmmO0oonml20klmmnOoonnmlj10klmmNNmmlki0jkllMMmml'+
        'lkj0jklllMMmllkjhikll';
        fdat[2]:='L1mmmllkkjhijLmm10mmmlkkihijklllmm10nnmlkjighijkllmn10onmlkjhgghikklmno1onmkjigf0ghijkmmn1nmkjhge1efghijkll'+
        'lkjihfed10effghhiiihgfedc20deeFffedcc210ccDcccbb31bbbaaa510';
      end;
      '7':begin
        fdat[1]:='S53klm3210klmnPPonlkih0klmoPPonlkihijklmNNnlkjigiijkLlllmmlkjihgghhiiJjkllkjiih0Fggghhiijjjiihg0Dddeffghiii'+
        'hhhg0cB210iihhg10aaa3iiihg21';
        fdat[2]:='210Ihg320jiiih320jjiihg320jjjihg32Jih320kjjjig32kkjjihg32kkkjih32Kjig32kkjjihg310hiihhgf32Gffe310E';
        fdat[3]:='eddc310Ccbb321bbaaa5320';
      end;
      '8':begin
        fdat[1]:='S43mmNmml3lmmmNnmmlkj2lmmNNmlkji1klmNNmmlkjihiklmmnnn10nnmlkjigijklmnnn2onlkjhgijklnOo1onmkjhghjklmoPpponmk'+
        'jh0hijlmnOooonmkj10ijklmNnnonmlj2jklMmmmnnmlki1jjk';
        fdat[2]:='kLlllmmmlkj1jjKKklmllkjhijjjkk1Llllkkihijjjkk2mmmllkjighijjkk20mmlkjihfghijjkl20mljihfeefghiijk10lkjhgfe1ee'+
        'fghhIihgfedc10ddeeffggffeedcb20cccDdccbb31Baaa51';
      end;
      '9':begin
        fdat[1]:='S543210nooonnm31mnOonnml21llmNNmllj10klllMMmllki1klllMMmllkj0jkLll1Mllkjhiklllmmm1MllkjhijllM10nnnmlkjhijkl'+
        'mmnn10oonmlkihhijl';
        fdat[2]:='lmnoo1ooomlkihghiklmnoo1ppomljig0ghijklmnOnmkjhg0efghiklmNmljig10deghijlMlkjhg2dfghikllmlljigf20efhijkllkki'+
        'hf310lllkjhge31Kjigf310Jigf310Hhgfd31Ffeed31D';
        fdat[3]:='ddcc32ccB3210aaa5321';
      end;
      '.':begin
        fdat[1]:='H65431K10Kkk0ijkjjkkkghiHhffgFfdeeDd0cccbbb10baaa541';
      end;
      ',':begin
        fdat[1]:='H65431lkjj10llkkjj0kkkJjiijiihiighhhgggh0gggF1fffeee2E2ddd20dcc2ccb2bbb20aaa310';
      end;
      ':':begin
        fdat[1]:='H531kkkj10Kki0ijkjjjiighihhhggffgfffeeeffEd0fEd10fffe42kkkj10lKi0kkkjjjihjiihhhgghggfffeefeeDc0cccbbb10baaa';
      end;
      ';':begin
        fdat[1]:='H531kkkj10Kki0jjkjjjihhhihhhgfffgfffeeeffEd0fEd10fffe42kkjj10jkkkji0iiJiighiiihhhfghhG0fggF1fffeee2E2ddd20d'+
        'cc2ccb2bbb20aaa310';
      end;
      '!':begin
        fdat[1]:='H1kkkj10kllkkj0jkllKjjKkkjjkkJiJijIiihih0hhhG0Fee1eeD10Dc10eddd20fe420kkkj10Kki0jjkjjjihhhihhhgfgfgfffEeDc0'+
        'cccbbb10baaa541';
      end;
      '?':begin
        fdat[1]:='P20mnnnml3klNnmlk20jkmNnmlkki1jjklmnnnmmkkji0jhhikl1mmlkkjhhhgghjk1llkkjiggfefgij1kkjjigffddefgh0Ihgfe1bcde'+
        '1Gfed3Eedd30eeddd310eeedd32ffe4321';
        fdat[2]:='3210kkkj310Kki30ijkjjjih3ghihhhgf3ffgfffee3deeDc30cccbbb310baaa6520';
      end;
      '(':begin
        fdat[1]:='M30ihgf210kjihge21kkjigf20llkkjhg20kllkjih21Ljig20kmmlljig2jlmmlkjh20klmmmkjh20klmmmkjh2jklmmmkjh2jklmmmkjh'+
        '2jklmmmkjh2jklmmmkjh2iklmmmkjh2ijlmmlkjh2ijkmmllji2hjkLji1';
        fdat[2]:='1hijkllkji20hiKji20ghjkkkji20fghJii20fghIi21fghhihh210efGf210eefeee3Ddc3cccbbb30A';
      end;
      ')':begin
        fdat[1]:='Mghhi30ghiijj3hiJ3hjjkkkj21hijK210ijklllk21ijkLj20hjklmllki20jklmmlki20jklmmmkj20jklmmmkjh2jklmmmkjh2jklmmm'+
        'kjh2jklmmmkjh2jklmmmkjh2jklmmlkih2jklmllkih2jkLj';
        fdat[2]:='ig2jklllkihf2jklkkjig20Kkihf20kkkjjhge2Jihge20ijiihfe21hhhgfec20Fedc20eDc21cccbbb210A30';
      end;
      '<':begin
        fdat[1]:='R6210g3210jhg32lkjigf31llkjhged3lkkjhgfedc20mllkjhgfedcb2lmmlkjhgedc20kllmmljigfd21ijklllkjih3hijjkkk30';
        fdat[2]:='1fghijkk310eefghijjk30cddefghijjk31ddefghijjkj30cdeefghiihh30cddefggfffe31ddeeD32Cbb321bbaa40a5410';
      end;
      '>':begin
        fdat[1]:='R5421g40hhi3210ghijjk32efghijkk31deefghijjk30cddefghijkkk30defghjklllk30fghiklmllkj30ijklmlkjih310llkji';
        fdat[2]:='hf310lljihfe30lkjjhgfdc210lkjihgfedcb2klkjihfedcb20iiijhgfedcb20fGgfedcb210Eddcb31Cbb32bbaa321a62';
      end;
      '+':begin
        fdat[1]:='R650gfff321gfff321H321jjji321llkk321nmmm321nmmm210ggijkLLkih';
        fdat[2]:='feefghiKKihfedddefhIIgfdcbbbcefGGfdcaa210gfff321gfff321feee321eddd321dccc321cbbb321baaa5431';
      end;
      '-':begin
        fdat[1]:='J653210klmnnlkih0ijklkjihfgghiiihgfeeffggffedcDddccb0Bbbaaa543210';
      end;
      '=':begin
        fdat[1]:='S743gghiJJjjihgfeeffggHHhgfeedeffggHHhgfeedeffggHHhgfeed2';
        fdat[2]:='5420gghiJJjjihgfeeffggHHhgfeeDdEEeeddccBBBBbaaa653';
      end;
      '*':begin
        fdat[1]:='N21jj310L31mnmm21ijk0M0kih0gijkl0nn0lkihfghjklmnnmljigfghijklmmlkihge0ghi0kllk0hgf20ijjjih21efghhhgfed2ddee'+
        'feedcc2bccc1ccbb20bbb1aaa5420';
      end;
      '/':begin
        fdat[1]:='J210ffe21fffe21ffee21fee210ffe21fffe21ffee21fee210ffe21fffe21ffee21ffe210fff21F21fffe21fff21F21F21fffe21fff'+
        '21F21F21E21d';
        fdat[2]:='dd21dccc21cbbb21baaa431';
      end;
      '&':begin
        fdat[1]:=']3lmmmkjh210Kj3kklllkjhgf20jklkkji21jkkklkjigfee10ijjkkkjih2jjKjigfddd10hijkkkjih2jKji10edd10hj0kkjjig2klll'+
        'kj320lkjihf2klllkj32lkjihfe2llmlkjj31lkjihfe0';
        fdat[2]:='2lmmlkjji3llkihfec10Llkkjjii20nmlkjhged10kLkjjiih10kmnmlkjigf20kLkj21jklmlkjih21ikllmmkk2iiijklkjji210ijllm'+
        'mll2Jklkjjih21ijklM2kkk0klkjiih21hijlmmnmm3lkjihg21g';
        fdat[3]:='hijlmmnnm210lkjhgf210fhijklmnnn20mkjhged210efghijkllmmmllkjigfed30defgghiijjjiihgfedc310ddeeffGgfeedcb321cc'+
        'Ddddccbb41Bbbaaa';
      end;
      '"':begin
        fdat[1]:='J0gh2hf0ffhi1igfeefgh1hgedeefg0qgfDef1fecc0cd2dc1cc2cb1bc2bb1bb2ba1aa2a';
      end;
      '''':begin
        fdat[1]:='D0fe0fEeedeDdcc0cc1bb1bb1aa1a';
      end;
    end;
    if fdat[1]<>'' then
    begin
      xx:=textx;
      yy:=texty;
      if textstring[t] in ['a'..'z'] then dec(yy);
      width:=ord(fdat[1,1])-65;
      j:=1;
      i:=2;
      repeat
        repeat
          case fdat[j,i] of
            '0':incxx;
            '1':for q:=1 to 2 do incxx;
            '2':for q:=1 to 4 do incxx;
            '3':for q:=1 to 8 do incxx;
            '4':for q:=1 to 16 do incxx;
            '5':for q:=1 to 32 do incxx;
            '6':for q:=1 to 64 do incxx;
            '7':for q:=1 to 128 do incxx;
            'a'..'p':putpix(ord(fdat[j,i])-96);
            'A'..'P':for q:=1 to 4 do putpix(ord(fdat[j,i])-64);
          end;
          inc(i);
        until i>length(fdat[j]);
        inc(j);
        i:=1;
      until fdat[j]='';
      textx:=textx+width;
    end else textx:=textx+16;
    inc(textx,space);
  end;
end;

begin
  writeln('SMFONT30 - Font by Remco de Korte - Soft Machine');
  delay(100);
end.
