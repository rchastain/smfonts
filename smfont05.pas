unit SMFONT05;

{$MODE DELPHI}

interface

uses
  //crt, graph;
  ptcGraph, ptcCrt;

procedure font05_1(textstring: string; textx, texty, textcolor, space: integer);
procedure font05_2(textstring: string; textx, texty, textcolor, space: integer);
procedure font05_3(textstring: string; textx, texty, textcolor, space: integer);
procedure font05_4(textstring: string; textx, texty, textcolor, space: integer);

implementation

procedure font05_1;

var
  d, t, e, h, l, xx, yy, w, c{, z}: integer;
  fdat: string;
  n: char;

begin
  //for t := 1 to length(textstring) do
  t := 0;
  while t <= length(textstring) do
  begin
    inc(t);
    fdat := 'EDH0a';
    case textstring[t] of
      'a', '�', '�', '�': fdat := 'FDH0aaaaaaadaaafadda';
      'b': fdat := 'FAH1agodacacedca';
      'c': fdat := 'DDH0acafccba';
      'd': fdat := 'FAH0dcdfbacqga';
      'e': fdat := 'EDH0acafaaadaaabaa';
      'f': fdat := 'EAH0cacaapbaccf';
      'g': fdat := 'FDJ0aaaaaaahaaaaadaaacbdcba';
      'h': fdat := 'GAH1afqcageddga';
      'i': fdat := 'DAH0caccagaega';
      'j': fdat := 'DAJ0hbcaecaiafa';
      'k': fdat := 'GAH1afqdafecaaccaca';
      'l': fdat := 'DAH1afqga';
      'm': fdat := 'JDH1acldeaedeadda';
      'n': fdat := 'GDH1acldeadda';
      'o': fdat := 'EDH0acafcfaca';
      'p': fdat := 'FDJ1aepcaafccc';
      'q': fdat := 'FDJ0accebacaaaamfa';
      'r': fdat := 'EDH1aclccc';
      's': fdat := 'DDH0aaaeabaeaaa';
      't': fdat := 'DBH0baflbaca';
      'u': fdat := 'GDH1addaedkda';
      'v': fdat := 'FDH1adcbecebad';
      'w': fdat := 'IDH1adcbecebecebad';
      'x': fdat := 'FDH1accafbebcca';
      'y': fdat := 'FDJ1adecgcbcbeaf';
      'z': fdat := 'EDH1acbakabca';
      'A': fdat := 'HAI0gafcdbbaacaadebacffchaa';
      'B': fdat := 'GAI1afaahahaabacaaabacaahbbacb';
      'C': fdat := 'GAI0bddfbbdbaafaaafabaeaaccab';
      'D': fdat := 'HAI1afaahahaafaaafaabdbbfddc';
      'E': fdat := 'FAI1afaahahaabacaaaacbaabdba';
      'F': fdat := 'FAI1afaahahaabacaaaacdbg';
      'G': fdat := 'HAI0bddfbbdbaafaaadaaabaccacbbgac';
      'H': fdat := 'IAI1afaahahaabacadaeabacaahahaafaa';
      'I': fdat := 'DAI1afaahahaafaa';
      'J': fdat := 'FAI0ebgcaafaahagbah';
      'K': fdat := 'HAI1afaahahaabbbacdcbccaaebaafaa';
      'L': fdat := 'GAI1afaahahaafahagbfca';
      'M': fdat := 'JAI1afaahacdaaffedcccdaahahaafaa';
      'N': fdat := 'HAI1afaahabeabcgcdaccbhaah';
      'O': fdat := 'HAI0bddfbbdbaafaaafaabdbbfddc';
      'P': fdat := 'GAI1afaahahaacabaaacadeece';
      'Q': fdat := 'HAJ0bdefcbdbbafbaafedbaaafbabdca';
      'R': fdat := 'HAI1afaahahaabacaaabcccadbbcbhaa';
      'S': fdat := 'EAI0acbbaeaababbbabaaeabbcb';
      'T': fdat := 'FAI1bgafaahahaafaabg';
      'U': fdat := 'HAI1ahgbhaafahaaafaagbah';
      'V': fdat := 'HAI1ahcffcabegbaabcccfah';
      'W': fdat := 'LAI1ahcfegeaadcababdedabefcaaacdbgah';
      'X': fdat := 'GAI1afaabdbadaaaaaaadeaadabdbaafaa';
      'Y': fdat := 'HAI1ahcfebadeedaaabcaabgah';
      'Z': fdat := 'GAI0gaaccbaacdaaadaaadcaabdbaadca';
      '0': fdat := 'EAI0afbhaafaahbfb';
      '1': fdat := 'DAI1afaahahhaa';
      '2': fdat := 'EAI0abdaabccabbaabadbbbbcba';
      '3': fdat := 'EAI0aadabafaaabacaahbbacb';
      '4': fdat := 'FAI0dbfaaaeabadgahfac';
      '5': fdat := 'EAI0cbabaeabababbaababaabacbc';
      '6': fdat := 'EAI0cdcgbaaacaaabeaaccb';
      '7': fdat := 'EAI1cfbgbccaedbg';
      '8': fdat := 'EAI0abbbbhaabbbaahbbbbb';
      '9': fdat := 'EAI0accaaebaaacaaabgcdd';
      '.': fdat := 'BGI1baba';
      ',': fdat := 'BGJ1bada';
      ':': fdat := 'BDI1babababa';
      ';': fdat := 'BDJ1babacaca';
      '!': fdat := 'BAI1ccbaeaba';
      '?': fdat := 'EAI0abfccbaacaabadfbf';
      '+': fdat := 'ECI0bafadedafad';
      '-': fdat := 'CFI1acacac';
      '=': fdat := 'FDI1aaacaaacaaacaaacaaacaaac';
      '(': fdat := 'DAJ0cddhbafaaaha';
      ')': fdat := 'DAJ1ahaaafabhddc';
      '{': fdat := 'DAJ0dafcadacceha';
      '}': fdat := 'DAJ1ahdcdacadeae';
      '"': fdat := 'BAI1cfcf';
      '~': fdat := 'FDE0abaabaab';
      '/': fdat := 'CAI0fcccccc';
      '�': fdat := 'EAI0dceebaaaaaaaaabcaaebaaa';
      '@': fdat := 'HAI0bddadababbbaaaaabaaaaaaabaaaaabcaabacaec';
      '�', '�': fdat := 'EDH0acafaaadaaabaa';
      '�': fdat := 'EAH0kacacecega';
      '�', '�': fdat := 'EDH0acafcfaca';
      '�': fdat := 'GDH1addaedkda';
      '�': fdat := 'HAI0gafcdbbaacaadebacffchaa';
      '�': fdat := 'FAI1afaahahaabacaaaacbaabdba';
      '�': fdat := 'DAI1afaahahaafaa';
      '�': fdat := 'HAI0bddfbbdbaafaaafaabdbbfddc';
      '�': fdat := 'HAI1ahgbhaafahaaafaagbah';
      '�': fdat := 'FAI0arhabacaahbbacb';
      '�': fdat := 'EAB1dbd';
      '�': fdat := 'H>?1ddd';
      '�': fdat := 'E@B0daaaaaaa';
      '�': fdat := 'ECI0fajaja';
      ' ': fdat := 'EDH0a';
    end;
    e := ord(fdat[1]) - 65;
    h := ord(fdat[2]) - 65;
    l := ord(fdat[3]) - 65;
    n := fdat[4];
    xx := 0;
    yy := h;
    w := 4;
    if space > -1 then
    begin
      repeat
        inc(w);
        for c := 1 to ord(fdat[w]) - 96 do
        begin
          if n = '1' then
            case textcolor of
              0..255: putpixel(textx + xx, texty + yy, textcolor);
              256..{512} 511: putpixel(textx + xx, texty + yy, textcolor + yy * 3 div 2);
              512..767: putpixel(textx + xx, texty + yy, textcolor - yy);
            end;
          inc(yy);
          if yy > l then
          begin
            yy := h;
            inc(xx);
          end;
        end;
        if n = '1' then n := '0' else n := '1';
      until w = length(fdat);
      textx := textx + e + space + 1;
    end else
    begin
      repeat
        inc(w);
        for c := 1 to ord(fdat[w]) - 96 do
        begin
          if n = '1' then
            case textcolor of
              0..255: putpixel(textx + yy, texty - xx, textcolor);
              256..{512} 511: putpixel(textx + yy, texty - xx, textcolor + yy * 3 div 2);
              512..767: putpixel(textx + yy, texty - xx, textcolor - yy);
            end;
          inc(yy);
          if yy > l then
          begin
            yy := h;
            inc(xx);
          end;
        end;
        if n = '1' then n := '0' else n := '1';
      until w = length(fdat);
      texty := texty - e + space - 1;
    end;
    case textstring[t] of
      '�', '�', '�', '�', '�', '�', '�', '�':
        begin
          if space > -1 then d := textx else d := texty;
          case textstring[t] of
            '�', '�', '�', '�', '�', '�': if space > -1 then textx := textx - e - space - 1 else texty := texty + e - space + 1;
            '�': if space > -1 then textx := textx - e - space else texty := texty + e - space;
            '�': if space > -1 then textx := textx - 2 - space else texty := texty + 2 - space;
          end;
          case textstring[t] of
            '�', '�', '�', '�', '�': textstring[t] := '�';
            '�': textstring[t] := '�';
            '�': textstring[t] := '�';
            '�': textstring[t] := '�';
          end;
          dec(t);
        end;
      '�', '�', '�', '�', '�':
        begin
          if space > -1 then d := textx else d := texty;
          case textstring[t] of
            '�', '�', '�': if space > -1 then textx := textx - e - space else texty := texty + e - space;
            '�': if space > -1 then textx := textx - e - space - 2 else texty := texty + e - space + 2;
            '�': if space > -1 then textx := textx - e - space - 1 else texty := texty + e - space + 1;
          end;
          textstring[t] := '�';
          dec(t);
        end;
    end;
    if textstring[t] in ['�', '�', '�', '�'] then
    begin
      if space > -1 then textx := d else texty := d;
      textstring[t] := ' ';
    end;
  end;
end;

procedure font05_2;

var
  d, t, e, h, l, xx, yy, w, c: integer;
  fdat: string;
  n: char;

begin
  //for t := 1 to length(textstring) do
  t := 0;
  while t <= length(textstring) do
  begin
    inc(t);
    fdat := 'FDH0a';
    case textstring[t] of
      'a': fdat := 'GEK0aabbabaebabbbabhaffa';
      'b': fdat := 'GAK1ajueaeadaeadgeea';
      'c': fdat := 'EEK0aeahecdccaa';
      'd': fdat := 'GAK0eeegdaebcaewja';
      'e': fdat := 'FEK0aeahbabbbabebaacaaa';
      'f': fdat := 'FAK0daeaavcaecibi';
      'g': fdat := 'GEN0acadaeababcaababcaabafabaebbabfba';
      'h': fdat := 'HAK1aiweadadajgefja';
      'i': fdat := 'DAK0daecbibgja';
      'j': fdat := 'DAN0dagbdahcblbia';
      'k': fdat := 'IAK1aiwgabafcfbbcdadbdaeaja';
      'l': fdat := 'DAK1aiwja';
      'm': fdat := 'LEK1aeoaadbfgafaadbfgaffa';
      'n': fdat := 'HEK1aeoaadbfgaffa';
      'o': fdat := 'FEK0aeahebehaea';
      'p': fdat := 'GEN1ahveabbeacgded';
      'q': fdat := 'GEN0aedgcaeacaeabaasia';
      'r': fdat := 'FEK1aeoaadbfbe';
      's': fdat := 'EEK0abbfbbacabbfbba';
      't': fdat := 'EBK0cahgajcaeacadaa';
      'u': fdat := 'HEK1affagfbeofa';
      'v': fdat := 'HEK1afbeebaaeecacbbeaf';
      'w': fdat := 'LEK1afbedcaaeddabcdcaaeddabcbeaf';
      'x': fdat := 'HEK1aebedbgdgbdebea';
      'y': fdat := 'HEN1ageegdbageccaabfbhai';
      'z': fdat := 'FEK1bdbcdajadcbdb';
      'A': fdat := 'KAK0jaibfcaaccaabaabdacdcaedaageaafehcja';
      'B': fdat := 'JAK1aibixdadbdadbdadbcccfaeaccca';
      'C': fdat := 'IAK0cedibbebaaibibiaaahaabfaadeaa';
      'D': fdat := 'KAK1aibixibibicgbabebcgeec';
      'E': fdat := 'IAK1aibixdadbdadbbebbidec';
      'F': fdat := 'IAK1aibixdadbdadbbecajch';
      'G': fdat := 'JAK0cedibbebaaibibiaaadacaabcibdgad';
      'H': fdat := 'LAK1aibixdadaeajaeadadxibia';
      'I': fdat := 'FAK1aibixibia';
      'J': fdat := 'IAK0gchdjbibikajaajaj';
      'K': fdat := 'LAK1aibixdadbcccacabbfacbabaaeegcibia';
      'L': fdat := 'IAK1aibixibiajajahc';
      'M': fdat := 'PAK1aibinhfeabfheicgbgbfcechxibia';
      'N': fdat := 'LAK1aibinhdgabciceadccaecbljaj';
      'O': fdat := 'JAK0cedibbebaaibibibiaabebbidec';
      'P': fdat := 'IAK1aibixdadbdadbdaeffdf';
      'Q': fdat := 'JAN0cegiebebdaiacaibbaicaaiaababebcaaicacef';
      'R': fdat := 'LAK1aibixdadbdbcbdccadaacafbcaddbjaja';
      'S': fdat := 'GAK0accidaaabcdbcccbdcbaaadicca';
      'T': fdat := 'HAK1chaibixibidh';
      'U': fdat := 'KAK1ajajjaliajajbikaajaj';
      'V': fdat := 'KAK1ajajchfeabeiehceccabbfchaj';
      'W': fdat := 'OAK1ajchfeabeiegeccddgfeabeiehdcdcdgaj';
      'X': fdat := 'KAK1aibhdgfcbabaehdgaacababcfgcibia';
      'Y': fdat := 'JAK1ajbidfbaddbcgfedbdbabfciaj';
      'Z': fdat := 'IAK0jeedeeddabcccbaddefcgcfd';
      '0': fdat := 'GAK0bgciabgcicgbaicgb';
      '1': fdat := 'FAK1aibiwjaja';
      '2': fdat := 'GAK0bagaabfdfedbaibbaddbhab';
      '3': fdat := 'GAK0bafbaagdcadcbbdhcaacaegcb';
      '4': fdat := 'GAK0fcgaabfabbdbcbcugbb';
      '5': fdat := 'GAK0dadbadddabddabecbbdcbcbaaaddb';
      '6': fdat := 'GAK0dedhbddbaabaecbbccdeaaecb';
      '7': fdat := 'GAK0abhbibhcecabbcddgaj';
      '8': fdat := 'GAK0acccaeafbcdbcbdbccclacbda';
      '9': fdat := 'GAK0bceaaedccbbceabaabddbhded';
      '.': fdat := 'BJK1d';
      ',': fdat := 'BJN1bbea';
      ':': fdat := 'BEK1bcdcb';
      ';': fdat := 'BEN1bcbbccda';
      '!': fdat := 'BAK1edjab';
      '?': fdat := 'FAK0acgdgahcdcahfdf';
      '+': fdat := 'IBJ0dahahahadidahahahad';
      '-': fdat := 'DHJ1abababab';
      '=': fdat := 'HEJ1abababababababababababababababab';
      '(': fdat := 'DAN0dffjcbhbaala';
      ')': fdat := 'DAN1alaabhbcjffd';
      '{': fdat := 'EAN0faheafaecglbla';
      '}': fdat := 'EAN1alblfcfaeafgag';
      '"': fdat := 'BAJ1dfdf';
      '�': fdat := 'FBK0dedgaaaababbbababacdbadcaaa';
      '�', '�', '�': fdat := 'GEK0aabbabaebabbbabhaffa';
      '�': fdat := 'FEK0aeahbabbbabebaacaaa';
      '�': fdat := 'DAK0daeadgdgja';
      '�', '�': fdat := 'FEK0aeahebehaea';
      '�': fdat := 'HEK1affagfbeofa';
      '�': fdat := 'KAK0jaibfcaaccaabaabdacdcaedaageaafehcja';
      '�': fdat := 'IAK1aibixdadbdadbbebbidec';
      '�': fdat := 'FAK1aibixibia';
      '�': fdat := 'JAK0cedibbebaaibibibiaabebbidec';
      '�': fdat := 'KAK1ajajjaliajajbikaajaj';
      '�': fdat := 'FAB1ddd';
      '�': fdat := 'J>?0bddd';
      '�': fdat := 'JAL0axkadadaaadadaagcabdaehc';
      '�': fdat := 'F@C0ebaabbbaab';
      '�': fdat := 'FDL0hambfana';
      '�': fdat := 'FEK0aeahbabbbabebaacaaa';
      ' ': fdat := 'FDH0a';
    end;
    e := ord(fdat[1]) - 65;
    h := ord(fdat[2]) - 65;
    l := ord(fdat[3]) - 65;
    n := fdat[4];
    xx := 0;
    yy := h;
    w := 4;
    if space > -1 then
    begin
      repeat
        inc(w);
        for c := 1 to ord(fdat[w]) - 96 do
        begin
          if n = '1' then
            case textcolor of
              0..255: putpixel(textx + xx, texty + yy, textcolor);
              256..{512} 511: putpixel(textx + xx, texty + yy, textcolor + yy * 3 div 2);
              512..767: putpixel(textx + xx, texty + yy, textcolor - yy);
            end;
          inc(yy);
          if yy > l then
          begin
            yy := h;
            inc(xx);
          end;
        end;
        if n = '1' then n := '0' else n := '1';
      until w = length(fdat);
      textx := textx + e + space + 1;
    end else
    begin
      repeat
        inc(w);
        for c := 1 to ord(fdat[w]) - 96 do
        begin
          if n = '1' then
            case textcolor of
              0..255: putpixel(textx + yy, texty - xx, textcolor);
              256..{512} 511: putpixel(textx + yy, texty - xx, textcolor + yy * 3 div 2);
              512..767: putpixel(textx + yy, texty - xx, textcolor - yy);
            end;
          inc(yy);
          if yy > l then
          begin
            yy := h;
            inc(xx);
          end;
        end;
        if n = '1' then n := '0' else n := '1';
      until w = length(fdat);
      texty := texty - e + space - 1;
    end;
    case textstring[t] of
      '�', '�', '�', '�', '�', '�', '�', '�':
        begin
          if space > -1 then d := textx else d := texty;
          case textstring[t] of
            '�', '�', '�', '�', '�': if space > -1 then textx := textx - e - space - 1 else texty := texty + e - space + 1;
            '�': if space > -1 then textx := textx - e - space - 2 else texty := texty + e - space + 2;
            '�': if space > -1 then textx := textx - e - space else texty := texty + e - space;
            '�': if space > -1 then textx := textx - 2 - space else texty := texty + 2 - space;
          end;
          case textstring[t] of
            '�', '�', '�', '�', '�': textstring[t] := '�';
            '�': textstring[t] := '�';
            '�': textstring[t] := '�';
            '�': textstring[t] := '�';
          end;
          dec(t);
        end;
      '�', '�', '�', '�', '�':
        begin
          if space > -1 then d := textx else d := texty;
          case textstring[t] of
            '�', '�', '�': if space > -1 then textx := textx - e - space else texty := texty + e - space;
            '�': if space > -1 then textx := textx - e - space - 2 else texty := texty + e - space + 2;
            '�': if space > -1 then textx := textx - e - space - 1 else texty := texty + e - space + 1;
          end;
          textstring[t] := '�';
          dec(t);
        end;
    end;
    if textstring[t] in ['�', '�', '�', '�'] then
    begin
      if space > -1 then textx := d else texty := d;
      textstring[t] := ' ';
    end;
  end;
end;

procedure font05_3;

var
  d, t, e, h, l, xx, yy, w, c: integer;
  fdat: string;
  n: char;

begin
  //for t := 1 to length(textstring) do
  t := 0;
  while t <= length(textstring) do
  begin
    inc(t);
    fdat := 'HDH0a';
    case textstring[t] of
      'a': fdat := 'IEN0bbcbbcbeabafcbcbcacaaiajaiia';
      'b': fdat := 'JAN1am{amfagadahadbfbdifhgec';
      'c': fdat := 'HEN0cechbkfchcgdfaabeaa';
      'd': fdat := 'JAN0ffghejdbfbdahbdafaa�maa';
      'e': fdat := 'HEN0bfchakcacccadfdaadcaccbab';
      'f': fdat := 'GAN0dahaama|cahdkck';
      'g': fdat := 'IER0bccbabbeadaiacbbeaacbbeaacbhacbgbcbbacccaaaahcb';
      'h': fdat := 'KAN1al�eagadamamjdjeima';
      'i': fdat := 'EAN0dahdamamajma';
      'j': fdat := 'FAR0obpcdaldaqaqama';
      'k': fdat := 'KAN1al�iacahcgabaacaadccddbecdagbma';
      'l': fdat := 'EAN1al�ma';
      'm': fdat := 'QEN1ahaagbiahuaiaagbiahuaiia';
      'n': fdat := 'KEN1ahaagbiaitaiia';
      'o': fdat := 'IEN0cddhalfchcflahddc';
      'p': fdat := 'JER1al�aafadbhadbfbdjehgff';
      'q': fdat := 'JER0ceghfidbfbdahadagadaama{ma';
      'r': fdat := 'HEN1ahaagbibhcg';
      's': fdat := 'FEN0adbibaaabccbccbaaabibda';
      't': fdat := 'GAN0daljczdahadahalaa';
      'u': fdat := 'KEN1aiiatiaibgaa~ia';
      'v': fdat := 'KEN1aibhdffdaageffcaacbddfbhai';
      'w': fdat := 'OEN1aicgfdiaabgfcaabcdcgfdhbabgfcaaaddbhai';
      'x': fdat := 'JEN1ahcgedgbaabaefeabaabgdegcha';
      'y': fdat := 'JER1ajbabiggjecagabfficfabbickam';
      'z': fdat := 'HEN0iddddfbnbfddecfc';
      'A': fdat := 'PAN0mamalbkcibbafdcadbcafbeadedadhaaggcaeihfjdlbma';
      'B': fdat := 'MAN1albl�eafbeafbeafcccejcbaeagbcbfidb';
      'C': fdat := 'MAN0dffjclbcfcabjdjclblblblaaajabchaaefab';
      'D': fdat := 'NAN1albl�lblblckcjbacfcblcjffd';
      'E': fdat := 'LAN1albl�eafbdcebbgcblckehbjca';
      'F': fdat := 'LAN1albl�eafbeafbdcfabgdambldj';
      'G': fdat := 'OAN0dffjclbcfcabjckclblbgadcfadaaaffacdeaeceiamae';
      'H': fdat := 'PAN1albl�eafbeafafamagaeafbeaf�lbla';
      'I': fdat := 'GAN1albl�lbla';
      'J': fdat := 'KAN0ickdjbbamblbl|albamam';
      'K': fdat := 'OAN1albl�eafbecdaeehabdfaddabaagfiejclbla';
      'L': fdat := 'MAN1albl�lblamamamalbjdicb';
      'M': fdat := 'SAN1alblrjggiihhhifjbicicibhdj�lbla';
      'N': fdat := 'OAN0mblqkdjfkdkdldkddafdcahdaomam';
      'O': fdat := 'NAN0dffjclbcfcabjclblblblcjbacfcblcjffd';
      'P': fdat := 'MAN1albl�faebfaebfafbdbfbdbgfhfidh';
      'Q': fdat := 'NAR0dfjjglfcfcebjbdalbcaldaaldaalgjbbbacfccbaldabjeadfh';
      'R': fdat := 'OAN1albl�faebfaebfbeafdcbdfbhadbfdcbdfbmama';
      'S': fdat := 'JAN0bcdeaeebageaabbdebdcebecdaaaddbbabdlcejcb';
      'T': fdat := 'MAN1ckamamalbl�lblbmamck';
      'U': fdat := 'OAN1amamlbmamaakclamamambkaaajbakcamam';
      'V': fdat := 'OAN1amamckeihfabgdadhifjcibdaebfabchckblam';
      'W': fdat := 'UAN1amamckeihfaahihigicaafcdbcbgeiggaahdachigjcgddabchckam';
      'X': fdat := 'OAN1alblcjfgheabbaebadacejeiaadbbcacgbafghejclbm';
      'Y': fdat := 'NAN1ambldjfgbafebcjfhggfafbcbgbbajckamam';
      'Z': fdat := 'MAN0mehegfgffeabdecbcedbaeffhehdjcidie';
      '0': fdat := 'IAN0dffjclabkblblaalcjffd';
      '1': fdat := 'GAN0bajaabjaama{mama';
      '2': fdat := 'JAN0caiaabibaaiehffbafdbbkccafdcbcfcjbb';
      '3': fdat := 'IAN0bahbbbhcaajddafdbcejcgagbcbfidc';
      '4': fdat := 'IAN0hcjaabiabbgbcbfaebe�ibc';
      '5': fdat := 'IAN0fadbdddcabacdfaceeacfdbcedbcedcccaabeeb';
      '6': fdat := 'IAN0fegickcdfbabbagaaacbfccjegaafdc';
      '7': fdat := 'IAN0cbidjckcjdgcacebdcbcfeibl';
      '8': fdat := 'IAN0bcdccebeaicdbdebdcebddcdciaeafcccdb';
      '9': fdat := 'IAN0befaagejccfbcaaagabbabfdckcigef';
      '.': fdat := 'CLN1i';
      ',': fdat := 'CLQ1cbdaabcb';
      ':': fdat := 'CEN1cdfdfdc';
      ';': fdat := 'CEQ1cdcbddcaaacecb';
      '!': fdat := 'CAN1eflbhfc';
      '?': fdat := 'IAN0ackcjaabjajdfbbecbdiiejci';
      '+': fdat := 'KCM0eajajajajaekeajajajajae';
      '-': fdat := 'FJL1abababababab';
      '=': fdat := 'KFL1abacabacabacabacabacabacabacabacabacabacabac';
      '(': fdat := 'FAR0ffjjfnccjcbanaaapa';
      ')': fdat := 'FAR1apaaanabcjccnfjjff';
      '{': fdat := 'EAR0hakbcaaacccgahabbcccccpa';
      '}': fdat := 'EAR1apcbccccbagahcbcaaaccjai';
      '"': fdat := 'CAL1dhgedh';
      '�': fdat := 'HAN0ffghejabaacacdbacadbcedaedcagcbab';
      '�', '�', '�': fdat := 'IEN0bbcbbcbeabafcbcbcacaaiajaiia';
      '�': fdat := 'HEN0bfchakcacccadfdaadcaccbab';
      '�': fdat := 'FAN0rahadjdjdjma';
      '�', '�': fdat := 'IEN0cddhalfchcflahddc';
      '�': fdat := 'KEN1aiiatiaibgaa~ia';
      '�': fdat := 'PAN0mamalbkcibbafdcadbcafbeadedadhaaggcaeihfjdlbma';
      '�': fdat := 'LAN1albl�eafbdcebbgcblckehbjca';
      '�': fdat := 'GAN1albl�lbla';
      '�': fdat := 'NAN0dffjclbcfcabjclblblblcjbacfcblcjffd';
      '�': fdat := 'OAN1amamlbmamaakclamamambkaaajbakcamam';
      '�': fdat := 'MAP0a�oaeafabaeafabbcceabicbceagdcbfkdb';
      '�': fdat := 'HAC1iii';
      '�': fdat := 'P=?0fili';
      '�': fdat := 'H@C0ibaabbbaab';
      '�': fdat := 'HEN0bfchakcacccadfdaadcaccbab';
      '�': fdat := 'IDO0jbjasbibibsbib';
      //'�':fdat:='IDO0kajataibiataja';
      //'o':fdat:='IEN0cddhalfchcflahddc';
      ' ': fdat := 'HDH0a';
    end;
    e := ord(fdat[1]) - 65;
    h := ord(fdat[2]) - 65;
    l := ord(fdat[3]) - 65;
    n := fdat[4];
    xx := 0;
    yy := h;
    w := 4;
    if space > -1 then
    begin
      repeat
        inc(w);
        for c := 1 to ord(fdat[w]) - 96 do
        begin
          if n = '1' then
            case textcolor of
              0..255: putpixel(textx + xx, texty + yy, textcolor);
              256..{512} 511: putpixel(textx + xx, texty + yy, textcolor + yy * 3 div 2);
              512..767: putpixel(textx + xx, texty + yy, textcolor - yy);
            end;
          inc(yy);
          if yy > l then
          begin
            yy := h;
            inc(xx);
          end;
        end;
        if n = '1' then n := '0' else n := '1';
      until w = length(fdat);
      textx := textx + e + space + 1;
    end else
    begin
      repeat
        inc(w);
        for c := 1 to ord(fdat[w]) - 96 do
        begin
          if n = '1' then
            case textcolor of
              0..255: putpixel(textx + yy, texty - xx, textcolor);
              256..{512} 511: putpixel(textx + yy, texty - xx, textcolor + yy * 3 div 2);
              512..767: putpixel(textx + yy, texty - xx, textcolor - yy);
            end;
          inc(yy);
          if yy > l then
          begin
            yy := h;
            inc(xx);
          end;
        end;
        if n = '1' then n := '0' else n := '1';
      until w = length(fdat);
      texty := texty - e + space - 1;
    end;
    case textstring[t] of
      '�', '�', '�', '�', '�', '�', '�', '�':
        begin
          if space > -1 then d := textx else d := texty;
          case textstring[t] of
            '�', '�', '�', '�', '�': if space > -1 then textx := textx - e - space - 1 else texty := texty + e - space + 1;
            '�': if space > -1 then textx := textx - e - space - 2 else texty := texty + e - space + 2;
            '�': if space > -1 then textx := textx - e - space else texty := texty + e - space;
            '�': if space > -1 then textx := textx - 3 - space else texty := texty + 3 - space;
          end;
          case textstring[t] of
            '�', '�', '�', '�', '�': textstring[t] := '�';
            '�': textstring[t] := '�';
            '�': textstring[t] := '�';
            '�': textstring[t] := '�';
          end;
          dec(t);
        end;
      '�', '�', '�', '�', '�':
        begin
          if space > -1 then d := textx else d := texty;
          case textstring[t] of
            '�': if space > -1 then textx := textx - e - space - 1 else texty := texty + e - space - 1;
            '�': if space > -1 then textx := textx - e - space else texty := texty + e - space;
            '�': if space > -1 then textx := textx - e - space + 1 else texty := texty + e - space + 1;
            '�': if space > -1 then textx := textx - e - space - 4 else texty := texty + e - space + 4;
            '�': if space > -1 then textx := textx - e - space - 2 else texty := texty + e - space + 2;
          end;
          textstring[t] := '�';
          dec(t);
        end;
    end;
    if textstring[t] in ['�', '�', '�', '�'] then
    begin
      if space > -1 then textx := d else texty := d;
      textstring[t] := ' ';
    end;
  end;
end;

procedure font05_4;

var
  d, t, e, h, l, xx, yy, w, c: integer;
  fdat: string;
  n: char;

begin
  //for t := 1 to length(textstring) do
  t := 0;
  while t <= length(textstring) do
  begin
    inc(t);
    fdat := 'LDH0a';
    case textstring[t] of
      'a': fdat := 'OHV0cbecddcebebfbaabbgaafgaaebdabaeaeabbcaeacmbnbmbmclmanab';
      'b': fdat := 'OAV1auuauatbtbuibibiakahbkahcibhnhmjklioef';
      'c': fdat := 'LHV0dfhiekclbnabgeaajcaakbadhbaegacdfaecj';
      'd': fdat := 'OAV0kfnjkljmhnhbhdhakbiajabagbhacuauauatbtuab';
      'e': fdat := 'LHV0dfhidlclbnabdacdaaeadcabdaebagebbfeacfdafdcad';
      'f': fdat := 'MAV0gbtbkadrcsbtbtauaafbkaaafbmdrerdsbr';
      'g': fdat := 'NH\0cdecbccfceacbhagblcdckcdccfbcdcbhacdccfbcdckcdcjddcjddcieeaaabadgebbmcc';
      'h': fdat := 'PAV1asaauauauauaujajaiatbtbkahnhnhnimjluaa';
      'i': fdat := 'GAV0galabccnaebnaebnaebnbccnuaa';
      'j': fdat := 'JA\0xbydxe{agasaacczbzbtaebtbccrc';
      'k': fdat := 'QAV1asaauauauauauoaeancsdqfbahacjhdbhhcdghbgehaidtbuaa';
      'l': fdat := 'GAV1asaauauauauauuaa';
      'm': fdat := 'YHV1alaananananancajabambmbkaanananbmclcajabanambkaanananbmclnaa';
      'n': fdat := 'PHV1alaananananancajabambmbkaanananbmclnaa';
      'o': fdat := 'NHV0dfgielclbnabjbaalaaalaabjbanblcldjgfe';
      'p': fdat := 'OH\1as�abiagckagbkagcibgngnhljjmfk';
      'q': fdat := 'OH\0eenikkimhmgchcgakbgakahbibgvata�ta';
      'r': fdat := 'MHV1alaananananancbiabanamdkejekck';
      's': fdat := 'JHV0bdcebfccbhdababedbaacedaaadecabacecabcbgbebfjdc';
      't': fdat := 'JCV0farbrlgnepbrasfbjbfbjagbiac';
      'u': fdat := 'PHV1anlcmbnananmbmbmabajacnanananannaa';
      'v': fdat := 'PHV1anbmdkfihgjelcabkaadihgjbdaecfabciclbman';
      'w': fdat := 'WHV1anbmdkfiifkdnaabkfhbagbebccgejfiifkdnaabkghicdadcgejbman';
      'x': fdat := 'NHV1alaabkaacibaefcagcabaajcaaaaihjaacaahaabacgacfeabicaakbaalaa';
      'y': fdat := 'OH\1apcabnhmjkljmjaakfbbaakbbdacklfpckaccndqbsat';
      'z': fdat := 'LHV0maaefcabgeaafgaadiaabkakbaaidaagfaaegbachcaahea';
      'A': fdat := 'WAU0tatasbqdocbamcdakbbadahcdakbgaibiafhfaekdaflbahmdaflbahmjkmhofqdsbtata';
      'B': fdat := 'TAU1asbscq�iaibiaibiaibhbicgbheedfxaiaibhbicgbiddegoec';
      'C': fdat := 'TAU0hfmjimgoeqdrbehfbcmcacoeqcsbsbsbsbsaaaraaaqabcoabdmabgjac';
      'D': fdat := 'UAU1asbscq�sbsbscrcqfncaeiebscqdqeohklgg';
      'E': fdat := 'SAU1asbscq�iaibiaibhchbgffbdkdcrcqfohlcpendc';
      'F': fdat := 'RAU1asbscq�iahciaibiaibhciafggadkebsbsdqfo';
      'G': fdat := 'WAU0hflkimgoeqcsbeiebcnbabqdqcsbsbkagbkagbkbfaaajiabihbchhbefhagehmbsatah';
      'H': fdat := 'WAU1asbscq�hahciaibiaiajajaiaibiaichah�qcsbsa';
      'I': fdat := 'KAU1asbscq�qcsbsa';
      'J': fdat := 'PAU0ocqepfogocbbsbscq�atasbrcbsatat';
      'K': fdat := 'YAU1asbscq�hahcibhbhdgahfnhmaahjachbaeafjcahibbikkimgofpdrcsata';
      'L': fdat := 'TAU1asbscq�rbsbsatatatatasbsbrcofndc';
      'M': fdat := ']AU1asbscqzphnilkkmjmkmjmjllhocpcpbpcpbmacboaabp�qcsbsa';
      'N': fdat := 'UAU1asbscq{ohnhmaagohnhnhogohnheahhdajgcbjhawsatat';
      'O': fdat := 'WAU0gglkimgoeqcsbeieacobabqcsbsbsbsbscqeobbeiebscqeogmiklgg';
      'P': fdat := 'RAU1asbscq�iahciaibiaicgbjcecjkkilimgoem';
      'Q': fdat := 'WAZ0ggqknmlojqhsgeiegbocebqbeasbdasccasdbaseaaseabqbadabocbcaeiedbasdbbqfacogadmhaekiaggl';
      'R': fdat := 'WAU1asbscq�iahciaibibhbidgaiefbghdcejcjahcibiahegbgffcdjdrcsbta';
      'S': fdat := 'OAU0ceehbheebigcbacfgbaaefgaaafegcffgbffgbgegcfffaabffdbacegbbbdejagdhned';
      'T': fdat := 'SAU1fodqbsatasbsbr�rcsbsbtbsdqfo';
      'U': fdat := 'VAU1atatbsqdsbtatavqdrcsatatatatbraaaqbabobbqdbsatat';
      'V': fdat := 'WAU1atatcrdqgnilkjmhacleaelklkkmfqbqbpchagbkaebmbacodqbsatat';
      'W': fdat := '`AU1atbscrephmjkmhofadmjmkklgbamcdakbgcgbiebckgnjkliofacmdaenjllgpcococjaebmbacocrbsat';
      'X': fdat := 'VAU1asbscqeohljjacigadjeaelabgachiadinhmjcafachbbdaekcahljjmgoeqdrbta';
      'Y': fdat := 'VAU1atatbsdqepgniklibajgccqfohmilkjibhbgcjbeblbcbodqcrbsat';
      'Z': fdat := 'TAU0tashkhlhkijjjkhibbghdbeiebdhgbbihkjjkhlgnfneoenfmec';
      '0': fdat := 'NAU0fijmfqcsbsabqcsbscqbasbscqenjif';
      '1': fdat := 'KAU0capabbpabbpabsata�sbtata';
      '2': fdat := 'NAU0taccmbbcmcacmdaclijlhcajedbrcqddajfdbhgdceheobd';
      '3': fdat := 'NAU0qcdaldcameabndaafaiefbideciecehoebatafbkcebkdcdhnef';
      '4': fdat := 'NAU0ldpaacoabcmbcclaeckafcibgchrb�mcrce';
      '5': fdat := 'NAU0jafbidedegeeacbefhbefhbeggbfgfcehecehecfgedfeaadefcbacgibajfd';
      '6': fdat := 'NAU0jflkimfpdqdfhdbccakaacdakaabechbabeoflabgkaaiibajfd';
      '7': fdat := 'MAU0dboepdqdqdqdngkjhfcdeffdbfiilfocr';
      '8': fdat := 'NAU0cdgdegdfcibhbibhamfdcghbeggbfgfbggdde�bgbjcfchedfdd';
      '9': fdat := 'NAU0dfjabiiaakgbalfoebabhcebaakadcaakaccbdhfdqdpfnhklgi';
      '.': fdat := 'EQU0acaoaca';
      ',': fdat := 'FQZ0acfedfcaaebbbgddd';
      ':': fdat := 'EHU0acfcaedjdjdeacfca';
      ';': fdat := 'FHZ0acfcfededfdecaaedebbbcfgmdd';
      '!': fdat := 'EAU0aekcakesbofeaekca';
      '?': fdat := 'LAU0ccpfofngjcaabcjfidbgfddpfcajlhmgpdn';
      '+': fdat := 'PDS0gbnbnbnbnbnbnbg�gbnbnbnbnbnbnbg';
      '-': fdat := 'INQ1bbbbbbbbbbbbbbbbbb';
      '=': fdat := 'QHQ1bcbcbcbcbcbcbcbcbcbcbcbcbcbcbcbcbcbcbcbcbcbcbcbcbcbcbcbcbcbcbcbcbcbc';
      '(': fdat := 'JA[0jgqmmojsgekeecqcdbsbcbubbawaaaya';
      ')': fdat := 'JA[1ayaaawabbubcbsbdcqceekegsjommqgj';
      '{': fdat := 'IA[0mayaaaoddbaaedekakckckbkckabddgddcybya';
      '}': fdat := 'IA[1aybxdddgddbakckbkckckakedeaabddpbyam';
      '"': fdat := 'EAQ0adlhijghjdl';
      '�': fdat := 'LAU0kfnijlileabnadbbdachcaeadfdbdaedegecgfeaifdaldcac';
      '�', '�', '�': fdat := 'OHV0cbecddcebebfbaabbgaafgaaebdabaeaeabbcaeacmbnbmbmclmanab';
      '�': fdat := 'LHV0dfhidlclbnabdacdaaeadcabdaebagebbfeacfdafdcad';
      '�': fdat := 'GAV0}alahnhnhnhnhnuaa';
      '�', '�': fdat := 'NHV0dfgielclbnabjbaalaaalaabjbanblcldjgfe';
      '�': fdat := 'PHV1anlcmbnananmbmbmabajacnanananannaa';
      '�': fdat := 'WAU0tatasbqdocbamcdakbbadahcdakbgaibiafhfaekdaflbahmdaflbahmjkmhofqdsbtata';
      '�': fdat := 'SAU1asbscq�iaibiaibhchbgffbdkdcrcqfohlcpendc';
      '�': fdat := 'KAU1asbscq�qcsbsa';
      '�': fdat := 'WAU0gglkimgoeqcsbeieacobabqcsbsbsbsbscqeobbeiebscqeogmiklgg';
      '�': fdat := 'VAU1atatbsqdsbtatavqdrcsatatatatbraaaqbabobbqdbsatat';
      '�': fdat := 'TAX0cuawa�vawaiaiacahbiacbgbhbccedfccudiaiehbifgbigdegre';
      '�': fdat := 'LAE0acaoacvcaoac';
      '�': fdat := 'W;?0pcaoac{caoac';
      '�': fdat := 'LAE0ucaacbcbcaac';
      '�': fdat := 'LHV0dfhidlclbnabdacdaaeadcabdaebagebbfeacfdafdcad';
      '�': fdat := 'NFW0ocncncmcncncncncncncncmcncnc';
      //'o':fdat:='NHV0dfgielclbnabjbaalaaalaabjbanblcldjgfe';
      ' ': fdat := 'LDH0a';
    end;
    e := ord(fdat[1]) - 65;
    h := ord(fdat[2]) - 65;
    l := ord(fdat[3]) - 65;
    n := fdat[4];
    xx := 0;
    yy := h;
    w := 4;
    if space > -1 then
    begin
      repeat
        inc(w);
        for c := 1 to ord(fdat[w]) - 96 do
        begin
          if n = '1' then
            case textcolor of
              0..255: putpixel(textx + xx, texty + yy, textcolor);
              256..{512} 511: putpixel(textx + xx, texty + yy, textcolor + yy * 3 div 2);
              512..767: putpixel(textx + xx, texty + yy, textcolor - yy);
            end;
          inc(yy);
          if yy > l then
          begin
            yy := h;
            inc(xx);
          end;
        end;
        if n = '1' then n := '0' else n := '1';
      until w = length(fdat);
      textx := textx + e + space + 1;
    end else
    begin
      repeat
        inc(w);
        for c := 1 to ord(fdat[w]) - 96 do
        begin
          if n = '1' then
            case textcolor of
              0..255: putpixel(textx + yy, texty - xx, textcolor);
              256..{512} 511: putpixel(textx + yy, texty - xx, textcolor + yy * 3 div 2);
              512..767: putpixel(textx + yy, texty - xx, textcolor - yy);
            end;
          inc(yy);
          if yy > l then
          begin
            yy := h;
            inc(xx);
          end;
        end;
        if n = '1' then n := '0' else n := '1';
      until w = length(fdat);
      texty := texty - e + space - 1;
    end;
    case textstring[t] of
      '�', '�', '�', '�', '�', '�', '�', '�':
        begin
          if space > -1 then d := textx else d := texty;
          case textstring[t] of
            '�', '�', '�', '�', '�': if space > -1 then textx := textx - e - space - 1 else texty := texty + e - space + 1;
            '�': if space > -1 then textx := textx - e - space else texty := texty + e - space;
            '�': if space > -1 then textx := textx - e - space - 3 else texty := texty + e - space + 3;
            '�': if space > -1 then textx := textx - space - 6 else texty := texty - space;
          end;
          case textstring[t] of
            '�', '�', '�', '�', '�': textstring[t] := '�';
            '�': textstring[t] := '�';
            '�': textstring[t] := '�';
            '�': textstring[t] := '�';
          end;
          dec(t);
        end;
      '�', '�', '�', '�', '�':
        begin
          if space > -1 then d := textx else d := texty;
          case textstring[t] of
            '�', '�', '�': if space > -1 then textx := textx - e - space else texty := texty + e - space;
            '�': if space > -1 then textx := textx - e - space - 6 else texty := texty + e - space + 6;
            '�': if space > -1 then textx := textx - e - space - 2 else texty := texty + e - space + 2;
          end;
          textstring[t] := '�';
          dec(t);
        end;
    end;
    if textstring[t] in ['�', '�', '�', '�'] then
    begin
      if space > -1 then textx := d else texty := d;
      textstring[t] := ' ';
    end;
  end;
end;

begin
  writeln('SMFONT05 - Font by Remco de Korte - Soft Machine');
  delay(100);
end.
