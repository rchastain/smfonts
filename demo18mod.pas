uses
{$IFDEF unix}
  cThreads,
{$ENDIF}
  ptcGraph, ptcCrt,
  //crt,
  smfont18, smgraf {, graph};

var
  i:integer;

begin
  initgraf;

  for i := 1 to 7 do setpal({colnum[i]}i,0,0,63-9*(i-1));
  for i := 8 to 15 do setpal({colnum[i]}i,63-9*(i-8),63-9*(i-8),0);
  
  font18_4('SMFONT18',0,0,1,4);
  font18_2('Font by Remco de Korte - Soft Machine',0,32,2,2);
  font18_1('updated nov. 1996',0,48,3,1);
  font18_3('abcdefghijklmnopqrstuvwxyz',0,120,4,3);
  font18_3('ABCDEFGHIJKLMNOPQRSTUVWXYZ',0,160,5,3);
  font18_3('0123456789!?.,:;+-=()<>*/&"',0,200,6,3);
  font18_1('Press a key...',0,280,7,2);
  
  //fadein;
  readkey;
  fadeout;
  cleardevice;
  
  for i := 1 to 15 do
    font18_3('ABCDEFGHIJKLMNOPQRSTUVWXYZ',0,20*i,i,3);
  
  fadein;
  readkey;
  fadeout;
  cleardevice;
  
  font18_4('SMFONT18',0,0,15,4);
  font18_2('Font by Remco de Korte - Soft Machine',0,32,15,2);
  font18_1('updated nov. 1996',0,48,15,1);
  font18_3('abcdefghijklmnopqrstuvwxyz',0,120,15,3);
  font18_3('ABCDEFGHIJKLMNOPQRSTUVWXYZ',0,160,15,3);
  font18_3('0123456789!?.,:;+-=()<>*/&"',0,200,15,3);
  font18_1('Press a key...',0,280,15,2);
  
  fadein;
  readkey;
  
  closegraph;
end.
